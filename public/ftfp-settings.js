(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["ftfp-settings"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/DashboardComponent.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/admin/components/DashboardComponent.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");
/* harmony import */ var _UserStateChartComponent_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./UserStateChartComponent.js */ "./resources/js/admin/components/UserStateChartComponent.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  // extends: Bar,
  components: {
    LineChart: _UserStateChartComponent_js__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  data: function data() {
    return {
      datacollection: null,
      selectedYear: new Date().getFullYear(),
      base_url: window.base_url,
      isLoading: true,
      data: {
        available_years: []
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
          display: false,
          labels: {
            fontColor: 'rgb(255, 99, 132)'
          }
        }
      }
    };
  },
  mounted: function mounted() {
    var _this = this;

    axios.get('/').then(function (data) {
      _this.data = data.data;

      _this.data.lead_boards.sort(function (a, b) {
        return b.state.points - a.state.points;
      });

      _this.isLoading = false;

      _this.fillData(_this.data.users_states);
    })["catch"](function (error) {
      return _this.isLoading = false;
    });
  },
  methods: {
    changeYear: function changeYear() {
      console.log(this.selectedYear);
    },
    fillData: function fillData(datax) {
      var data = datax;
      var labels = data.map(function (month) {
        return month.name;
      });
      var values = data.map(function (month) {
        return month.value;
      });
      this.datacollection = {
        labels: labels,
        datasets: [{
          label: 'Users Registration',
          backgroundColor: '#ee2c48',
          data: values
        }]
      };
    },
    loadUserChartData: function loadUserChartData() {
      var _this2 = this;

      axios.get("/chart/".concat(this.selectedYear, "/users")).then(function (_ref) {
        var data = _ref.data;

        _this2.fillData(data);
      });
    },
    ordinal_suffix_of: function ordinal_suffix_of(i) {
      var j = i % 10,
          k = i % 100;
      var suffix = "th";

      if (j === 1 && k !== 11) {
        suffix = "st";
      }

      if (j === 2 && k !== 12) {
        suffix = "nd";
      }

      if (j === 3 && k !== 13) {
        suffix = "rd";
      }

      return suffix;
    }
  },
  computed: {
    allYears: function allYears() {
      var year = new Date().getFullYear();
      var years = [];

      for (var i = year - 9; i <= year; i++) {
        years.push(i);
      }

      return years;
    }
  },
  watch: {
    'selectedYear': function selectedYear() {
      this.loadUserChartData();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/SettingsComponent.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/admin/components/SettingsComponent.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      base_url: window.base_url,
      isLoading: true,
      settings: {
        leverage_points: 0,
        challenge_points: 0,
        challenge_accept_points: 0,
        video_posted_points: 0,
        max_no_challenge_accepted_in_72_hr: 0,
        time_to_accept_a_challenge: 0,
        time_to_complete_a_challenge: 0,
        min_exercise_for_a_challenge: 0,
        time_to_select_another_challenge: 0,
        time_to_approve_or_deny_challenge: 0,
        voting_time_for_a_challenge: 0,
        time_format: 'Hours',
        allowed_requests_monthly: 0,
        allowed_votes_monthly: 0,
        acceptable_challenge_monthly: 0,
        no_of_days_to_reset_user_settings: 0,
        points_earned_per_vote: 0,
        reset_days: 0,
        minimum_users_to_show_trivia_challenge: 500,
        maximum_questions_per_trivia_challenge: 25,
        bonus_points_interval: 0,
        ftfp_bonus_points: 0,
        minimum_friends_for_challenge: 5,
        maximum_friends_request_accept_in_a_month: 20
      },
      formSettings: [{
        title: "Set Leverage Points",
        name: 'leverage_points',
        postFix: 'per challenge',
        type: "number"
      }, {
        title: "Points earned per challenge initiated",
        name: 'challenge_points',
        postFix: '',
        type: "number"
      }, {
        title: "Points earned per challenge accepted",
        name: 'challenge_accept_points',
        postFix: '',
        type: "number"
      }, {
        title: "Points earned per video posted/submitted",
        name: 'video_posted_points',
        postFix: '',
        type: "number"
      }, {
        title: "Maximum number of challenges to be accepted in 72 hours",
        name: 'max_no_challenge_accepted_in_72_hr',
        postFix: '',
        type: "number"
      }, {
        title: "Set time to accept a challenge",
        name: 'time_to_accept_a_challenge',
        postFix: 'hour',
        type: "number"
      }, {
        title: "Set time to complete a challenge",
        name: 'time_to_complete_a_challenge',
        postFix: 'hour',
        type: "number"
      }, {
        title: "Minimum number of exercise that can be in order to challenge a friend",
        name: 'min_exercise_for_a_challenge',
        postFix: '',
        type: "number"
      }, {
        title: "Set time to select another challenge",
        name: 'time_to_select_another_challenge',
        postFix: 'hour',
        type: "number"
      }, {
        title: "Set time for challenger and initiator to approve or deny a challenge",
        name: 'time_to_approve_or_deny_challenge',
        postFix: 'hour',
        type: "number"
      }, {
        title: "Set voting time for a challenge",
        name: 'voting_time_for_a_challenge',
        postFix: 'hour',
        type: "number"
      }, {
        title: "Set time format for a challenge(hour/minutes)",
        name: 'time_format',
        postFix: '',
        type: "text"
      }, {
        title: "No of request user will have to initiate challenge for Follow The Fit Path Challenge",
        name: 'allowed_requests_monthly',
        postFix: '',
        type: "number"
      }, {
        title: "No of votes user will have to vote for Follow The Fit Path Challenge",
        name: 'allowed_votes_monthly',
        postFix: '',
        type: "number"
      }, {
        title: "No of challenge request user can accept in a month in Follow The Fit Path Challenge",
        name: 'acceptable_challenge_monthly',
        postFix: '',
        type: "number"
      }, {
        title: "Points Earned/vote",
        name: 'points_earned_per_vote',
        postFix: '',
        type: "number"
      }, {
        title: "Reset Days",
        name: 'reset_days',
        postFix: 'Days',
        type: "number"
      }, {
        title: "Minimum Users Required to show Trivia Challenge",
        name: 'minimum_users_to_show_trivia_challenge',
        postFix: 'Users',
        type: "number"
      }, {
        title: "Max no. of question/trivia challenge",
        name: 'maximum_questions_per_trivia_challenge',
        postFix: 'Days',
        type: "number"
      }, {
        title: "Points needed to add bonus points to Global Points",
        name: 'bonus_points_interval',
        postFix: '',
        type: "number"
      }, {
        title: "Bonus points for Follow the Fit Path",
        name: 'ftfp_bonus_points',
        postFix: 'Points',
        type: "number"
      }, {
        title: "Minimum Friends for challenge request",
        name: 'minimum_friends_for_challenge',
        postFix: 'Points',
        type: "number"
      }, {
        title: "Minimum Friends request accept per month",
        name: 'maximum_friends_request_accept_in_a_month',
        postFix: '',
        type: "number"
      }]
    };
  },
  mounted: function mounted() {
    var _this = this;

    axios.get('/settings').then(function (_ref) {
      var data = _ref.data;
      var settings = data;

      for (var key in settings) {
        if (settings.hasOwnProperty(key)) {
          _this.settings[key] = settings[key];
        }
      }

      _this.isLoading = false;
    });
  },
  methods: {
    save: function save() {
      var _this2 = this;

      this.isLoading = true;
      axios.post('/settings', this.settings).then(function (_ref2) {
        var data = _ref2.data;

        _this2.$toastr.success('Settings Saved Successfully', 'Success');

        _this2.isLoading = false;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/users/EditProfileComponent.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/admin/components/users/EditProfileComponent.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var cropperjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! cropperjs */ "./node_modules/cropperjs/dist/cropper.js");
/* harmony import */ var cropperjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(cropperjs__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {},
  data: function data() {
    return {
      userId: null,
      isEdit: false,
      cropper: null,
      imageEditing: false,
      borderRadius: 200,
      scale: 1,
      base_url: window.base_url,
      isLoading: false,
      personalImage: false,
      myFiles: [],
      form: {
        name: '',
        email: '',
        password: '',
        password_confirmation: '',
        city: '',
        address_state: '',
        country: '',
        postal_code: ''
      },
      componentForm: {
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      }
    };
  },
  computed: {
    scaleNumberic: function scaleNumberic() {
      return Number(this.scale);
    }
  },
  mounted: function mounted() {
    if (this.$route.params.id) {
      this.isLoading = true;
      this.isEdit = true;
      this.userId = this.$route.params.id;
      this.loadUser();
    }

    var userid = this.userId;
    this.$validator.extend("unique", {
      validate: function validate(value, field) {
        return axios.post("/check?field=email&model=user&ignore=" + userid, {
          email: value
        }).then(function (_ref) {
          var data = _ref.data;
          return {
            valid: true,
            data: {
              message: "good to go"
            }
          };
        })["catch"](function (error) {
          return {
            valid: false,
            data: {
              message: "".concat(value, " is already taken.")
            }
          };
        });
      },
      getMessage: function getMessage(field, params, data) {
        return data.message;
      }
    });
  },
  methods: {
    loadUser: function loadUser() {
      var _this = this;

      axios.get("/users/".concat(this.userId)).then(function (data) {
        _this.form = data.data;
        _this.personalImage = data.data.image;
        _this.isLoading = false;
      });
    },
    onPlaceChanged: function onPlaceChanged(place) {
      var fields = {
        locality: 'city',
        administrative_area_level_1: 'state',
        country: 'country',
        postal_code: 'postal_code'
      };

      for (var i = 0; i < place.place.address_components.length; i++) {
        var addressType = place.place.address_components[i].types[0];
        if (this.componentForm[addressType]) this.form[fields[addressType]] = place.place.address_components[i][this.componentForm[addressType]];
      }
    },
    onNoResult: function onNoResult() {
      console.log('no result place');
    },
    readURL: function readURL() {
      var _this2 = this;

      var file = this.$refs.profileImageInput.files[0];

      if (file) {
        var reader = new FileReader();

        reader.onload = function (e) {
          _this2.personalImage = e.target.result;
          _this2.form.image = e.target.result;
          var image = document.getElementById('personal_img'); // this.cropper = new Cropper(image, {
          //     aspectRatio: 1,
          //     // viewMode: 1,
          //     // crop(event) {
          //     //     console.log(event.detail.x);
          //     //     console.log(event.detail.y);
          //     //     /*console.log(event.detail.width);
          //     //     console.log(event.detail.height);
          //     //     console.log(event.detail.rotate);
          //     //     console.log(event.detail.scaleX);
          //     //     console.log(event.detail.scaleY);*/
          //     // },
          // });
        };

        reader.readAsDataURL(file);
      }
    },
    validate: function validate() {
      var _this3 = this;

      this.$validator.validateAll().then(function (result) {
        if (!result) return;

        _this3.save();
      });
    },
    save: function save() {
      this.isLoading = true;

      if (this.isEdit) {
        this.update();
      } else {
        this.insert();
      }
    },
    update: function update() {
      var _this4 = this;

      axios.put("/users/".concat(this.userId), this.form).then(function (data) {
        _this4.isLoading = false;

        _this4.$toastr.success('User has been updated successfully', 'Success', {});

        _this4.$router.push({
          name: 'users.index'
        });
      })["catch"](function (e) {
        return _this4.isLoading = false;
      });
    },
    insert: function insert() {
      var _this5 = this;

      axios.post('/users', this.form).then(function (data) {
        _this5.isLoading = false;

        _this5.$toastr.success('User has been added successfully', 'Success', {});

        _this5.$router.push({
          name: 'users.index'
        });
      })["catch"](function (e) {
        return _this5.isLoading = false;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/DashboardComponent.vue?vue&type=style&index=0&lang=css&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/admin/components/DashboardComponent.vue?vue&type=style&index=0&lang=css& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.small {\n    width: 100%;\n    height: 400px;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-SG": "./node_modules/moment/locale/en-SG.js",
	"./en-SG.js": "./node_modules/moment/locale/en-SG.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/DashboardComponent.vue?vue&type=style&index=0&lang=css&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/admin/components/DashboardComponent.vue?vue&type=style&index=0&lang=css& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--7-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./DashboardComponent.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/DashboardComponent.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/DashboardComponent.vue?vue&type=template&id=6128fbb4&":
/*!***************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/admin/components/DashboardComponent.vue?vue&type=template&id=6128fbb4& ***!
  \***************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "app-content content" },
    [
      _vm.isLoading
        ? _c("BlockUI", { attrs: { message: "Loading..." } }, [
            _c("i", { staticClass: "fa fa-cog fa-spin fa-3x fa-fw" })
          ])
        : _vm._e(),
      _vm._v(" "),
      _c("div", { staticClass: "content-wrapper" }, [
        _c("div", { staticClass: "content-body" }, [
          _c("section", { attrs: { id: "combination-charts" } }, [
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-12" }, [
                _c(
                  "div",
                  { staticClass: "card rounded p-3 admin-overview-main" },
                  [
                    _c("div", { staticClass: "row" }, [
                      _c("div", { staticClass: "col-lg-12" }, [
                        _c("h1", [_vm._v("Home")]),
                        _vm._v(" "),
                        _c("div", { staticClass: "row" }, [
                          _c(
                            "div",
                            {
                              staticClass:
                                "col-lg-9 col-md-12 col-sm-12 col-xs-12"
                            },
                            [
                              _c("div", { staticClass: "row" }, [
                                _c(
                                  "div",
                                  {
                                    staticClass:
                                      "col-lg-4 col-md-6 col-sm-6 col-xs-12"
                                  },
                                  [
                                    _c(
                                      "div",
                                      { staticClass: "employ-top-boxs" },
                                      [
                                        _c("div", { staticClass: "inner" }, [
                                          _c("span", [_vm._v("Users")]),
                                          _vm._v(" "),
                                          _c("h5", [
                                            _vm._v(_vm._s(_vm.data.total_users))
                                          ])
                                        ])
                                      ]
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  {
                                    staticClass:
                                      "col-lg-4 col-md-6 col-sm-6 col-xs-12"
                                  },
                                  [
                                    _c(
                                      "div",
                                      { staticClass: "employ-top-boxs" },
                                      [
                                        _c("div", { staticClass: "inner" }, [
                                          _c("span", [
                                            _vm._v("Active Challenge")
                                          ]),
                                          _vm._v(" "),
                                          _c("h5", [
                                            _vm._v(
                                              _vm._s(_vm.data.active_challenges)
                                            )
                                          ])
                                        ])
                                      ]
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  {
                                    staticClass:
                                      "col-lg-4 col-md-6 col-sm-6 col-xs-12"
                                  },
                                  [
                                    _c(
                                      "div",
                                      { staticClass: "employ-top-boxs d-none" },
                                      [
                                        _c("div", { staticClass: "inner" }, [
                                          _c("span", [_vm._v("Income")]),
                                          _vm._v(" "),
                                          _c("h5", [
                                            _vm._v(
                                              "$" + _vm._s(_vm.data.income)
                                            )
                                          ])
                                        ]),
                                        _vm._v(" "),
                                        _c("img", {
                                          attrs: {
                                            src:
                                              _vm.base_url +
                                              "/images/Admin-Homescreen_03.png",
                                            alt: ""
                                          }
                                        })
                                      ]
                                    )
                                  ]
                                )
                              ]),
                              _vm._v(" "),
                              _c(
                                "div",
                                {
                                  staticClass:
                                    "row align-items-center pt-5 pb-5"
                                },
                                [
                                  _vm._m(0),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "col-md-6" }, [
                                    _c(
                                      "select",
                                      {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.selectedYear,
                                            expression: "selectedYear"
                                          }
                                        ],
                                        staticClass: "form-control",
                                        on: {
                                          change: [
                                            function($event) {
                                              var $$selectedVal = Array.prototype.filter
                                                .call(
                                                  $event.target.options,
                                                  function(o) {
                                                    return o.selected
                                                  }
                                                )
                                                .map(function(o) {
                                                  var val =
                                                    "_value" in o
                                                      ? o._value
                                                      : o.value
                                                  return val
                                                })
                                              _vm.selectedYear = $event.target
                                                .multiple
                                                ? $$selectedVal
                                                : $$selectedVal[0]
                                            },
                                            _vm.changeYear
                                          ]
                                        }
                                      },
                                      _vm._l(_vm.allYears, function(year) {
                                        return _c(
                                          "option",
                                          { domProps: { value: year } },
                                          [_vm._v(_vm._s(year))]
                                        )
                                      }),
                                      0
                                    )
                                  ]),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "col-md-12" }, [
                                    _vm.datacollection
                                      ? _c(
                                          "div",
                                          { staticClass: "small" },
                                          [
                                            _c("line-chart", {
                                              attrs: {
                                                "chart-data":
                                                  _vm.datacollection,
                                                options: _vm.options
                                              }
                                            })
                                          ],
                                          1
                                        )
                                      : _vm._e()
                                  ]),
                                  _vm._v(" "),
                                  _vm._m(1)
                                ]
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass:
                                "col-lg-3 col-md-3 col-sm-3 col-xs-12"
                            },
                            [
                              _c(
                                "h3",
                                {
                                  staticClass:
                                    "emply-chanlng-span text-left mt-0"
                                },
                                [_vm._v("Recently Added Users")]
                              ),
                              _vm._v(" "),
                              _vm._l(_vm.data.recent_users, function(user) {
                                return _c(
                                  "div",
                                  {
                                    staticClass:
                                      "chalng-members-box box-shadow-1",
                                    on: { key: user.id }
                                  },
                                  [
                                    _c("img", {
                                      attrs: { src: user.image, alt: user.name }
                                    }),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "flex-column" }, [
                                      _c("p", {
                                        domProps: {
                                          textContent: _vm._s(user.name)
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("span", [
                                        _vm._v(
                                          "Points: " + _vm._s(user.state.points)
                                        )
                                      ])
                                    ])
                                  ]
                                )
                              })
                            ],
                            2
                          )
                        ]),
                        _vm._v(" "),
                        _vm._m(2),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "row mt-3" },
                          _vm._l(_vm.data.lead_boards, function(user, index) {
                            return _c(
                              "div",
                              { staticClass: "col-lg-4 col-md-12 col-sm-12" },
                              [
                                _c(
                                  "div",
                                  {
                                    staticClass:
                                      "chalng-members-box box-shadow-1"
                                  },
                                  [
                                    _c("img", {
                                      attrs: { src: user.image, alt: user.name }
                                    }),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "flex-column" }, [
                                      _c("p", {
                                        domProps: {
                                          textContent: _vm._s(user.name)
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("span", [
                                        _vm._v(
                                          "Points: " + _vm._s(user.state.points)
                                        )
                                      ])
                                    ]),
                                    _vm._v(" "),
                                    _c("h5", { staticClass: "postion-h5" }, [
                                      _vm._v(_vm._s(index + 1) + " "),
                                      _c("i", [
                                        _vm._v(
                                          _vm._s(
                                            _vm.ordinal_suffix_of(index + 1)
                                          )
                                        )
                                      ])
                                    ])
                                  ]
                                )
                              ]
                            )
                          }),
                          0
                        )
                      ])
                    ])
                  ]
                )
              ])
            ])
          ])
        ])
      ])
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-6" }, [
      _c("span", { staticClass: "emply-chanlng-span" }, [
        _vm._v("User Registrations")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-sm-12" }, [
      _c("span", { staticClass: "emply-chanlng-span" }, [_vm._v("Months")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-md-8 col-sm-12" }, [
        _c("h3", { staticClass: "emply-chanlng-span text-left" }, [
          _vm._v("Leadboard ")
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-3 col-sm-12 dataTables_wrapper mr-1" })
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/SettingsComponent.vue?vue&type=template&id=73524a7f&":
/*!**************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/admin/components/SettingsComponent.vue?vue&type=template&id=73524a7f& ***!
  \**************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "app-content content" },
    [
      _vm.isLoading
        ? _c("BlockUI", { attrs: { message: "Loading..." } }, [
            _c("i", { staticClass: "fa fa-cog fa-spin fa-3x fa-fw" })
          ])
        : _vm._e(),
      _vm._v(" "),
      _c("div", { staticClass: "content-wrapper" }, [
        _c("div", { staticClass: "content-body" }, [
          _c("section", { attrs: { id: "combination-charts" } }, [
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-12" }, [
                _c(
                  "div",
                  { staticClass: "card rounded p-3 admin-overview-main" },
                  [
                    _c("div", { staticClass: "row" }, [
                      _c("div", { staticClass: "col-lg-12" }, [
                        _c("div", { staticClass: "admin-chlng-setting" }, [
                          _c(
                            "form",
                            {
                              attrs: { method: "post" },
                              on: {
                                submit: function($event) {
                                  $event.preventDefault()
                                  return _vm.save()
                                }
                              }
                            },
                            [
                              _vm._l(_vm.formSettings, function(
                                setting,
                                index
                              ) {
                                return _c(
                                  "div",
                                  {
                                    key: index,
                                    staticClass: "row align-items-center"
                                  },
                                  [
                                    _c(
                                      "div",
                                      { staticClass: "col-md-5 col-sm-12" },
                                      [
                                        _c("label", [
                                          _vm._v(_vm._s(setting.title))
                                        ])
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      { staticClass: "col-md-7 col-sm-12" },
                                      [
                                        setting.type === "checkbox"
                                          ? _c("input", {
                                              directives: [
                                                {
                                                  name: "model",
                                                  rawName: "v-model",
                                                  value:
                                                    _vm.settings[setting.name],
                                                  expression:
                                                    "settings[setting.name]"
                                                }
                                              ],
                                              staticClass:
                                                "admin-chlng-setting-input",
                                              attrs: {
                                                min: "1",
                                                required: "",
                                                type: "checkbox"
                                              },
                                              domProps: {
                                                checked: Array.isArray(
                                                  _vm.settings[setting.name]
                                                )
                                                  ? _vm._i(
                                                      _vm.settings[
                                                        setting.name
                                                      ],
                                                      null
                                                    ) > -1
                                                  : _vm.settings[setting.name]
                                              },
                                              on: {
                                                change: function($event) {
                                                  var $$a =
                                                      _vm.settings[
                                                        setting.name
                                                      ],
                                                    $$el = $event.target,
                                                    $$c = $$el.checked
                                                      ? true
                                                      : false
                                                  if (Array.isArray($$a)) {
                                                    var $$v = null,
                                                      $$i = _vm._i($$a, $$v)
                                                    if ($$el.checked) {
                                                      $$i < 0 &&
                                                        _vm.$set(
                                                          _vm.settings,
                                                          setting.name,
                                                          $$a.concat([$$v])
                                                        )
                                                    } else {
                                                      $$i > -1 &&
                                                        _vm.$set(
                                                          _vm.settings,
                                                          setting.name,
                                                          $$a
                                                            .slice(0, $$i)
                                                            .concat(
                                                              $$a.slice($$i + 1)
                                                            )
                                                        )
                                                    }
                                                  } else {
                                                    _vm.$set(
                                                      _vm.settings,
                                                      setting.name,
                                                      $$c
                                                    )
                                                  }
                                                }
                                              }
                                            })
                                          : setting.type === "radio"
                                          ? _c("input", {
                                              directives: [
                                                {
                                                  name: "model",
                                                  rawName: "v-model",
                                                  value:
                                                    _vm.settings[setting.name],
                                                  expression:
                                                    "settings[setting.name]"
                                                }
                                              ],
                                              staticClass:
                                                "admin-chlng-setting-input",
                                              attrs: {
                                                min: "1",
                                                required: "",
                                                type: "radio"
                                              },
                                              domProps: {
                                                checked: _vm._q(
                                                  _vm.settings[setting.name],
                                                  null
                                                )
                                              },
                                              on: {
                                                change: function($event) {
                                                  return _vm.$set(
                                                    _vm.settings,
                                                    setting.name,
                                                    null
                                                  )
                                                }
                                              }
                                            })
                                          : _c("input", {
                                              directives: [
                                                {
                                                  name: "model",
                                                  rawName: "v-model",
                                                  value:
                                                    _vm.settings[setting.name],
                                                  expression:
                                                    "settings[setting.name]"
                                                }
                                              ],
                                              staticClass:
                                                "admin-chlng-setting-input",
                                              attrs: {
                                                min: "1",
                                                required: "",
                                                type: setting.type
                                              },
                                              domProps: {
                                                value:
                                                  _vm.settings[setting.name]
                                              },
                                              on: {
                                                input: function($event) {
                                                  if ($event.target.composing) {
                                                    return
                                                  }
                                                  _vm.$set(
                                                    _vm.settings,
                                                    setting.name,
                                                    $event.target.value
                                                  )
                                                }
                                              }
                                            }),
                                        _vm._v(" "),
                                        _c("span", [
                                          _vm._v(_vm._s(setting.postFix))
                                        ])
                                      ]
                                    )
                                  ]
                                )
                              }),
                              _vm._v(" "),
                              _c(
                                "button",
                                {
                                  staticClass: "cnt-btnn mt-5",
                                  attrs: { type: "submit" }
                                },
                                [
                                  _vm._v(
                                    "\n                                                Save Changes\n                                                "
                                  ),
                                  _vm.isLoading
                                    ? _c("i", {
                                        staticClass: "fa fa-refresh spinner"
                                      })
                                    : _vm._e()
                                ]
                              )
                            ],
                            2
                          )
                        ])
                      ])
                    ])
                  ]
                )
              ])
            ])
          ])
        ])
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/users/EditProfileComponent.vue?vue&type=template&id=7e698310&":
/*!***********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/admin/components/users/EditProfileComponent.vue?vue&type=template&id=7e698310& ***!
  \***********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "app-content content" },
    [
      _vm.isLoading
        ? _c("BlockUI", { attrs: { message: "Loading..." } }, [
            _c("i", { staticClass: "fa fa-cog fa-spin fa-3x fa-fw" })
          ])
        : _vm._e(),
      _vm._v(" "),
      _c("div", { staticClass: "content-wrapper" }, [
        _c("div", { staticClass: "content-body" }, [
          _c("section", { attrs: { id: "combination-charts" } }, [
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-12" }, [
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-md-2 col-sm-12" }),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-8 col-sm-12" }, [
                    _c(
                      "div",
                      { staticClass: "card rounded p-3 admin-overview-main" },
                      [
                        _c("div", { staticClass: "row" }, [
                          _c("div", { staticClass: "col-lg-12" }, [
                            _c(
                              "form",
                              {
                                attrs: { enctype: "multipart/form-data" },
                                on: {
                                  submit: function($event) {
                                    $event.preventDefault()
                                    return _vm.validate($event)
                                  }
                                }
                              },
                              [
                                _c(
                                  "div",
                                  { staticClass: "admin-add-user-main" },
                                  [
                                    _c(
                                      "h1",
                                      [
                                        _c(
                                          "router-link",
                                          {
                                            staticClass: "profile-back",
                                            attrs: {
                                              to: { name: "users.index" }
                                            }
                                          },
                                          [
                                            _c("i", {
                                              staticClass: "fa fa-angle-left",
                                              attrs: { "aria-hidden": "true" }
                                            }),
                                            _vm._v(
                                              " Users\n                                                            "
                                            )
                                          ]
                                        )
                                      ],
                                      1
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      { staticClass: "regi-profile-main" },
                                      [
                                        _c("img", {
                                          ref: "img",
                                          attrs: {
                                            src: _vm.personalImage
                                              ? _vm.personalImage
                                              : _vm.base_url +
                                                "/images/upload-img.jpg",
                                            id: "personal_img",
                                            alt: ""
                                          }
                                        }),
                                        _vm._v(" "),
                                        _vm._m(0),
                                        _vm._v(" "),
                                        _c("input", {
                                          ref: "profileImageInput",
                                          attrs: {
                                            type: "file",
                                            accept: "image/*",
                                            id: "upload",
                                            name: "personal_img"
                                          },
                                          on: {
                                            change: function($event) {
                                              return _vm.readURL()
                                            }
                                          }
                                        }),
                                        _vm._v(" "),
                                        _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: _vm.form.image,
                                              expression: "form.image"
                                            }
                                          ],
                                          attrs: {
                                            type: "hidden",
                                            name: "image"
                                          },
                                          domProps: { value: _vm.form.image },
                                          on: {
                                            input: function($event) {
                                              if ($event.target.composing) {
                                                return
                                              }
                                              _vm.$set(
                                                _vm.form,
                                                "image",
                                                $event.target.value
                                              )
                                            }
                                          }
                                        }),
                                        _vm._v(" "),
                                        _c("span", [
                                          _vm._v(
                                            _vm._s(_vm.errors.first("image"))
                                          )
                                        ])
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "mb-2" }, [
                                      _vm._m(1),
                                      _vm._v(" "),
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "validate",
                                            rawName: "v-validate",
                                            value: "required",
                                            expression: "'required'"
                                          },
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.form.name,
                                            expression: "form.name"
                                          }
                                        ],
                                        staticClass:
                                          "admin-chlng-fit-top-input",
                                        attrs: {
                                          type: "text",
                                          id: "name",
                                          name: "name"
                                        },
                                        domProps: { value: _vm.form.name },
                                        on: {
                                          input: function($event) {
                                            if ($event.target.composing) {
                                              return
                                            }
                                            _vm.$set(
                                              _vm.form,
                                              "name",
                                              $event.target.value
                                            )
                                          }
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("span", [
                                        _vm._v(_vm._s(_vm.errors.first("name")))
                                      ])
                                    ]),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "mb-2" }, [
                                      _vm._m(2),
                                      _vm._v(" "),
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "validate",
                                            rawName: "v-validate",
                                            value: "required|email|unique",
                                            expression:
                                              "'required|email|unique'"
                                          },
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.form.email,
                                            expression: "form.email"
                                          }
                                        ],
                                        staticClass:
                                          "admin-chlng-fit-top-input",
                                        attrs: {
                                          type: "email",
                                          id: "email",
                                          name: "email"
                                        },
                                        domProps: { value: _vm.form.email },
                                        on: {
                                          input: function($event) {
                                            if ($event.target.composing) {
                                              return
                                            }
                                            _vm.$set(
                                              _vm.form,
                                              "email",
                                              $event.target.value
                                            )
                                          }
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("span", [
                                        _vm._v(
                                          _vm._s(_vm.errors.first("email"))
                                        )
                                      ])
                                    ]),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "mb-2" }, [
                                      _vm._m(3),
                                      _vm._v(" "),
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "validate",
                                            rawName: "v-validate",
                                            value: _vm.isEdit ? "" : "required",
                                            expression: "isEdit?'':'required'"
                                          },
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.form.password,
                                            expression: "form.password"
                                          }
                                        ],
                                        ref: "password",
                                        staticClass:
                                          "admin-chlng-fit-top-input",
                                        attrs: {
                                          type: "password",
                                          id: "password",
                                          name: "password"
                                        },
                                        domProps: { value: _vm.form.password },
                                        on: {
                                          input: function($event) {
                                            if ($event.target.composing) {
                                              return
                                            }
                                            _vm.$set(
                                              _vm.form,
                                              "password",
                                              $event.target.value
                                            )
                                          }
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("span", [
                                        _vm._v(
                                          _vm._s(_vm.errors.first("password"))
                                        )
                                      ])
                                    ]),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "mb-2" }, [
                                      _vm._m(4),
                                      _vm._v(" "),
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "validate",
                                            rawName: "v-validate",
                                            value: _vm.isEdit
                                              ? ""
                                              : "required|" +
                                                "confirmed:password",
                                            expression:
                                              "isEdit?'':'required|' + 'confirmed:password'"
                                          },
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value:
                                              _vm.form.password_confirmation,
                                            expression:
                                              "form.password_confirmation"
                                          }
                                        ],
                                        staticClass:
                                          "admin-chlng-fit-top-input",
                                        attrs: {
                                          type: "password",
                                          id: "password_confirmation",
                                          name: "password_confirmation"
                                        },
                                        domProps: {
                                          value: _vm.form.password_confirmation
                                        },
                                        on: {
                                          input: function($event) {
                                            if ($event.target.composing) {
                                              return
                                            }
                                            _vm.$set(
                                              _vm.form,
                                              "password_confirmation",
                                              $event.target.value
                                            )
                                          }
                                        }
                                      }),
                                      _vm._v(" "),
                                      _vm.errors.has("password_confirmation")
                                        ? _c("span", [
                                            _vm._v(
                                              _vm._s(
                                                _vm.errors.first(
                                                  "password_confirmation"
                                                )
                                                  ? _vm.errors
                                                      .first(
                                                        "password_confirmation"
                                                      )
                                                      .split("_")
                                                      .join(" ")
                                                  : ""
                                              )
                                            )
                                          ])
                                        : _vm._e()
                                    ]),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      { staticClass: "mb-2" },
                                      [
                                        _vm._m(5),
                                        _vm._v(" "),
                                        _c("VueGooglePlaces", {
                                          staticClass:
                                            "admin-chlng-fit-top-input",
                                          attrs: {
                                            "api-key":
                                              "AIzaSyAHPUufTlBkF5NfBT3uhS9K4BbW2N-mkb4",
                                            enableGeolocation: true,
                                            enableGeocode: true,
                                            version: "3.36",
                                            placeholder: "Input your place"
                                          },
                                          on: {
                                            placechanged: _vm.onPlaceChanged,
                                            noresult: _vm.onNoResult
                                          }
                                        })
                                      ],
                                      1
                                    ),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "mb-2" }, [
                                      _vm._m(6),
                                      _vm._v(" "),
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.form.country,
                                            expression: "form.country"
                                          }
                                        ],
                                        staticClass:
                                          "admin-chlng-fit-top-input",
                                        attrs: {
                                          type: "text",
                                          id: "country",
                                          name: "country"
                                        },
                                        domProps: { value: _vm.form.country },
                                        on: {
                                          input: function($event) {
                                            if ($event.target.composing) {
                                              return
                                            }
                                            _vm.$set(
                                              _vm.form,
                                              "country",
                                              $event.target.value
                                            )
                                          }
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("span", [
                                        _vm._v(
                                          _vm._s(_vm.errors.first("country"))
                                        )
                                      ])
                                    ]),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "mb-2" }, [
                                      _vm._m(7),
                                      _vm._v(" "),
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.form.address_state,
                                            expression: "form.address_state"
                                          }
                                        ],
                                        staticClass:
                                          "admin-chlng-fit-top-input",
                                        attrs: {
                                          type: "text",
                                          id: "administrative_area_level_1",
                                          name: "address_state"
                                        },
                                        domProps: {
                                          value: _vm.form.address_state
                                        },
                                        on: {
                                          input: function($event) {
                                            if ($event.target.composing) {
                                              return
                                            }
                                            _vm.$set(
                                              _vm.form,
                                              "address_state",
                                              $event.target.value
                                            )
                                          }
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("span", [
                                        _vm._v(
                                          _vm._s(
                                            _vm.errors.first("address_state")
                                          )
                                        )
                                      ])
                                    ]),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "mb-2" }, [
                                      _vm._m(8),
                                      _vm._v(" "),
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.form.city,
                                            expression: "form.city"
                                          }
                                        ],
                                        staticClass:
                                          "admin-chlng-fit-top-input",
                                        attrs: {
                                          type: "text",
                                          id: "locality",
                                          name: "city"
                                        },
                                        domProps: { value: _vm.form.city },
                                        on: {
                                          input: function($event) {
                                            if ($event.target.composing) {
                                              return
                                            }
                                            _vm.$set(
                                              _vm.form,
                                              "city",
                                              $event.target.value
                                            )
                                          }
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("span", [
                                        _vm._v(_vm._s(_vm.errors.first("city")))
                                      ])
                                    ]),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "mb-2" }, [
                                      _vm._m(9),
                                      _vm._v(" "),
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.form.postal_code,
                                            expression: "form.postal_code"
                                          }
                                        ],
                                        staticClass:
                                          "admin-chlng-fit-top-input",
                                        attrs: {
                                          type: "text",
                                          id: "postal_code",
                                          name: "postal_code"
                                        },
                                        domProps: {
                                          value: _vm.form.postal_code
                                        },
                                        on: {
                                          input: function($event) {
                                            if ($event.target.composing) {
                                              return
                                            }
                                            _vm.$set(
                                              _vm.form,
                                              "postal_code",
                                              $event.target.value
                                            )
                                          }
                                        }
                                      })
                                    ]),
                                    _vm._v(" "),
                                    _c("div", { staticClass: "mb-2" }, [
                                      _vm._m(10),
                                      _vm._v(" "),
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "validate",
                                            rawName: "v-validate",
                                            value: {
                                              regex: /^(\+1-)?[0-9]{3}-[0-9]{3}-[0-9]{4}$/
                                            },
                                            expression:
                                              "{regex: /^(\\+1-)?[0-9]{3}-[0-9]{3}-[0-9]{4}$/}"
                                          },
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.form.contact,
                                            expression: "form.contact"
                                          }
                                        ],
                                        staticClass:
                                          "admin-chlng-fit-top-input",
                                        attrs: {
                                          type: "text",
                                          id: "number",
                                          value: "",
                                          name: "contact",
                                          placeholder: "+1-xxx-xxx-xxxx"
                                        },
                                        domProps: { value: _vm.form.contact },
                                        on: {
                                          input: function($event) {
                                            if ($event.target.composing) {
                                              return
                                            }
                                            _vm.$set(
                                              _vm.form,
                                              "contact",
                                              $event.target.value
                                            )
                                          }
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("span", [
                                        _vm._v(
                                          _vm._s(_vm.errors.first("contact"))
                                        )
                                      ])
                                    ]),
                                    _vm._v(" "),
                                    _c(
                                      "button",
                                      {
                                        staticClass: "save cnt-btnn",
                                        attrs: {
                                          type: "submit",
                                          disabled: _vm.isLoading
                                        }
                                      },
                                      [
                                        !_vm.isEdit
                                          ? _c("span", [_vm._v("Add")])
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.isEdit
                                          ? _c("span", [_vm._v("Save")])
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm.isLoading
                                          ? _c("i", {
                                              staticClass:
                                                "fa fa-refresh spinner"
                                            })
                                          : _vm._e()
                                      ]
                                    )
                                  ]
                                )
                              ]
                            )
                          ])
                        ])
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-2 col-sm-12" })
                ])
              ])
            ])
          ])
        ])
      ])
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "label",
      {
        staticClass: "uplrd-img-btn",
        staticStyle: { "margin-top": "0" },
        attrs: { for: "upload" }
      },
      [
        _c("i", {
          staticClass: "fa fa-camera",
          attrs: { "aria-hidden": "true" }
        })
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [
      _c("i", { staticClass: "fa fa-user-circle" }),
      _vm._v("Name *")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [
      _c("i", { staticClass: "fa fa-envelope" }),
      _vm._v("Email Address *")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [
      _c("i", { staticClass: "fa fa-key" }),
      _vm._v("Enter Password *")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [
      _c("i", { staticClass: "fa fa-key" }),
      _vm._v("Password Confirmation*")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [
      _c("i", { staticClass: "fa fa-map-marker" }),
      _vm._v(
        "Address\n                                                            "
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [
      _c("i", { staticClass: "fa fa-map-marker" }),
      _vm._v("Country")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [
      _c("i", { staticClass: "fa fa-map-marker" }),
      _vm._v("State")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [
      _c("i", { staticClass: "fa fa-map-marker" }),
      _vm._v("City ")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [
      _c("i", { staticClass: "fa fa-map-marker" }),
      _vm._v("Zip Code ")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [
      _c("i", { staticClass: "fa fa-phone" }),
      _vm._v("Contact number:")
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/admin/components/DashboardComponent.vue":
/*!**************************************************************!*\
  !*** ./resources/js/admin/components/DashboardComponent.vue ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _DashboardComponent_vue_vue_type_template_id_6128fbb4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./DashboardComponent.vue?vue&type=template&id=6128fbb4& */ "./resources/js/admin/components/DashboardComponent.vue?vue&type=template&id=6128fbb4&");
/* harmony import */ var _DashboardComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./DashboardComponent.vue?vue&type=script&lang=js& */ "./resources/js/admin/components/DashboardComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _DashboardComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./DashboardComponent.vue?vue&type=style&index=0&lang=css& */ "./resources/js/admin/components/DashboardComponent.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _DashboardComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _DashboardComponent_vue_vue_type_template_id_6128fbb4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _DashboardComponent_vue_vue_type_template_id_6128fbb4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/admin/components/DashboardComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/admin/components/DashboardComponent.vue?vue&type=script&lang=js&":
/*!***************************************************************************************!*\
  !*** ./resources/js/admin/components/DashboardComponent.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DashboardComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./DashboardComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/DashboardComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DashboardComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/admin/components/DashboardComponent.vue?vue&type=style&index=0&lang=css&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/admin/components/DashboardComponent.vue?vue&type=style&index=0&lang=css& ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_DashboardComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--7-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./DashboardComponent.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/DashboardComponent.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_DashboardComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_DashboardComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_DashboardComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_DashboardComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_DashboardComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/admin/components/DashboardComponent.vue?vue&type=template&id=6128fbb4&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/admin/components/DashboardComponent.vue?vue&type=template&id=6128fbb4& ***!
  \*********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DashboardComponent_vue_vue_type_template_id_6128fbb4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./DashboardComponent.vue?vue&type=template&id=6128fbb4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/DashboardComponent.vue?vue&type=template&id=6128fbb4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DashboardComponent_vue_vue_type_template_id_6128fbb4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DashboardComponent_vue_vue_type_template_id_6128fbb4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/admin/components/SettingsComponent.vue":
/*!*************************************************************!*\
  !*** ./resources/js/admin/components/SettingsComponent.vue ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _SettingsComponent_vue_vue_type_template_id_73524a7f___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SettingsComponent.vue?vue&type=template&id=73524a7f& */ "./resources/js/admin/components/SettingsComponent.vue?vue&type=template&id=73524a7f&");
/* harmony import */ var _SettingsComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SettingsComponent.vue?vue&type=script&lang=js& */ "./resources/js/admin/components/SettingsComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _SettingsComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _SettingsComponent_vue_vue_type_template_id_73524a7f___WEBPACK_IMPORTED_MODULE_0__["render"],
  _SettingsComponent_vue_vue_type_template_id_73524a7f___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/admin/components/SettingsComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/admin/components/SettingsComponent.vue?vue&type=script&lang=js&":
/*!**************************************************************************************!*\
  !*** ./resources/js/admin/components/SettingsComponent.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SettingsComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./SettingsComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/SettingsComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SettingsComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/admin/components/SettingsComponent.vue?vue&type=template&id=73524a7f&":
/*!********************************************************************************************!*\
  !*** ./resources/js/admin/components/SettingsComponent.vue?vue&type=template&id=73524a7f& ***!
  \********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SettingsComponent_vue_vue_type_template_id_73524a7f___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./SettingsComponent.vue?vue&type=template&id=73524a7f& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/SettingsComponent.vue?vue&type=template&id=73524a7f&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SettingsComponent_vue_vue_type_template_id_73524a7f___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SettingsComponent_vue_vue_type_template_id_73524a7f___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/admin/components/UserStateChartComponent.js":
/*!******************************************************************!*\
  !*** ./resources/js/admin/components/UserStateChartComponent.js ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");

var reactiveProp = vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["mixins"].reactiveProp;
/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Bar"],
  mixins: [reactiveProp],
  props: ['options'],
  mounted: function mounted() {
    // this.chartData is created in the mixin.
    // If you want to pass options please create a local options object
    this.renderChart(this.chartData, this.options);
  }
});

/***/ }),

/***/ "./resources/js/admin/components/users/EditProfileComponent.vue":
/*!**********************************************************************!*\
  !*** ./resources/js/admin/components/users/EditProfileComponent.vue ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _EditProfileComponent_vue_vue_type_template_id_7e698310___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./EditProfileComponent.vue?vue&type=template&id=7e698310& */ "./resources/js/admin/components/users/EditProfileComponent.vue?vue&type=template&id=7e698310&");
/* harmony import */ var _EditProfileComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./EditProfileComponent.vue?vue&type=script&lang=js& */ "./resources/js/admin/components/users/EditProfileComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _EditProfileComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _EditProfileComponent_vue_vue_type_template_id_7e698310___WEBPACK_IMPORTED_MODULE_0__["render"],
  _EditProfileComponent_vue_vue_type_template_id_7e698310___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/admin/components/users/EditProfileComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/admin/components/users/EditProfileComponent.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/admin/components/users/EditProfileComponent.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EditProfileComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./EditProfileComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/users/EditProfileComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EditProfileComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/admin/components/users/EditProfileComponent.vue?vue&type=template&id=7e698310&":
/*!*****************************************************************************************************!*\
  !*** ./resources/js/admin/components/users/EditProfileComponent.vue?vue&type=template&id=7e698310& ***!
  \*****************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditProfileComponent_vue_vue_type_template_id_7e698310___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./EditProfileComponent.vue?vue&type=template&id=7e698310& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/users/EditProfileComponent.vue?vue&type=template&id=7e698310&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditProfileComponent_vue_vue_type_template_id_7e698310___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditProfileComponent_vue_vue_type_template_id_7e698310___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);