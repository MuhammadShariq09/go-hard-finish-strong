(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["single-polling-request"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/pooling-requests/ShowComponent.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/admin/components/pooling-requests/ShowComponent.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['challengeData'],
  data: function data() {
    return {
      base_url: window.base_url,
      isLoading: true,
      challenge: {},
      userData: false,
      winnerId: undefined
    };
  },
  mounted: function mounted() {
    this.loadPoolingRequest();
  },
  methods: {
    seconds_to_days_hours_mins_secs_str: function seconds_to_days_hours_mins_secs_str(exerciseIndex, userid) {
      var video = this.$refs["video".concat(exerciseIndex, "-").concat(userid)][0];
      var seconds = video.duration;
      var days = Math.floor(seconds / (24 * 60 * 60));
      seconds -= days * (24 * 60 * 60);
      var hours = Math.floor(seconds / (60 * 60));
      seconds -= hours * (60 * 60);
      var minutes = Math.floor(seconds / 60);
      seconds -= minutes * 60;
      var d = 0 < days ? days + " day, " : "";
      var h = 0 < hours ? hours + " hours, " : "";
      var m = 0 < minutes ? minutes + " min, " : "";
      var s = 0 < seconds ? Math.floor(seconds) + " sec" : ""; // this.challenge.challenge.exercises[exerciseIndex].time = ;

      video.closest('.modal.fade').nextElementSibling.getElementsByTagName('span')[0].innerHTML = "Time: " + d + h + m + " " + s + ""; // video.parentElement.parentElement.parentElement.parentElement.nextElementSibling
    },
    loadPoolingRequest: function loadPoolingRequest() {
      var _this = this;

      if (this.challengeData) {
        this.challenge = this.challengeData;
        this.userData = true;
        this.isLoading = false;
      } else {
        this.isLoading = true;
        axios.get("/pooling-requests/".concat(this.$route.params.id)).then(function (data) {
          _this.isLoading = false;
          _this.challenge = data.data;
          _this.challenge.challenge = JSON.parse(_this.challenge.challenge);
        });
      }
    },
    declareWinner: function declareWinner() {
      var _this2 = this;

      if (!this.winnerId) {
        this.$toastr.warning("Please Select Winner", "Warning!", {});
        return;
      }

      this.$dialog.confirm("Are you sure to declare him winner").then(function (dialog) {
        axios.post("/pooling-requests/declare-result", {
          challenge_id: _this2.challenge.id,
          winner_id: _this2.winnerId
        }).then(function (d) {
          if (_this2.winnerId === _this2.challenge.sender.id) {
            _this2.challenge.sender_result = "WINNER";
            _this2.challenge.recipient_result = "DEFEAT";
          } else {
            _this2.challenge.recipient_result = "WINNER";
            _this2.challenge.sender_result = "DEFEAT";
          }

          dialog.close();

          _this2.$toastr.success('Declaration is successful', 'Success', {});
        })["catch"](function (d) {
          dialog.close();
        });
      });
    },
    openVideoModal: function openVideoModal(videoId) {
      $("#".concat(videoId)).modal('show');
    }
  },
  computed: {
    challengerVoteCount: function challengerVoteCount() {
      var _this3 = this;

      return this.challenge.votes.filter(function (v) {
        return v.vote_for === _this3.challenge.sender.id;
      }).length;
    },
    opponentVoteCount: function opponentVoteCount() {
      var _this4 = this;

      return this.challenge.votes.filter(function (v) {
        return v.vote_for === _this4.challenge.recipient.id;
      }).length;
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/pooling-requests/ShowComponent.vue?vue&type=style&index=0&lang=css&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/admin/components/pooling-requests/ShowComponent.vue?vue&type=style&index=0&lang=css& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.modal-dialog {\n    max-width: 800px;\n}\n.modal-header {\n    position: absolute;\n    background: transparent;\n    z-index: 11;\n    right: 0;\n    color: black;\n    border: none;\n}\n.chalng-vid-box button span {\n    color: black;\n    font-size: 27px;\n    background: #00000040;\n    width: 30px;\n    height: 30px;\n    line-height: 30px;\n    border-radius: 50%;\n}\n.modal-body {\n    padding: 0;\n}\n.modal-content {\n    background: transparent;\n    border: none;\n    overflow: hidden;\n}\n.modal-dialog {\n    max-width: unset;\n    display: table;\n}\ninput[type=radio]+label {\n    color: transparent;\n    width: 20px;\n    height: 20px;\n    background: rgb(255, 255, 255);\n    border-radius: 50%;\n    border: 4px solid #ccc;\n    cursor: pointer;\n    position: relative;\n}\ninput[type=radio]+label.selected {\n    border: 4px solid #a40123;\n}\ninput[type=radio]+label.selected:after {\n    content: \"\";\n    background:  #a40123;\n    position:absolute;\n    width: 5px;\n    height: 5px;\n    top: 50%;\n    left: 50%;\n    border-radius: 50%;\n    -webkit-transform: translate(-50%, -50%);\n            transform: translate(-50%, -50%);\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/pooling-requests/ShowComponent.vue?vue&type=style&index=0&lang=css&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/admin/components/pooling-requests/ShowComponent.vue?vue&type=style&index=0&lang=css& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader??ref--7-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ShowComponent.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/pooling-requests/ShowComponent.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/pooling-requests/ShowComponent.vue?vue&type=template&id=452258f2&":
/*!***************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/admin/components/pooling-requests/ShowComponent.vue?vue&type=template&id=452258f2& ***!
  \***************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { class: { "app-content content": !_vm.userData } },
    [
      _vm.isLoading
        ? _c("BlockUI", { attrs: { message: "Loading..." } }, [
            _c("i", { staticClass: "fa fa-cog fa-spin fa-3x fa-fw" })
          ])
        : _vm._e(),
      _vm._v(" "),
      !_vm.isLoading
        ? _c("div", { staticClass: "content-wrapper" }, [
            _c("div", { staticClass: "content-body" }, [
              _c("section", { attrs: { id: "combination-charts" } }, [
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-lg-12" }, [
                    _c(
                      "div",
                      {
                        class: {
                          card: !_vm.userData,
                          rounded: !_vm.userData,
                          "p-3": !_vm.userData,
                          "admin-overview-main": !_vm.userData
                        }
                      },
                      [
                        !_vm.userData
                          ? _c(
                              "h1",
                              [
                                _c(
                                  "router-link",
                                  {
                                    staticClass: "profile-back",
                                    attrs: {
                                      to: { name: "pooling.requests.index" }
                                    }
                                  },
                                  [
                                    _c("i", {
                                      staticClass: "fa fa-angle-left",
                                      attrs: { "aria-hidden": "true" }
                                    })
                                  ]
                                ),
                                _vm._v(
                                  " Polling Details\n                            "
                                )
                              ],
                              1
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        !_vm.userData
                          ? _c(
                              "h3",
                              {
                                staticClass: "emply-chanlng-span text-left pb-3"
                              },
                              [_vm._v("Lightweight Run challenge ")]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _c("div", { staticClass: "row" }, [
                          _c(
                            "div",
                            {
                              staticClass:
                                "col-lg-6 col-md-12 col-sm-12 col-xs-12 px-3 bord-right"
                            },
                            [
                              _c(
                                "div",
                                { staticClass: "chanlg-detail-vid-top" },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass: "d-flex align-items-center"
                                    },
                                    [
                                      _c("img", {
                                        attrs: {
                                          src: _vm.challenge.recipient.image,
                                          alt: ""
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c(
                                        "div",
                                        { staticClass: "flex-column" },
                                        [
                                          _c("p", { staticClass: "name" }, [
                                            _vm._v(
                                              _vm._s(
                                                _vm.challenge.recipient.name
                                              )
                                            )
                                          ]),
                                          _vm._v(" "),
                                          _c("span", [_vm._v("Opponent")])
                                        ]
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("h5", [
                                    _vm._v(
                                      "Total votes: " +
                                        _vm._s(_vm.opponentVoteCount)
                                    )
                                  ])
                                ]
                              ),
                              _vm._v(" "),
                              _vm._l(
                                _vm.challenge.challenge.exercises,
                                function(exercise, index) {
                                  return _c(
                                    "div",
                                    {
                                      staticClass: "chalng-vid-box",
                                      on: { key: exercise.id }
                                    },
                                    [
                                      exercise.uploadedVideos &&
                                      exercise.uploadedVideos[
                                        _vm.challenge.recipient.id
                                      ]
                                        ? _c("img", {
                                            staticStyle: { cursor: "pointer" },
                                            attrs: {
                                              src:
                                                _vm.base_url +
                                                "/images/user-chlng-video1.png",
                                              alt: exercise.title
                                            },
                                            on: {
                                              click: function($event) {
                                                return _vm.openVideoModal(
                                                  "video" +
                                                    index +
                                                    "-" +
                                                    _vm.challenge.recipient.id
                                                )
                                              }
                                            }
                                          })
                                        : _vm._e(),
                                      _vm._v(" "),
                                      _c(
                                        "div",
                                        {
                                          staticClass: "modal fade",
                                          attrs: {
                                            id:
                                              "video" +
                                              index +
                                              "-" +
                                              _vm.challenge.recipient.id,
                                            tabindex: "-1",
                                            role: "dialog",
                                            "aria-labelledby":
                                              "exampleModalLabel",
                                            "aria-hidden": "true"
                                          }
                                        },
                                        [
                                          _c(
                                            "div",
                                            {
                                              staticClass: "modal-dialog",
                                              attrs: { role: "document" }
                                            },
                                            [
                                              _c(
                                                "div",
                                                {
                                                  staticClass: "modal-content"
                                                },
                                                [
                                                  _vm._m(0, true),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass: "modal-body"
                                                    },
                                                    [
                                                      exercise.uploadedVideos &&
                                                      exercise.uploadedVideos[
                                                        _vm.challenge.recipient
                                                          .id
                                                      ]
                                                        ? _c("video", {
                                                            ref:
                                                              "video" +
                                                              index +
                                                              "-" +
                                                              _vm.challenge
                                                                .recipient.id,
                                                            refInFor: true,
                                                            staticClass:
                                                              "img-fluid img-thumbnails",
                                                            attrs: {
                                                              controls: "",
                                                              src:
                                                                exercise
                                                                  .uploadedVideos[
                                                                  _vm.challenge
                                                                    .recipient
                                                                    .id
                                                                ]
                                                            },
                                                            on: {
                                                              loadedmetadata: function(
                                                                $event
                                                              ) {
                                                                return _vm.seconds_to_days_hours_mins_secs_str(
                                                                  index,
                                                                  _vm.challenge
                                                                    .recipient
                                                                    .id
                                                                )
                                                              }
                                                            }
                                                          })
                                                        : _vm._e()
                                                    ]
                                                  )
                                                ]
                                              )
                                            ]
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "div",
                                        {
                                          staticClass:
                                            "d-flex flex-row justify-content-between mt-1"
                                        },
                                        [
                                          _c(
                                            "div",
                                            { staticClass: "flex-column" },
                                            [
                                              _c("p", [
                                                _vm._v(_vm._s(exercise.title))
                                              ]),
                                              _vm._v(" "),
                                              _c("h5", [
                                                _vm._v(
                                                  "Reps: " +
                                                    _vm._s(exercise.repeat)
                                                )
                                              ])
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c("span", [
                                            _vm._v("Time: "),
                                            exercise.time
                                              ? _c("span", [
                                                  _vm._v(_vm._s(exercise.time))
                                                ])
                                              : _vm._e()
                                          ])
                                        ]
                                      )
                                    ]
                                  )
                                }
                              ),
                              _vm._v(" "),
                              !_vm.userData &&
                              !_vm.challenge.sender_result &&
                              !_vm.challenge.recipient_result
                                ? _c(
                                    "div",
                                    {
                                      staticClass:
                                        "polling-history-details-bg skin  skin-flat"
                                    },
                                    [
                                      _c("h5", [_vm._v("Winner")]),
                                      _vm._v(" "),
                                      _c(
                                        "div",
                                        { staticClass: "position-relative" },
                                        [
                                          _c("img", {
                                            staticClass: "main-bg",
                                            attrs: {
                                              src:
                                                _vm.challenge.recipient.image,
                                              alt: _vm.challenge.recipient.name
                                            }
                                          })
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c("h6", [
                                        _vm._v(
                                          _vm._s(_vm.challenge.recipient.name)
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c("fieldset", [
                                        _c(
                                          "div",
                                          {
                                            staticClass: "iradio_flat-green",
                                            staticStyle: {
                                              position: "relative"
                                            }
                                          },
                                          [
                                            _c("input", {
                                              directives: [
                                                {
                                                  name: "model",
                                                  rawName: "v-model",
                                                  value: _vm.winnerId,
                                                  expression: "winnerId"
                                                }
                                              ],
                                              staticStyle: {
                                                position: "absolute",
                                                opacity: "0"
                                              },
                                              attrs: {
                                                type: "radio",
                                                name: "input-radio-4",
                                                id:
                                                  "input-radio-" +
                                                  _vm.challenge.recipient.id
                                              },
                                              domProps: {
                                                checked: _vm._q(
                                                  _vm.winnerId,
                                                  null
                                                )
                                              },
                                              on: {
                                                change: function($event) {
                                                  _vm.winnerId = null
                                                }
                                              }
                                            }),
                                            _vm._v(" "),
                                            _c(
                                              "label",
                                              {
                                                class: {
                                                  selected:
                                                    _vm.challenge.recipient
                                                      .id === _vm.winnerId
                                                },
                                                on: {
                                                  click: function($event) {
                                                    _vm.winnerId =
                                                      _vm.challenge.recipient.id
                                                  }
                                                }
                                              },
                                              [_vm._v("label")]
                                            )
                                          ]
                                        )
                                      ])
                                    ]
                                  )
                                : _vm._e()
                            ],
                            2
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass:
                                "col-lg-6 col-md-12 col-sm-12 col-xs-12 px-3"
                            },
                            [
                              _c(
                                "div",
                                { staticClass: "chanlg-detail-vid-top" },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass: "d-flex align-items-center"
                                    },
                                    [
                                      _c("img", {
                                        attrs: {
                                          src: _vm.challenge.sender.image,
                                          alt: ""
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c(
                                        "div",
                                        { staticClass: "flex-column" },
                                        [
                                          _c("p", { staticClass: "name" }, [
                                            _vm._v(
                                              _vm._s(_vm.challenge.sender.name)
                                            )
                                          ]),
                                          _vm._v(" "),
                                          _c("span", [_vm._v("Challenger")])
                                        ]
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("h5", [
                                    _vm._v(
                                      "Total votes: " +
                                        _vm._s(_vm.challengerVoteCount)
                                    )
                                  ])
                                ]
                              ),
                              _vm._v(" "),
                              _vm._l(
                                _vm.challenge.challenge.exercises,
                                function(exercise, index) {
                                  return _c(
                                    "div",
                                    {
                                      staticClass: "chalng-vid-box",
                                      on: { key: exercise.id }
                                    },
                                    [
                                      exercise.uploadedVideos &&
                                      exercise.uploadedVideos[
                                        _vm.challenge.sender.id
                                      ]
                                        ? _c("img", {
                                            staticStyle: { cursor: "pointer" },
                                            attrs: {
                                              src:
                                                _vm.base_url +
                                                "/images/user-chlng-video1.png",
                                              alt: exercise.title
                                            },
                                            on: {
                                              click: function($event) {
                                                return _vm.openVideoModal(
                                                  "video" +
                                                    index +
                                                    "-" +
                                                    _vm.challenge.sender.id
                                                )
                                              }
                                            }
                                          })
                                        : _vm._e(),
                                      _vm._v(" "),
                                      _c(
                                        "div",
                                        {
                                          staticClass: "modal fade",
                                          attrs: {
                                            id:
                                              "video" +
                                              index +
                                              "-" +
                                              _vm.challenge.sender.id,
                                            tabindex: "-1",
                                            role: "dialog",
                                            "aria-labelledby":
                                              "exampleModalLabel",
                                            "aria-hidden": "true"
                                          }
                                        },
                                        [
                                          _c(
                                            "div",
                                            {
                                              staticClass: "modal-dialog",
                                              attrs: { role: "document" }
                                            },
                                            [
                                              _c(
                                                "div",
                                                {
                                                  staticClass: "modal-content"
                                                },
                                                [
                                                  _vm._m(1, true),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass: "modal-body"
                                                    },
                                                    [
                                                      exercise.uploadedVideos &&
                                                      exercise.uploadedVideos[
                                                        _vm.challenge.sender.id
                                                      ]
                                                        ? _c("video", {
                                                            ref:
                                                              "video" +
                                                              index +
                                                              "-" +
                                                              _vm.challenge
                                                                .sender.id,
                                                            refInFor: true,
                                                            staticClass:
                                                              "img-fluid img-thumbnails",
                                                            attrs: {
                                                              controls: "",
                                                              src:
                                                                exercise
                                                                  .uploadedVideos[
                                                                  _vm.challenge
                                                                    .sender.id
                                                                ]
                                                            },
                                                            on: {
                                                              loadedmetadata: function(
                                                                $event
                                                              ) {
                                                                return _vm.seconds_to_days_hours_mins_secs_str(
                                                                  index,
                                                                  _vm.challenge
                                                                    .sender.id
                                                                )
                                                              }
                                                            }
                                                          })
                                                        : _vm._e()
                                                    ]
                                                  )
                                                ]
                                              )
                                            ]
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "div",
                                        {
                                          staticClass:
                                            "d-flex flex-row justify-content-between mt-1"
                                        },
                                        [
                                          _c(
                                            "div",
                                            { staticClass: "flex-column" },
                                            [
                                              _c("p", [
                                                _vm._v(_vm._s(exercise.title))
                                              ]),
                                              _vm._v(" "),
                                              _c("h5", [
                                                _vm._v(
                                                  "Reps: " +
                                                    _vm._s(exercise.repeat)
                                                )
                                              ])
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c("span", [
                                            _vm._v("Time: "),
                                            exercise.time
                                              ? _c("span", [
                                                  _vm._v(_vm._s(exercise.time))
                                                ])
                                              : _vm._e()
                                          ])
                                        ]
                                      )
                                    ]
                                  )
                                }
                              ),
                              _vm._v(" "),
                              !_vm.userData &&
                              !_vm.challenge.sender_result &&
                              !_vm.challenge.recipient_result
                                ? _c(
                                    "div",
                                    {
                                      staticClass:
                                        "polling-history-details-bg skin  skin-flat"
                                    },
                                    [
                                      _c("h5", [_vm._v("Winner")]),
                                      _vm._v(" "),
                                      _c(
                                        "div",
                                        { staticClass: "position-relative" },
                                        [
                                          _c("img", {
                                            staticClass: "main-bg",
                                            attrs: {
                                              src: _vm.challenge.sender.image,
                                              alt: _vm.challenge.sender.name
                                            }
                                          })
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c("h6", [
                                        _vm._v(
                                          _vm._s(_vm.challenge.sender.name)
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c("fieldset", [
                                        _c(
                                          "div",
                                          {
                                            staticClass: "iradio_flat-green",
                                            staticStyle: {
                                              position: "relative"
                                            }
                                          },
                                          [
                                            _c("input", {
                                              directives: [
                                                {
                                                  name: "model",
                                                  rawName: "v-model",
                                                  value: _vm.winnerId,
                                                  expression: "winnerId"
                                                }
                                              ],
                                              staticStyle: {
                                                position: "absolute",
                                                opacity: "0"
                                              },
                                              attrs: {
                                                type: "radio",
                                                name: "input-radio-4",
                                                id:
                                                  "input-radio-" +
                                                  _vm.challenge.sender.id
                                              },
                                              domProps: {
                                                checked: _vm._q(
                                                  _vm.winnerId,
                                                  null
                                                )
                                              },
                                              on: {
                                                change: function($event) {
                                                  _vm.winnerId = null
                                                }
                                              }
                                            }),
                                            _vm._v(" "),
                                            _c(
                                              "label",
                                              {
                                                class: {
                                                  selected:
                                                    _vm.challenge.sender.id ===
                                                    _vm.winnerId
                                                },
                                                on: {
                                                  click: function($event) {
                                                    _vm.winnerId =
                                                      _vm.challenge.sender.id
                                                  }
                                                }
                                              },
                                              [_vm._v("label")]
                                            )
                                          ]
                                        )
                                      ])
                                    ]
                                  )
                                : _vm._e()
                            ],
                            2
                          )
                        ]),
                        _vm._v(" "),
                        !_vm.userData
                          ? _c("div", { staticClass: "row text-center" }, [
                              _vm.challenge.sender_result &&
                              _vm.challenge.recipient_result
                                ? _c(
                                    "div",
                                    {
                                      staticClass:
                                        "polling-history-details-bg d-inline-block"
                                    },
                                    [
                                      _vm.challenge.status === 5
                                        ? _c(
                                            "div",
                                            {
                                              staticClass: "position-relative"
                                            },
                                            [
                                              _vm.challenge.recipient_result ===
                                              "WINNER"
                                                ? _c("img", {
                                                    staticClass: "main-bg",
                                                    attrs: {
                                                      src:
                                                        _vm.challenge.recipient
                                                          .image,
                                                      alt:
                                                        _vm.challenge.recipient
                                                          .name
                                                    }
                                                  })
                                                : _vm._e(),
                                              _vm._v(" "),
                                              _vm.challenge.sender_result ===
                                              "WINNER"
                                                ? _c("img", {
                                                    staticClass: "main-bg",
                                                    attrs: {
                                                      src:
                                                        _vm.challenge.sender
                                                          .image,
                                                      alt:
                                                        _vm.challenge.sender
                                                          .name
                                                    }
                                                  })
                                                : _vm._e(),
                                              _vm._v(" "),
                                              _c("img", {
                                                staticClass: "trophy-img",
                                                attrs: {
                                                  src:
                                                    _vm.base_url +
                                                    "/images/polling-history-details-_03.png",
                                                  alt: ""
                                                }
                                              })
                                            ]
                                          )
                                        : _vm._e(),
                                      _vm._v(" "),
                                      _vm.challenge.recipient_result ===
                                      "WINNER"
                                        ? _c("h6", [
                                            _vm._v(
                                              _vm._s(
                                                _vm.challenge.recipient.name
                                              )
                                            )
                                          ])
                                        : _vm._e(),
                                      _vm._v(" "),
                                      _vm.challenge.sender_result === "WINNER"
                                        ? _c("h6", [
                                            _vm._v(
                                              _vm._s(_vm.challenge.sender.name)
                                            )
                                          ])
                                        : _vm._e()
                                    ]
                                  )
                                : _vm._e()
                            ])
                          : _vm._e(),
                        _vm._v(" "),
                        !_vm.userData
                          ? _c("div", { staticClass: "row" }, [
                              _c("div", { staticClass: "col" }, [
                                !_vm.challenge.sender_result &&
                                !_vm.challenge.recipient_result
                                  ? _c(
                                      "button",
                                      {
                                        staticClass: "cnt-btnn",
                                        on: {
                                          click: function($event) {
                                            $event.preventDefault()
                                            return _vm.declareWinner()
                                          }
                                        }
                                      },
                                      [_vm._v("Declared Winner")]
                                    )
                                  : _vm._e()
                              ])
                            ])
                          : _vm._e()
                      ]
                    )
                  ])
                ])
              ])
            ])
          ])
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-header" }, [
      _c(
        "button",
        {
          staticClass: "close",
          attrs: {
            type: "button",
            "data-dismiss": "modal",
            "aria-label": "Close"
          }
        },
        [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-header" }, [
      _c(
        "button",
        {
          staticClass: "close",
          attrs: {
            type: "button",
            "data-dismiss": "modal",
            "aria-label": "Close"
          }
        },
        [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/admin/components/pooling-requests/ShowComponent.vue":
/*!**************************************************************************!*\
  !*** ./resources/js/admin/components/pooling-requests/ShowComponent.vue ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ShowComponent_vue_vue_type_template_id_452258f2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ShowComponent.vue?vue&type=template&id=452258f2& */ "./resources/js/admin/components/pooling-requests/ShowComponent.vue?vue&type=template&id=452258f2&");
/* harmony import */ var _ShowComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ShowComponent.vue?vue&type=script&lang=js& */ "./resources/js/admin/components/pooling-requests/ShowComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _ShowComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ShowComponent.vue?vue&type=style&index=0&lang=css& */ "./resources/js/admin/components/pooling-requests/ShowComponent.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _ShowComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ShowComponent_vue_vue_type_template_id_452258f2___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ShowComponent_vue_vue_type_template_id_452258f2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/admin/components/pooling-requests/ShowComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/admin/components/pooling-requests/ShowComponent.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************!*\
  !*** ./resources/js/admin/components/pooling-requests/ShowComponent.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ShowComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/pooling-requests/ShowComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/admin/components/pooling-requests/ShowComponent.vue?vue&type=style&index=0&lang=css&":
/*!***********************************************************************************************************!*\
  !*** ./resources/js/admin/components/pooling-requests/ShowComponent.vue?vue&type=style&index=0&lang=css& ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader??ref--7-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ShowComponent.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/pooling-requests/ShowComponent.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/admin/components/pooling-requests/ShowComponent.vue?vue&type=template&id=452258f2&":
/*!*********************************************************************************************************!*\
  !*** ./resources/js/admin/components/pooling-requests/ShowComponent.vue?vue&type=template&id=452258f2& ***!
  \*********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_template_id_452258f2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ShowComponent.vue?vue&type=template&id=452258f2& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/pooling-requests/ShowComponent.vue?vue&type=template&id=452258f2&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_template_id_452258f2___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_template_id_452258f2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);