(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["index-ftfp-challenges"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/challenges/IndexComponent.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/admin/components/challenges/IndexComponent.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      base_url: window.base_url,
      isLoading: true,
      path: 'East',
      level: '1',
      challenges: {},
      activeChallenges: [],
      per_page: 10,
      search_term: ''
    };
  },
  mounted: function mounted() {
    this.loadChallenges();
    this.loadActiveChallenges();
    var path = window.localStorage.getItem('path');
    var level = window.localStorage.getItem('level');
    if (path) this.path = path;
    if (level) this.level = level;
  },
  methods: {
    lengthChanged: function lengthChanged(e) {
      console.log(e);
    },
    loadChallenges: function loadChallenges() {
      var _this = this;

      var page = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
      this.isLoading = true;
      axios.get("/challenges?page=".concat(page, "&path=").concat(this.path, "&level=").concat(this.level, "&per_page=").concat(this.per_page, "&search_term=").concat(this.search_term)).then(function (data) {
        _this.challenges = data.data;
        _this.isLoading = false;
      })["catch"](function (_) {
        return console.log(_.response);
      });
    },
    loadActiveChallenges: function loadActiveChallenges() {
      var page = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
      this.isLoading = true; // axios.get(`/active-challenges?page=${page}&path=${this.path}&level=${this.level}`)
      //     .then(data => {
      //         this.activeChallenges = data.data;
      //         this.isLoading = false;
      //     }).catch(_ => this.isLoading = false);
    },
    deleteChallenge: function deleteChallenge(id) {
      var _this2 = this;

      this.$dialog.confirm('Do you want to delete this challenge.?', {
        okText: 'Delete'
      }).then(function (dialog) {
        axios["delete"]("/challenges/".concat(id)).then(function (data) {
          _this2.loadChallenges();

          _this2.$toastr.success('Challenge Deleted Successfully', 'Success', {});

          dialog.close();
        });
      }); // axios.delete().then()
    }
  },
  watch: {
    'path': function path() {
      if (this.path !== 'east') {
        this.loadChallenges();
        this.search_term = '';
      }

      window.localStorage.setItem('path', this.path);
    },
    'level': function level() {
      this.loadChallenges();
      this.search_term = '';
      window.localStorage.setItem('level', this.level);
    },
    'per_page': function per_page() {
      this.loadChallenges();
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/challenges/IndexComponent.vue?vue&type=style&index=0&lang=css&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/admin/components/challenges/IndexComponent.vue?vue&type=style&index=0&lang=css& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\ntable {\n    width: 100%;\n    border-spacing: 0 26px;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/challenges/IndexComponent.vue?vue&type=style&index=0&lang=css&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/admin/components/challenges/IndexComponent.vue?vue&type=style&index=0&lang=css& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader??ref--7-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./IndexComponent.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/challenges/IndexComponent.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/challenges/IndexComponent.vue?vue&type=template&id=6e3e0d6f&":
/*!**********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/admin/components/challenges/IndexComponent.vue?vue&type=template&id=6e3e0d6f& ***!
  \**********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "app-content content" },
    [
      _vm.isLoading
        ? _c("BlockUI", { attrs: { message: "Loading..." } }, [
            _c("i", { staticClass: "fa fa-cog fa-spin fa-3x fa-fw" })
          ])
        : _vm._e(),
      _vm._v(" "),
      _c("div", { staticClass: "content-wrapper" }, [
        _c("div", { staticClass: "content-body" }, [
          _c("section", { attrs: { id: "combination-charts" } }, [
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-12" }, [
                _c(
                  "div",
                  { staticClass: "card rounded p-3 admin-overview-main" },
                  [
                    _c("div", { staticClass: "row" }, [
                      _c("div", { staticClass: "col-lg-12" }, [
                        _c("h1", [_vm._v("Follow the Fit Path Challenges")]),
                        _vm._v(" "),
                        _c("div", { staticClass: "row" }, [
                          _c("div", { staticClass: "col-md-6 col-sm-12" }, [
                            _c("div", { staticClass: "admin-top-red-nav" }, [
                              _c("ul", [
                                _c("li", [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.path,
                                        expression: "path"
                                      }
                                    ],
                                    staticClass: "d-none",
                                    attrs: {
                                      type: "radio",
                                      value: "East",
                                      id: "path-east"
                                    },
                                    domProps: {
                                      checked: _vm._q(_vm.path, "East")
                                    },
                                    on: {
                                      change: function($event) {
                                        _vm.path = "East"
                                      }
                                    }
                                  }),
                                  _c("label", { attrs: { for: "path-east" } }, [
                                    _vm._v("East Path")
                                  ])
                                ]),
                                _vm._v(" "),
                                _c("li", [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.path,
                                        expression: "path"
                                      }
                                    ],
                                    staticClass: "d-none",
                                    attrs: {
                                      type: "radio",
                                      value: "West",
                                      id: "path-west"
                                    },
                                    domProps: {
                                      checked: _vm._q(_vm.path, "West")
                                    },
                                    on: {
                                      change: function($event) {
                                        _vm.path = "West"
                                      }
                                    }
                                  }),
                                  _c("label", { attrs: { for: "path-west" } }, [
                                    _vm._v("West Path")
                                  ])
                                ]),
                                _vm._v(" "),
                                _c("li", [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.path,
                                        expression: "path"
                                      }
                                    ],
                                    staticClass: "d-none",
                                    attrs: {
                                      type: "radio",
                                      value: "North",
                                      id: "path-north"
                                    },
                                    domProps: {
                                      checked: _vm._q(_vm.path, "North")
                                    },
                                    on: {
                                      change: function($event) {
                                        _vm.path = "North"
                                      }
                                    }
                                  }),
                                  _c(
                                    "label",
                                    { attrs: { for: "path-north" } },
                                    [_vm._v("North Path")]
                                  )
                                ]),
                                _vm._v(" "),
                                _c("li", [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.path,
                                        expression: "path"
                                      }
                                    ],
                                    staticClass: "d-none",
                                    attrs: {
                                      type: "radio",
                                      value: "South",
                                      id: "path-south"
                                    },
                                    domProps: {
                                      checked: _vm._q(_vm.path, "South")
                                    },
                                    on: {
                                      change: function($event) {
                                        _vm.path = "South"
                                      }
                                    }
                                  }),
                                  _c(
                                    "label",
                                    { attrs: { for: "path-south" } },
                                    [_vm._v("South Path")]
                                  )
                                ])
                              ])
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "clearfix" })
                          ])
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "row" }, [
                          _c("div", { staticClass: "col-md-6 col-sm-12" }, [
                            _c(
                              "h2",
                              { staticClass: "fit-top--h2" },
                              [
                                _c("span", [_vm._v("Total Challenges")]),
                                _vm._v(" "),
                                _vm.challenges.data
                                  ? [_vm._v(_vm._s(_vm.challenges.meta.total))]
                                  : _vm._e()
                              ],
                              2
                            )
                          ]),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "col-md-6 col-sm-12" },
                            [
                              _c(
                                "router-link",
                                {
                                  staticClass: "green-btn-project",
                                  attrs: { to: { name: "challenges.create" } }
                                },
                                [
                                  _c("i", {
                                    staticClass: "fa fa-plus-circle",
                                    attrs: { "aria-hidden": "true" }
                                  }),
                                  _vm._v(
                                    " Add Challenge\n                                                "
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "router-link",
                                {
                                  staticClass: "green-btn-project2",
                                  attrs: { to: { name: "fit_to_path.create" } }
                                },
                                [_vm._v("Set level & Leverage points")]
                              ),
                              _vm._v(" "),
                              _c(
                                "router-link",
                                {
                                  staticClass: "fit-setting-a",
                                  attrs: { to: { name: "settings" } }
                                },
                                [
                                  _c("i", {
                                    staticClass: "fa fa-cog",
                                    attrs: { "aria-hidden": "true" }
                                  })
                                ]
                              )
                            ],
                            1
                          )
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "admin-top-blue-nav" }, [
                          _c("ul", [
                            _c("li", [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.level,
                                    expression: "level"
                                  }
                                ],
                                staticClass: "d-none",
                                attrs: {
                                  type: "radio",
                                  value: "1",
                                  id: "level1"
                                },
                                domProps: { checked: _vm._q(_vm.level, "1") },
                                on: {
                                  change: function($event) {
                                    _vm.level = "1"
                                  }
                                }
                              }),
                              _c("label", { attrs: { for: "level1" } }, [
                                _vm._v("Level 1")
                              ])
                            ]),
                            _vm._v(" "),
                            _c("li", [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.level,
                                    expression: "level"
                                  }
                                ],
                                staticClass: "d-none",
                                attrs: {
                                  type: "radio",
                                  value: "2",
                                  id: "level2"
                                },
                                domProps: { checked: _vm._q(_vm.level, "2") },
                                on: {
                                  change: function($event) {
                                    _vm.level = "2"
                                  }
                                }
                              }),
                              _c("label", { attrs: { for: "level2" } }, [
                                _vm._v("Level 2")
                              ])
                            ]),
                            _vm._v(" "),
                            _c("li", [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.level,
                                    expression: "level"
                                  }
                                ],
                                staticClass: "d-none",
                                attrs: {
                                  type: "radio",
                                  value: "3",
                                  id: "level3"
                                },
                                domProps: { checked: _vm._q(_vm.level, "3") },
                                on: {
                                  change: function($event) {
                                    _vm.level = "3"
                                  }
                                }
                              }),
                              _c("label", { attrs: { for: "level3" } }, [
                                _vm._v("Level 3")
                              ])
                            ]),
                            _vm._v(" "),
                            _c("li", [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.level,
                                    expression: "level"
                                  }
                                ],
                                staticClass: "d-none",
                                attrs: {
                                  type: "radio",
                                  value: "4",
                                  id: "level4"
                                },
                                domProps: { checked: _vm._q(_vm.level, "4") },
                                on: {
                                  change: function($event) {
                                    _vm.level = "4"
                                  }
                                }
                              }),
                              _c("label", { attrs: { for: "level4" } }, [
                                _vm._v("Level 4")
                              ])
                            ]),
                            _vm._v(" "),
                            _c("li", [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.level,
                                    expression: "level"
                                  }
                                ],
                                staticClass: "d-none",
                                attrs: {
                                  type: "radio",
                                  value: "5",
                                  id: "level5"
                                },
                                domProps: { checked: _vm._q(_vm.level, "5") },
                                on: {
                                  change: function($event) {
                                    _vm.level = "5"
                                  }
                                }
                              }),
                              _c("label", { attrs: { for: "level5" } }, [
                                _vm._v("Level 5")
                              ])
                            ]),
                            _vm._v(" "),
                            _c("li", [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.level,
                                    expression: "level"
                                  }
                                ],
                                staticClass: "d-none",
                                attrs: {
                                  type: "radio",
                                  value: "6",
                                  id: "level6"
                                },
                                domProps: { checked: _vm._q(_vm.level, "6") },
                                on: {
                                  change: function($event) {
                                    _vm.level = "6"
                                  }
                                }
                              }),
                              _c("label", { attrs: { for: "level6" } }, [
                                _vm._v("Level 6")
                              ])
                            ]),
                            _vm._v(" "),
                            _c("li", [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.level,
                                    expression: "level"
                                  }
                                ],
                                staticClass: "d-none",
                                attrs: {
                                  type: "radio",
                                  value: "7",
                                  id: "level7"
                                },
                                domProps: { checked: _vm._q(_vm.level, "7") },
                                on: {
                                  change: function($event) {
                                    _vm.level = "7"
                                  }
                                }
                              }),
                              _c("label", { attrs: { for: "level7" } }, [
                                _vm._v("Level 7")
                              ])
                            ]),
                            _vm._v(" "),
                            _c("li", [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.level,
                                    expression: "level"
                                  }
                                ],
                                staticClass: "d-none",
                                attrs: {
                                  type: "radio",
                                  value: "8",
                                  id: "level8"
                                },
                                domProps: { checked: _vm._q(_vm.level, "8") },
                                on: {
                                  change: function($event) {
                                    _vm.level = "8"
                                  }
                                }
                              }),
                              _c("label", { attrs: { for: "level8" } }, [
                                _vm._v("Level 8")
                              ])
                            ]),
                            _vm._v(" "),
                            _c("li", [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.level,
                                    expression: "level"
                                  }
                                ],
                                staticClass: "d-none",
                                attrs: {
                                  type: "radio",
                                  value: "9",
                                  id: "level9"
                                },
                                domProps: { checked: _vm._q(_vm.level, "9") },
                                on: {
                                  change: function($event) {
                                    _vm.level = "9"
                                  }
                                }
                              }),
                              _c("label", { attrs: { for: "level9" } }, [
                                _vm._v("Level 9")
                              ])
                            ]),
                            _vm._v(" "),
                            _c("li", [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.level,
                                    expression: "level"
                                  }
                                ],
                                staticClass: "d-none",
                                attrs: {
                                  type: "radio",
                                  value: "10",
                                  id: "level10"
                                },
                                domProps: { checked: _vm._q(_vm.level, "10") },
                                on: {
                                  change: function($event) {
                                    _vm.level = "10"
                                  }
                                }
                              }),
                              _c("label", { attrs: { for: "level10" } }, [
                                _vm._v("Level 10")
                              ])
                            ])
                          ])
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "clearfix" })
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "maain-tabble table-responsive" },
                        [
                          _c(
                            "div",
                            {
                              staticClass:
                                "dataTables_wrapper container-fluid dt-bootstrap4 no-footer",
                              attrs: { id: "DataTables_Table_0_wrapper" }
                            },
                            [
                              _c("div", { staticClass: "row" }, [
                                _c(
                                  "div",
                                  { staticClass: "col-sm-12 col-md-6" },
                                  [
                                    _c("table-length", {
                                      on: {
                                        lengthChanged: function($event) {
                                          _vm.per_page = $event
                                        }
                                      }
                                    })
                                  ],
                                  1
                                ),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  { staticClass: "col-sm-12 col-md-6" },
                                  [
                                    _c(
                                      "div",
                                      {
                                        staticClass: "dataTables_filter",
                                        attrs: {
                                          id: "DataTables_Table_0_filter"
                                        }
                                      },
                                      [
                                        _c("label", [
                                          _vm._v(
                                            "Search:\n                                                            "
                                          ),
                                          _c("input", {
                                            directives: [
                                              {
                                                name: "model",
                                                rawName: "v-model",
                                                value: _vm.search_term,
                                                expression: "search_term"
                                              }
                                            ],
                                            staticClass:
                                              "form-control form-control-sm",
                                            attrs: {
                                              type: "search",
                                              placeholder: "",
                                              "aria-controls":
                                                "DataTables_Table_0"
                                            },
                                            domProps: {
                                              value: _vm.search_term
                                            },
                                            on: {
                                              input: [
                                                function($event) {
                                                  if ($event.target.composing) {
                                                    return
                                                  }
                                                  _vm.search_term =
                                                    $event.target.value
                                                },
                                                _vm.loadChallenges
                                              ]
                                            }
                                          })
                                        ])
                                      ]
                                    )
                                  ]
                                )
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "row" }, [
                                _c("div", { staticClass: "col-sm-12" }, [
                                  _c(
                                    "table",
                                    {
                                      staticClass:
                                        "table table-striped table-bordered zero-configuration dataTable no-footer",
                                      attrs: { id: "DataTables_Table_0" }
                                    },
                                    [
                                      _vm._m(0),
                                      _vm._v(" "),
                                      _c(
                                        "tbody",
                                        _vm._l(_vm.challenges.data, function(
                                          challenge
                                        ) {
                                          return _c(
                                            "tr",
                                            { key: challenge.id },
                                            [
                                              _c("td", [
                                                _vm._v(_vm._s(challenge.id))
                                              ]),
                                              _vm._v(" "),
                                              _c("td", [
                                                _vm._v(_vm._s(challenge.title))
                                              ]),
                                              _vm._v(" "),
                                              _c("td", [
                                                _vm._v(
                                                  _vm._s(
                                                    challenge.exercises.length
                                                  )
                                                )
                                              ]),
                                              _vm._v(" "),
                                              _c("td", [
                                                _vm._v(
                                                  _vm._s(
                                                    challenge.reward_points
                                                  )
                                                )
                                              ]),
                                              _vm._v(" "),
                                              _c("td", [
                                                _c(
                                                  "div",
                                                  {
                                                    staticClass:
                                                      "btn-group mr-1 mb-1"
                                                  },
                                                  [
                                                    _vm._m(1, true),
                                                    _vm._v(" "),
                                                    _c(
                                                      "div",
                                                      {
                                                        staticClass:
                                                          "dropdown-menu",
                                                        staticStyle: {
                                                          position: "absolute",
                                                          transform:
                                                            "translate3d(0px, 21px, 0px)",
                                                          top: "0px",
                                                          left: "0px",
                                                          "will-change":
                                                            "transform"
                                                        },
                                                        attrs: {
                                                          "x-placement":
                                                            "bottom-start"
                                                        }
                                                      },
                                                      [
                                                        _c(
                                                          "router-link",
                                                          {
                                                            staticClass:
                                                              "dropdown-item",
                                                            attrs: {
                                                              to: {
                                                                name:
                                                                  "challenges.show",
                                                                params: {
                                                                  id:
                                                                    challenge.id
                                                                }
                                                              }
                                                            }
                                                          },
                                                          [
                                                            _c("i", {
                                                              staticClass:
                                                                "fa fa-eye"
                                                            }),
                                                            _vm._v(
                                                              "View\n                                                                            "
                                                            )
                                                          ]
                                                        ),
                                                        _vm._v(" "),
                                                        _c(
                                                          "router-link",
                                                          {
                                                            staticClass:
                                                              "dropdown-item",
                                                            attrs: {
                                                              to: {
                                                                name:
                                                                  "challenges.edit",
                                                                params: {
                                                                  id:
                                                                    challenge.id
                                                                }
                                                              }
                                                            }
                                                          },
                                                          [
                                                            _c("i", {
                                                              staticClass:
                                                                "fa fa-pencil"
                                                            }),
                                                            _vm._v(
                                                              "Edit\n                                                                            "
                                                            )
                                                          ]
                                                        ),
                                                        _vm._v(" "),
                                                        _c(
                                                          "a",
                                                          {
                                                            staticClass:
                                                              "dropdown-item",
                                                            on: {
                                                              click: function(
                                                                $event
                                                              ) {
                                                                $event.preventDefault()
                                                                return _vm.deleteChallenge(
                                                                  challenge.id
                                                                )
                                                              }
                                                            }
                                                          },
                                                          [
                                                            _c("i", {
                                                              staticClass:
                                                                "fa fa-trash"
                                                            }),
                                                            _vm._v(
                                                              "Delete\n                                                                            "
                                                            )
                                                          ]
                                                        )
                                                      ],
                                                      1
                                                    )
                                                  ]
                                                )
                                              ])
                                            ]
                                          )
                                        }),
                                        0
                                      )
                                    ]
                                  )
                                ])
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "row" }, [
                                _c(
                                  "div",
                                  { staticClass: "col-sm-12 col-md-5" },
                                  [
                                    _vm.challenges &&
                                    _vm.challenges.meta &&
                                    _vm.challenges.meta.from
                                      ? _c(
                                          "div",
                                          {
                                            staticClass: "dataTables_info",
                                            attrs: {
                                              id: "DataTables_Table_0_info",
                                              role: "status",
                                              "aria-live": "polite"
                                            }
                                          },
                                          [
                                            _vm._v(
                                              "\n                                                        Showing " +
                                                _vm._s(
                                                  _vm.challenges.meta.from
                                                ) +
                                                " to " +
                                                _vm._s(_vm.challenges.meta.to) +
                                                " of " +
                                                _vm._s(
                                                  _vm.challenges.meta.total
                                                ) +
                                                " entries\n                                                    "
                                            )
                                          ]
                                        )
                                      : _vm._e()
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  { staticClass: "col-sm-12 col-md-7" },
                                  [
                                    _c(
                                      "div",
                                      {
                                        staticClass:
                                          "dataTables_paginate paging_simple_numbers",
                                        attrs: {
                                          id: "DataTables_Table_0_paginate"
                                        }
                                      },
                                      [
                                        _c(
                                          "pagination",
                                          {
                                            attrs: { data: _vm.challenges },
                                            on: {
                                              "pagination-change-page":
                                                _vm.loadChallenges
                                            }
                                          },
                                          [
                                            _c(
                                              "span",
                                              {
                                                attrs: { slot: "prev-nav" },
                                                slot: "prev-nav"
                                              },
                                              [_vm._v("< Previous")]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "span",
                                              {
                                                attrs: { slot: "next-nav" },
                                                slot: "next-nav"
                                              },
                                              [_vm._v("Next >")]
                                            )
                                          ]
                                        )
                                      ],
                                      1
                                    )
                                  ]
                                )
                              ])
                            ]
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _vm.activeChallenges.length > 0
                        ? _c(
                            "div",
                            { staticClass: "user-profile-bottom-fftp" },
                            [
                              _vm._m(2),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "row" },
                                _vm._l(_vm.activeChallenges, function(
                                  challenge
                                ) {
                                  return _c(
                                    "div",
                                    {
                                      staticClass:
                                        "col-xl-3 col-lg-6 col-md-6 col-sm-12 mb-2"
                                    },
                                    [
                                      _c(
                                        "div",
                                        { staticClass: "user-profile-btm-box" },
                                        [
                                          _c(
                                            "div",
                                            { staticClass: "top-head" },
                                            [
                                              _c("h3", [
                                                _vm._v(
                                                  _vm._s(
                                                    challenge.challenge.title
                                                  ) + " Challenge"
                                                )
                                              ]),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "d-flex flex-row align-items-center justify-content-between"
                                                },
                                                [
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass: "flex-column"
                                                    },
                                                    [
                                                      _c("img", {
                                                        staticClass: "pro-img",
                                                        attrs: {
                                                          src:
                                                            challenge.sender
                                                              .image,
                                                          alt:
                                                            challenge.sender
                                                              .name
                                                        }
                                                      }),
                                                      _vm._v(" "),
                                                      _c("span", [
                                                        _vm._v(
                                                          _vm._s(
                                                            challenge.sender
                                                              .name
                                                          )
                                                        )
                                                      ])
                                                    ]
                                                  ),
                                                  _vm._v(" "),
                                                  _c("span", [_vm._v("VS")]),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass: "flex-column"
                                                    },
                                                    [
                                                      _c("img", {
                                                        staticClass: "pro-img",
                                                        attrs: {
                                                          src:
                                                            challenge.recipient
                                                              .image,
                                                          alt:
                                                            challenge.recipient
                                                              .name
                                                        }
                                                      }),
                                                      _vm._v(" "),
                                                      _c("span", [
                                                        _vm._v(
                                                          _vm._s(
                                                            challenge.recipient
                                                              .name
                                                          )
                                                        )
                                                      ])
                                                    ]
                                                  )
                                                ]
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "span",
                                            { staticClass: "active-span" },
                                            [_vm._v("Active")]
                                          ),
                                          _vm._v(" "),
                                          _c("div", { staticClass: "p-1" }, [
                                            _c("h4", [
                                              _c("span", [_vm._v("Exercise")]),
                                              _vm._v(
                                                " " +
                                                  _vm._s(
                                                    challenge.challenge
                                                      .exercises[0].title
                                                  ) +
                                                  " in 5 Minutes"
                                              )
                                            ]),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              {
                                                staticClass:
                                                  "d-flex text-center justify-content-around"
                                              },
                                              [
                                                _c(
                                                  "span",
                                                  { staticClass: "date-time" },
                                                  [
                                                    _c("i", {
                                                      staticClass:
                                                        "fa fa-clock-o",
                                                      attrs: {
                                                        "aria-hidden": "true"
                                                      }
                                                    }),
                                                    _vm._v(
                                                      " " +
                                                        _vm._s(
                                                          challenge.left_time
                                                        )
                                                    )
                                                  ]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "span",
                                                  { staticClass: "date-time" },
                                                  [
                                                    _c("i", {
                                                      staticClass:
                                                        "fa fa-calendar",
                                                      attrs: {
                                                        "aria-hidden": "true"
                                                      }
                                                    }),
                                                    _vm._v(
                                                      " Date " +
                                                        _vm._s(
                                                          challenge.created_date
                                                        )
                                                    )
                                                  ]
                                                )
                                              ]
                                            )
                                          ])
                                        ]
                                      )
                                    ]
                                  )
                                }),
                                0
                              )
                            ]
                          )
                        : _vm._e()
                    ])
                  ]
                )
              ])
            ])
          ])
        ])
      ])
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("Challenge")]),
        _vm._v(" "),
        _c("th", [_vm._v("Title")]),
        _vm._v(" "),
        _c("th", [_vm._v("Exercise")]),
        _vm._v(" "),
        _c("th", [_vm._v("Reward Points")]),
        _vm._v(" "),
        _c("th", [_vm._v("Action")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "btn dropdown-toggle btn-drop-table btn-sm",
        attrs: {
          type: "button",
          "data-toggle": "dropdown",
          "aria-haspopup": "true",
          "aria-expanded": "false"
        }
      },
      [_c("i", { staticClass: "fa fa-ellipsis-v" })]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row mb-3" }, [
      _c("div", { staticClass: "col-md-7 col-sm-12 pt-3" }, [
        _c("h2", [_vm._v("Active Challenges")])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-4 col-sm-12 dataTables_wrapper" })
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/admin/components/challenges/IndexComponent.vue":
/*!*********************************************************************!*\
  !*** ./resources/js/admin/components/challenges/IndexComponent.vue ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _IndexComponent_vue_vue_type_template_id_6e3e0d6f___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./IndexComponent.vue?vue&type=template&id=6e3e0d6f& */ "./resources/js/admin/components/challenges/IndexComponent.vue?vue&type=template&id=6e3e0d6f&");
/* harmony import */ var _IndexComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./IndexComponent.vue?vue&type=script&lang=js& */ "./resources/js/admin/components/challenges/IndexComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _IndexComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./IndexComponent.vue?vue&type=style&index=0&lang=css& */ "./resources/js/admin/components/challenges/IndexComponent.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _IndexComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _IndexComponent_vue_vue_type_template_id_6e3e0d6f___WEBPACK_IMPORTED_MODULE_0__["render"],
  _IndexComponent_vue_vue_type_template_id_6e3e0d6f___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/admin/components/challenges/IndexComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/admin/components/challenges/IndexComponent.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/admin/components/challenges/IndexComponent.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./IndexComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/challenges/IndexComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/admin/components/challenges/IndexComponent.vue?vue&type=style&index=0&lang=css&":
/*!******************************************************************************************************!*\
  !*** ./resources/js/admin/components/challenges/IndexComponent.vue?vue&type=style&index=0&lang=css& ***!
  \******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader??ref--7-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./IndexComponent.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/challenges/IndexComponent.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/admin/components/challenges/IndexComponent.vue?vue&type=template&id=6e3e0d6f&":
/*!****************************************************************************************************!*\
  !*** ./resources/js/admin/components/challenges/IndexComponent.vue?vue&type=template&id=6e3e0d6f& ***!
  \****************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexComponent_vue_vue_type_template_id_6e3e0d6f___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./IndexComponent.vue?vue&type=template&id=6e3e0d6f& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/challenges/IndexComponent.vue?vue&type=template&id=6e3e0d6f&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexComponent_vue_vue_type_template_id_6e3e0d6f___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexComponent_vue_vue_type_template_id_6e3e0d6f___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);