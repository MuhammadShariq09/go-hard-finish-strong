(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["create-ftfp-challenges"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/challenges/CreateComponent.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/admin/components/challenges/CreateComponent.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _compoments_CropperComponent__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../compoments/CropperComponent */ "./resources/js/compoments/CropperComponent.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Cropper: _compoments_CropperComponent__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      base_url: window.base_url,
      isLoading: false,
      myFiles: [],
      isEdit: false,
      isExerciseEdit: false,
      editExerciseId: false,
      challenge: {
        level: 'Level 1',
        path: 'East',
        title: '',
        reward_points: '',
        description: '',
        image: "".concat(window.base_url, "/images/upload-img_03.jpg"),
        imageForError: "",
        exercises: [{
          title: '',
          description: '',
          sets: '',
          repeat: 1,
          category: '',
          image: "".concat(window.base_url, "/images/upload-img_03.jpg"),
          imageForError: ""
        }]
      },
      categories: []
    };
  },
  mounted: function mounted() {
    this.isEdit = !!this.$route.params.id;
    this.isExerciseEdit = this.$route.params.exerciseId !== undefined;
    this.editExerciseId = Number(this.$route.params.exerciseId);
    this.getExercise(this.$route.params.id);
    this.loadCategories();
  },
  methods: {
    loadCategories: function loadCategories() {
      var _this = this;

      this.isLoading = true;
      axios.get("/categories").then(function (data) {
        _this.categories = data.data;
        _this.isLoading = false;
      });
    },
    getExercise: function getExercise(id) {
      var _this2 = this;

      if (!this.isEdit) return;
      this.isLoading = true;
      axios.get("/challenges/".concat(id)).then(function (data) {
        _this2.challenge = data.data;
        _this2.isLoading = false;
      });
    },
    addExercise: function addExercise() {
      this.challenge.exercises.push({
        title: '',
        description: '',
        sets: '',
        repeat: 1,
        category: '',
        image: "".concat(window.base_url, "/images/upload-img_03.jpg"),
        imageForError: ""
      });
    },
    readURL: function readURL() {
      var _this3 = this;

      var index = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : -1;
      var file; // if index is < 0 then it is challenge image

      if (index < 0) file = this.$refs.challengeImage.files[0];else file = this.$refs["exerciseImage".concat(index)][0].files[0];

      if (file) {
        var reader = new FileReader();

        reader.onload = function (e) {
          if (index < 0) _this3.challenge.image = e.target.result;else _this3.challenge.exercises[index].image = e.target.result;
        };

        reader.readAsDataURL(file);
      }
    },
    removeExercise: function removeExercise(index) {
      var _this4 = this;

      this.$dialog.confirm('Are you sure you want to delete this exercise?', {
        okText: 'Proceed'
      }).then(function (dialog) {
        _this4.challenge.exercises.splice(index, 1);

        dialog.close();
      });
    },
    getError: function getError(message) {
      return message ? message.replace(/[0-9]\-/, ' ') : '';
    },
    validate: function validate() {
      var _this5 = this;

      // axios.post('/api/challenges2', this.challenge)
      // .then(data => {
      //     this.isLoading = false;
      //     this.$toastr.success('Challenge Created', 'Success', {});
      //     // this.$router.push({ name: 'challenges.index' });
      // }).catch(e => this.isLoading = false);
      // return;
      this.$validator.validateAll().then(function (result) {
        if (!result) return;

        _this5.save();
      });
    },
    save: function save() {
      this.isLoading = true;
      if (this.isEdit) this.edit();else this.insert();
    },
    insert: function insert() {
      var _this6 = this;

      axios.post('/challenges', this.challenge).then(function (data) {
        _this6.isLoading = false;

        _this6.$toastr.success('Challenge Created', 'Success', {});

        _this6.$router.push({
          name: 'challenges.index'
        });
      })["catch"](function (e) {
        return _this6.isLoading = false;
      });
    },
    edit: function edit() {
      var _this7 = this;

      axios.put("/challenges/".concat(this.challenge.id), this.challenge).then(function (data) {
        _this7.isLoading = false;

        _this7.$toastr.success('Challenge Created', 'Success', {}); // this.$router.push({ name: 'challenges.index' });

      })["catch"](function (e) {
        return _this7.isLoading = false;
      });
    },
    imageChanged: function imageChanged(data) {
      var index = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : -1;

      if (index >= 0) {
        this.challenge.exercises[index].image = data;
        this.challenge.exercises[index].imageForError = data;
      } else {
        this.challenge.image = data;
        this.challenge.imageForError = data;
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/compoments/CropperComponent.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/compoments/CropperComponent.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var cropperjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! cropperjs */ "./node_modules/cropperjs/dist/cropper.js");
/* harmony import */ var cropperjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(cropperjs__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    'src': {
      type: String,
      "default": ""
    },
    'elIdx': {
      type: [String, Number],
      "default": ""
    }
  },
  data: function data() {
    return {
      image: "",
      cropper: null
    };
  },
  created: function created() {
    this.image = this.src;
  },
  mounted: function mounted() {},
  methods: {
    crop: function crop() {
      this.image = this.cropper.getCroppedCanvas({
        maxWidth: 800,
        maxHeight: 800
      }).toDataURL();
      this.cropper.destroy();
      this.$emit('imageChanged', this.image);
    },
    readURL: function readURL() {
      var _this = this;

      var file = this.$refs["imageInput".concat(this.elIdx)].files[0];

      if (file) {
        var reader = new FileReader();

        reader.onload = function (e) {
          _this.image = e.target.result;
          setTimeout(function () {
            _this.cropper = new cropperjs__WEBPACK_IMPORTED_MODULE_0___default.a(_this.$refs["img".concat(_this.elIdx)], {
              aspectRatio: 1,
              viewMode: 1
              /*crop(event) {
                  console.log(event.detail.x);
                  console.log(event.detail.y);
                  /!*console.log(event.detail.width);
                  console.log(event.detail.height);
                  console.log(event.detail.rotate);
                  console.log(event.detail.scaleX);
                  console.log(event.detail.scaleY);*!/
              },*/

            });
          }, 10);
        };

        reader.readAsDataURL(file); // const image = document.getElementById('personal_img');
      }
    }
  },
  watch: {
    'src': function src() {
      this.image = this.src;
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/compoments/CropperComponent.vue?vue&type=style&index=0&lang=css&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/compoments/CropperComponent.vue?vue&type=style&index=0&lang=css& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.cropper-image-container{\n    position: relative;\n}\n.cropper-container img {\n    border-radius: 0;\n}\n.cropper-container.cropper-bg+label {\n    display: none;\n}\n.cropper-container.cropper-bg+label+button {\n    display: inline-block !important;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/compoments/CropperComponent.vue?vue&type=style&index=0&lang=css&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/compoments/CropperComponent.vue?vue&type=style&index=0&lang=css& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader??ref--7-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/vue-loader/lib??vue-loader-options!./CropperComponent.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/compoments/CropperComponent.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/challenges/CreateComponent.vue?vue&type=template&id=c9790a26&":
/*!***********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/admin/components/challenges/CreateComponent.vue?vue&type=template&id=c9790a26& ***!
  \***********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "app-content content" },
    [
      _vm.isLoading
        ? _c("BlockUI", { attrs: { message: "Loading..." } }, [
            _c("i", { staticClass: "fa fa-cog fa-spin fa-3x fa-fw" })
          ])
        : _vm._e(),
      _vm._v(" "),
      _c("div", { staticClass: "content-wrapper" }, [
        _c("div", { staticClass: "content-body" }, [
          _c(
            "form",
            {
              attrs: { id: "combination-charts" },
              on: {
                submit: function($event) {
                  $event.preventDefault()
                  return _vm.validate()
                }
              }
            },
            [
              _c("div", { staticClass: "row" }, [
                _c("div", { staticClass: "col-12" }, [
                  _c(
                    "div",
                    { staticClass: "card rounded p-3 admin-overview-main" },
                    [
                      _c("div", { staticClass: "row" }, [
                        _c(
                          "div",
                          { staticClass: "col-lg-12" },
                          [
                            !_vm.isEdit && !_vm.isExerciseEdit
                              ? _c("h1", [_vm._v("FFTP Add Challenges")])
                              : _vm._e(),
                            _vm._v(" "),
                            _vm.isEdit && !_vm.isExerciseEdit
                              ? _c("h1", [_vm._v("FFTP Update Challenges")])
                              : _vm._e(),
                            _vm._v(" "),
                            _vm.isEdit && _vm.isExerciseEdit
                              ? _c("h1", [_vm._v("Update Exercise")])
                              : _vm._e(),
                            _vm._v(" "),
                            !_vm.isExerciseEdit
                              ? _c("div", { staticClass: "row" }, [
                                  _c(
                                    "div",
                                    { staticClass: "col-md-6 col-sm-12" },
                                    [
                                      _c(
                                        "div",
                                        { staticClass: "admin-top-red-nav" },
                                        [
                                          _c("ul", [
                                            _c("li", [
                                              _c("input", {
                                                directives: [
                                                  {
                                                    name: "model",
                                                    rawName: "v-model",
                                                    value: _vm.challenge.path,
                                                    expression: "challenge.path"
                                                  }
                                                ],
                                                staticClass: "d-none",
                                                attrs: {
                                                  type: "radio",
                                                  value: "East",
                                                  id: "path-east"
                                                },
                                                domProps: {
                                                  checked: _vm._q(
                                                    _vm.challenge.path,
                                                    "East"
                                                  )
                                                },
                                                on: {
                                                  change: function($event) {
                                                    return _vm.$set(
                                                      _vm.challenge,
                                                      "path",
                                                      "East"
                                                    )
                                                  }
                                                }
                                              }),
                                              _c(
                                                "label",
                                                { attrs: { for: "path-east" } },
                                                [_vm._v("East Path")]
                                              )
                                            ]),
                                            _vm._v(" "),
                                            _c("li", [
                                              _c("input", {
                                                directives: [
                                                  {
                                                    name: "model",
                                                    rawName: "v-model",
                                                    value: _vm.challenge.path,
                                                    expression: "challenge.path"
                                                  }
                                                ],
                                                staticClass: "d-none",
                                                attrs: {
                                                  type: "radio",
                                                  value: "West",
                                                  id: "path-west"
                                                },
                                                domProps: {
                                                  checked: _vm._q(
                                                    _vm.challenge.path,
                                                    "West"
                                                  )
                                                },
                                                on: {
                                                  change: function($event) {
                                                    return _vm.$set(
                                                      _vm.challenge,
                                                      "path",
                                                      "West"
                                                    )
                                                  }
                                                }
                                              }),
                                              _c(
                                                "label",
                                                { attrs: { for: "path-west" } },
                                                [_vm._v("West Path")]
                                              )
                                            ]),
                                            _vm._v(" "),
                                            _c("li", [
                                              _c("input", {
                                                directives: [
                                                  {
                                                    name: "model",
                                                    rawName: "v-model",
                                                    value: _vm.challenge.path,
                                                    expression: "challenge.path"
                                                  }
                                                ],
                                                staticClass: "d-none",
                                                attrs: {
                                                  type: "radio",
                                                  value: "North",
                                                  id: "path-north"
                                                },
                                                domProps: {
                                                  checked: _vm._q(
                                                    _vm.challenge.path,
                                                    "North"
                                                  )
                                                },
                                                on: {
                                                  change: function($event) {
                                                    return _vm.$set(
                                                      _vm.challenge,
                                                      "path",
                                                      "North"
                                                    )
                                                  }
                                                }
                                              }),
                                              _c(
                                                "label",
                                                {
                                                  attrs: { for: "path-north" }
                                                },
                                                [_vm._v("North Path")]
                                              )
                                            ]),
                                            _vm._v(" "),
                                            _c("li", [
                                              _c("input", {
                                                directives: [
                                                  {
                                                    name: "model",
                                                    rawName: "v-model",
                                                    value: _vm.challenge.path,
                                                    expression: "challenge.path"
                                                  }
                                                ],
                                                staticClass: "d-none",
                                                attrs: {
                                                  type: "radio",
                                                  value: "South",
                                                  id: "path-south"
                                                },
                                                domProps: {
                                                  checked: _vm._q(
                                                    _vm.challenge.path,
                                                    "South"
                                                  )
                                                },
                                                on: {
                                                  change: function($event) {
                                                    return _vm.$set(
                                                      _vm.challenge,
                                                      "path",
                                                      "South"
                                                    )
                                                  }
                                                }
                                              }),
                                              _c(
                                                "label",
                                                {
                                                  attrs: { for: "path-south" }
                                                },
                                                [_vm._v("South Path")]
                                              )
                                            ])
                                          ])
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "clearfix" })
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "div",
                                    { staticClass: "col-md-6 col-sm-12 mt-2" },
                                    [
                                      _c(
                                        "router-link",
                                        {
                                          staticClass: "green-btn-project2",
                                          attrs: {
                                            to: { name: "fit_to_path.create" }
                                          }
                                        },
                                        [_vm._v("Set level points")]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "router-link",
                                        {
                                          staticClass: "fit-setting-a",
                                          attrs: { to: { name: "settings" } }
                                        },
                                        [
                                          _c("i", {
                                            staticClass: "fa fa-cog",
                                            attrs: { "aria-hidden": "true" }
                                          })
                                        ]
                                      )
                                    ],
                                    1
                                  )
                                ])
                              : _vm._e(),
                            _vm._v(" "),
                            _c("div", { staticClass: "admin-top-blue-nav" }, [
                              !_vm.isExerciseEdit
                                ? _c("ul", [
                                    _c("li", [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.challenge.level,
                                            expression: "challenge.level"
                                          }
                                        ],
                                        staticClass: "d-none",
                                        attrs: {
                                          type: "radio",
                                          value: "Level 1",
                                          id: "level1"
                                        },
                                        domProps: {
                                          checked: _vm._q(
                                            _vm.challenge.level,
                                            "Level 1"
                                          )
                                        },
                                        on: {
                                          change: function($event) {
                                            return _vm.$set(
                                              _vm.challenge,
                                              "level",
                                              "Level 1"
                                            )
                                          }
                                        }
                                      }),
                                      _c(
                                        "label",
                                        { attrs: { for: "level1" } },
                                        [_vm._v("Level 1")]
                                      )
                                    ]),
                                    _vm._v(" "),
                                    _c("li", [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.challenge.level,
                                            expression: "challenge.level"
                                          }
                                        ],
                                        staticClass: "d-none",
                                        attrs: {
                                          type: "radio",
                                          value: "Level 2",
                                          id: "level2"
                                        },
                                        domProps: {
                                          checked: _vm._q(
                                            _vm.challenge.level,
                                            "Level 2"
                                          )
                                        },
                                        on: {
                                          change: function($event) {
                                            return _vm.$set(
                                              _vm.challenge,
                                              "level",
                                              "Level 2"
                                            )
                                          }
                                        }
                                      }),
                                      _c(
                                        "label",
                                        { attrs: { for: "level2" } },
                                        [_vm._v("Level 2")]
                                      )
                                    ]),
                                    _vm._v(" "),
                                    _c("li", [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.challenge.level,
                                            expression: "challenge.level"
                                          }
                                        ],
                                        staticClass: "d-none",
                                        attrs: {
                                          type: "radio",
                                          value: "Level 3",
                                          id: "level3"
                                        },
                                        domProps: {
                                          checked: _vm._q(
                                            _vm.challenge.level,
                                            "Level 3"
                                          )
                                        },
                                        on: {
                                          change: function($event) {
                                            return _vm.$set(
                                              _vm.challenge,
                                              "level",
                                              "Level 3"
                                            )
                                          }
                                        }
                                      }),
                                      _c(
                                        "label",
                                        { attrs: { for: "level3" } },
                                        [_vm._v("Level 3")]
                                      )
                                    ]),
                                    _vm._v(" "),
                                    _c("li", [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.challenge.level,
                                            expression: "challenge.level"
                                          }
                                        ],
                                        staticClass: "d-none",
                                        attrs: {
                                          type: "radio",
                                          value: "Level 4",
                                          id: "level4"
                                        },
                                        domProps: {
                                          checked: _vm._q(
                                            _vm.challenge.level,
                                            "Level 4"
                                          )
                                        },
                                        on: {
                                          change: function($event) {
                                            return _vm.$set(
                                              _vm.challenge,
                                              "level",
                                              "Level 4"
                                            )
                                          }
                                        }
                                      }),
                                      _c(
                                        "label",
                                        { attrs: { for: "level4" } },
                                        [_vm._v("Level 4")]
                                      )
                                    ]),
                                    _vm._v(" "),
                                    _c("li", [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.challenge.level,
                                            expression: "challenge.level"
                                          }
                                        ],
                                        staticClass: "d-none",
                                        attrs: {
                                          type: "radio",
                                          value: "Level 5",
                                          id: "level5"
                                        },
                                        domProps: {
                                          checked: _vm._q(
                                            _vm.challenge.level,
                                            "Level 5"
                                          )
                                        },
                                        on: {
                                          change: function($event) {
                                            return _vm.$set(
                                              _vm.challenge,
                                              "level",
                                              "Level 5"
                                            )
                                          }
                                        }
                                      }),
                                      _c(
                                        "label",
                                        { attrs: { for: "level5" } },
                                        [_vm._v("Level 5")]
                                      )
                                    ]),
                                    _vm._v(" "),
                                    _c("li", [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.challenge.level,
                                            expression: "challenge.level"
                                          }
                                        ],
                                        staticClass: "d-none",
                                        attrs: {
                                          type: "radio",
                                          value: "Level 6",
                                          id: "level6"
                                        },
                                        domProps: {
                                          checked: _vm._q(
                                            _vm.challenge.level,
                                            "Level 6"
                                          )
                                        },
                                        on: {
                                          change: function($event) {
                                            return _vm.$set(
                                              _vm.challenge,
                                              "level",
                                              "Level 6"
                                            )
                                          }
                                        }
                                      }),
                                      _c(
                                        "label",
                                        { attrs: { for: "level6" } },
                                        [_vm._v("Level 6")]
                                      )
                                    ]),
                                    _vm._v(" "),
                                    _c("li", [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.challenge.level,
                                            expression: "challenge.level"
                                          }
                                        ],
                                        staticClass: "d-none",
                                        attrs: {
                                          type: "radio",
                                          value: "Level 7",
                                          id: "level7"
                                        },
                                        domProps: {
                                          checked: _vm._q(
                                            _vm.challenge.level,
                                            "Level 7"
                                          )
                                        },
                                        on: {
                                          change: function($event) {
                                            return _vm.$set(
                                              _vm.challenge,
                                              "level",
                                              "Level 7"
                                            )
                                          }
                                        }
                                      }),
                                      _c(
                                        "label",
                                        { attrs: { for: "level7" } },
                                        [_vm._v("Level 7")]
                                      )
                                    ]),
                                    _vm._v(" "),
                                    _c("li", [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.challenge.level,
                                            expression: "challenge.level"
                                          }
                                        ],
                                        staticClass: "d-none",
                                        attrs: {
                                          type: "radio",
                                          value: "Level 8",
                                          id: "level8"
                                        },
                                        domProps: {
                                          checked: _vm._q(
                                            _vm.challenge.level,
                                            "Level 8"
                                          )
                                        },
                                        on: {
                                          change: function($event) {
                                            return _vm.$set(
                                              _vm.challenge,
                                              "level",
                                              "Level 8"
                                            )
                                          }
                                        }
                                      }),
                                      _c(
                                        "label",
                                        { attrs: { for: "level8" } },
                                        [_vm._v("Level 8")]
                                      )
                                    ]),
                                    _vm._v(" "),
                                    _c("li", [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.challenge.level,
                                            expression: "challenge.level"
                                          }
                                        ],
                                        staticClass: "d-none",
                                        attrs: {
                                          type: "radio",
                                          value: "Level 9",
                                          id: "level9"
                                        },
                                        domProps: {
                                          checked: _vm._q(
                                            _vm.challenge.level,
                                            "Level 9"
                                          )
                                        },
                                        on: {
                                          change: function($event) {
                                            return _vm.$set(
                                              _vm.challenge,
                                              "level",
                                              "Level 9"
                                            )
                                          }
                                        }
                                      }),
                                      _c(
                                        "label",
                                        { attrs: { for: "level9" } },
                                        [_vm._v("Level 9")]
                                      )
                                    ]),
                                    _vm._v(" "),
                                    _c("li", [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.challenge.level,
                                            expression: "challenge.level"
                                          }
                                        ],
                                        staticClass: "d-none",
                                        attrs: {
                                          type: "radio",
                                          value: "Level 10",
                                          id: "level10"
                                        },
                                        domProps: {
                                          checked: _vm._q(
                                            _vm.challenge.level,
                                            "Level 10"
                                          )
                                        },
                                        on: {
                                          change: function($event) {
                                            return _vm.$set(
                                              _vm.challenge,
                                              "level",
                                              "Level 10"
                                            )
                                          }
                                        }
                                      }),
                                      _c(
                                        "label",
                                        { attrs: { for: "level10" } },
                                        [_vm._v("Level 10")]
                                      )
                                    ])
                                  ])
                                : _vm._e()
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "clearfix" }),
                            _vm._v(" "),
                            !_vm.isExerciseEdit
                              ? _c("div", { staticClass: "row" }, [
                                  _c(
                                    "div",
                                    {
                                      staticClass: "col-lg-2 col-md-2 col-sm-12"
                                    },
                                    [
                                      _c("cropper", {
                                        attrs: {
                                          src: _vm.challenge.image
                                            ? _vm.challenge.image
                                            : _vm.base_url +
                                              "/images/upload-img.jpg"
                                        },
                                        on: {
                                          imageChanged: function($event) {
                                            return _vm.imageChanged($event)
                                          }
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "validate",
                                            rawName: "v-validate",
                                            value: {
                                              required: _vm.challenge.id
                                                ? false
                                                : true
                                            },
                                            expression:
                                              "{'required' : challenge.id? false: true}"
                                          },
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.challenge.imageForError,
                                            expression:
                                              "challenge.imageForError"
                                          }
                                        ],
                                        attrs: {
                                          type: "hidden",
                                          name: "imageForError"
                                        },
                                        domProps: {
                                          value: _vm.challenge.imageForError
                                        },
                                        on: {
                                          input: function($event) {
                                            if ($event.target.composing) {
                                              return
                                            }
                                            _vm.$set(
                                              _vm.challenge,
                                              "imageForError",
                                              $event.target.value
                                            )
                                          }
                                        }
                                      }),
                                      _vm._v(" "),
                                      _vm.errors.first("imageForError")
                                        ? _c("span", [
                                            _vm._v(
                                              _vm._s(
                                                _vm.errors
                                                  .first("imageForError")
                                                  .replace(
                                                    "imageForError",
                                                    "image"
                                                  )
                                              )
                                            )
                                          ])
                                        : _vm._e()
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "div",
                                    {
                                      staticClass:
                                        "col-lg-10 col-md-10 col-sm-12"
                                    },
                                    [
                                      _c(
                                        "div",
                                        {
                                          staticClass:
                                            "admin-add-user-main pt-0"
                                        },
                                        [
                                          !_vm.isEdit && !_vm.isExerciseEdit
                                            ? _c("h3", [
                                                _vm._v("New FTFP Challenge ")
                                              ])
                                            : _vm._e(),
                                          _vm._v(" "),
                                          _vm.isEdit && !_vm.isExerciseEdit
                                            ? _c("h3", [
                                                _vm._v("Update FTFP Challenge ")
                                              ])
                                            : _vm._e(),
                                          _vm._v(" "),
                                          _vm.isEdit && _vm.isExerciseEdit
                                            ? _c("h3", [
                                                _vm._v("Update Exercise ")
                                              ])
                                            : _vm._e(),
                                          _vm._v(" "),
                                          _c("div", { staticClass: "mb-2" }, [
                                            _c(
                                              "label",
                                              {
                                                attrs: {
                                                  for: "challenge-title"
                                                }
                                              },
                                              [_vm._v("Challenge Title :")]
                                            ),
                                            _vm._v(" "),
                                            _c("input", {
                                              directives: [
                                                {
                                                  name: "validate",
                                                  rawName: "v-validate",
                                                  value: "required",
                                                  expression: "'required'"
                                                },
                                                {
                                                  name: "model",
                                                  rawName: "v-model",
                                                  value: _vm.challenge.title,
                                                  expression: "challenge.title"
                                                }
                                              ],
                                              staticClass:
                                                "admin-chlng-fit-top-input",
                                              attrs: {
                                                type: "text",
                                                name: "title",
                                                id: "challenge-title"
                                              },
                                              domProps: {
                                                value: _vm.challenge.title
                                              },
                                              on: {
                                                input: function($event) {
                                                  if ($event.target.composing) {
                                                    return
                                                  }
                                                  _vm.$set(
                                                    _vm.challenge,
                                                    "title",
                                                    $event.target.value
                                                  )
                                                }
                                              }
                                            }),
                                            _vm._v(" "),
                                            _c("span", [
                                              _vm._v(
                                                _vm._s(
                                                  _vm.errors.first("title")
                                                )
                                              )
                                            ])
                                          ]),
                                          _vm._v(" "),
                                          _c("div", { staticClass: "mb-2" }, [
                                            _c(
                                              "label",
                                              {
                                                attrs: {
                                                  for: "challenge-reward-points"
                                                }
                                              },
                                              [_vm._v("Reward Points: ")]
                                            ),
                                            _vm._v(" "),
                                            _c("input", {
                                              directives: [
                                                {
                                                  name: "validate",
                                                  rawName: "v-validate",
                                                  value:
                                                    "required|numeric|max_value:1000",
                                                  expression:
                                                    "'required|numeric|max_value:1000'"
                                                },
                                                {
                                                  name: "model",
                                                  rawName: "v-model",
                                                  value:
                                                    _vm.challenge.reward_points,
                                                  expression:
                                                    "challenge.reward_points"
                                                }
                                              ],
                                              staticClass:
                                                "admin-chlng-fit-top-input",
                                              attrs: {
                                                type: "number",
                                                min: "1",
                                                name: "points",
                                                id: "challenge-reward-points"
                                              },
                                              domProps: {
                                                value:
                                                  _vm.challenge.reward_points
                                              },
                                              on: {
                                                input: function($event) {
                                                  if ($event.target.composing) {
                                                    return
                                                  }
                                                  _vm.$set(
                                                    _vm.challenge,
                                                    "reward_points",
                                                    $event.target.value
                                                  )
                                                }
                                              }
                                            }),
                                            _vm._v(" "),
                                            _c("span", [
                                              _vm._v(
                                                _vm._s(
                                                  _vm.errors.first("points")
                                                )
                                              )
                                            ])
                                          ]),
                                          _vm._v(" "),
                                          _c("div", { staticClass: "mb-2" }, [
                                            _c(
                                              "label",
                                              {
                                                attrs: {
                                                  for: "challenge-description"
                                                }
                                              },
                                              [_vm._v("Description:")]
                                            ),
                                            _vm._v(" "),
                                            _c("textarea", {
                                              directives: [
                                                {
                                                  name: "model",
                                                  rawName: "v-model",
                                                  value:
                                                    _vm.challenge.description,
                                                  expression:
                                                    "challenge.description"
                                                }
                                              ],
                                              staticClass:
                                                "admin-chlng-fit-top-txtara",
                                              attrs: {
                                                id: "challenge-description"
                                              },
                                              domProps: {
                                                value: _vm.challenge.description
                                              },
                                              on: {
                                                input: function($event) {
                                                  if ($event.target.composing) {
                                                    return
                                                  }
                                                  _vm.$set(
                                                    _vm.challenge,
                                                    "description",
                                                    $event.target.value
                                                  )
                                                }
                                              }
                                            })
                                          ])
                                        ]
                                      )
                                    ]
                                  )
                                ])
                              : _vm._e(),
                            _vm._v(" "),
                            _vm._l(_vm.challenge.exercises, function(
                              exercise,
                              index
                            ) {
                              return (_vm.isExerciseEdit &&
                                exercise.id === _vm.editExerciseId) ||
                                !_vm.isExerciseEdit
                                ? _c(
                                    "div",
                                    { key: index, staticClass: "row" },
                                    [
                                      (_vm.isExerciseEdit &&
                                        exercise.id === _vm.editExerciseId) ||
                                      !_vm.isExerciseEdit
                                        ? _c(
                                            "div",
                                            {
                                              staticClass:
                                                "col-lg-2 col-md-2 col-sm-12"
                                            },
                                            [
                                              _c("cropper", {
                                                attrs: {
                                                  "el-idx": index,
                                                  src: exercise.image
                                                    ? exercise.image
                                                    : _vm.base_url +
                                                      "/images/upload-img.jpg"
                                                },
                                                on: {
                                                  imageChanged: function(
                                                    $event
                                                  ) {
                                                    return _vm.imageChanged(
                                                      $event,
                                                      index
                                                    )
                                                  }
                                                }
                                              }),
                                              _vm._v(" "),
                                              _c("input", {
                                                directives: [
                                                  {
                                                    name: "validate",
                                                    rawName: "v-validate",
                                                    value: {
                                                      required: _vm.challenge.id
                                                        ? false
                                                        : true
                                                    },
                                                    expression:
                                                      "{'required' : challenge.id? false: true}"
                                                  },
                                                  {
                                                    name: "model",
                                                    rawName: "v-model",
                                                    value:
                                                      exercise.imageForError,
                                                    expression:
                                                      "exercise.imageForError"
                                                  }
                                                ],
                                                attrs: {
                                                  type: "hidden",
                                                  name:
                                                    "exercise" +
                                                    index +
                                                    "imageForError"
                                                },
                                                domProps: {
                                                  value: exercise.imageForError
                                                },
                                                on: {
                                                  input: function($event) {
                                                    if (
                                                      $event.target.composing
                                                    ) {
                                                      return
                                                    }
                                                    _vm.$set(
                                                      exercise,
                                                      "imageForError",
                                                      $event.target.value
                                                    )
                                                  }
                                                }
                                              }),
                                              _vm._v(" "),
                                              _vm.errors.first(
                                                "exercise" +
                                                  index +
                                                  "imageForError"
                                              )
                                                ? _c("span", [
                                                    _vm._v(
                                                      _vm._s(
                                                        _vm.errors
                                                          .first(
                                                            "exercise" +
                                                              index +
                                                              "imageForError"
                                                          )
                                                          .replace(
                                                            "exercise" +
                                                              index +
                                                              "imageForError",
                                                            "image"
                                                          )
                                                      )
                                                    )
                                                  ])
                                                : _vm._e()
                                            ],
                                            1
                                          )
                                        : _vm._e(),
                                      _vm._v(" "),
                                      _c(
                                        "div",
                                        {
                                          staticClass:
                                            "col-lg-10 col-md-10 col-sm-12"
                                        },
                                        [
                                          _c(
                                            "div",
                                            {
                                              staticClass:
                                                "admin-add-user-main pt-0"
                                            },
                                            [
                                              _c("h3", [
                                                _vm._v(
                                                  "Exercise Details:\n                                                        "
                                                ),
                                                _vm.challenge.exercises.length >
                                                  1 && !_vm.isExerciseEdit
                                                  ? _c(
                                                      "button",
                                                      {
                                                        staticClass:
                                                          "btn btn-outline-danger float-right",
                                                        attrs: {
                                                          type: "button"
                                                        },
                                                        on: {
                                                          click: function(
                                                            $event
                                                          ) {
                                                            return _vm.removeExercise(
                                                              index
                                                            )
                                                          }
                                                        }
                                                      },
                                                      [
                                                        _c("i", {
                                                          staticClass:
                                                            "fa fa-times"
                                                        })
                                                      ]
                                                    )
                                                  : _vm._e()
                                              ]),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                { staticClass: "row" },
                                                [
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass:
                                                        "col-lg-6 col-md-12"
                                                    },
                                                    [
                                                      _c(
                                                        "div",
                                                        { staticClass: "mb-2" },
                                                        [
                                                          _c(
                                                            "label",
                                                            {
                                                              attrs: {
                                                                for:
                                                                  "exercise" +
                                                                  index +
                                                                  "-title"
                                                              }
                                                            },
                                                            [
                                                              _vm._v(
                                                                "Exercise Title :"
                                                              )
                                                            ]
                                                          ),
                                                          _vm._v(" "),
                                                          _c("input", {
                                                            directives: [
                                                              {
                                                                name:
                                                                  "validate",
                                                                rawName:
                                                                  "v-validate",
                                                                value:
                                                                  "required",
                                                                expression:
                                                                  "'required'"
                                                              },
                                                              {
                                                                name: "model",
                                                                rawName:
                                                                  "v-model",
                                                                value:
                                                                  exercise.title,
                                                                expression:
                                                                  "exercise.title"
                                                              }
                                                            ],
                                                            staticClass:
                                                              "admin-chlng-fit-top-input",
                                                            attrs: {
                                                              type: "text",
                                                              name:
                                                                "exercise" +
                                                                index +
                                                                "-title",
                                                              id:
                                                                "exercise" +
                                                                index +
                                                                "-title"
                                                            },
                                                            domProps: {
                                                              value:
                                                                exercise.title
                                                            },
                                                            on: {
                                                              input: function(
                                                                $event
                                                              ) {
                                                                if (
                                                                  $event.target
                                                                    .composing
                                                                ) {
                                                                  return
                                                                }
                                                                _vm.$set(
                                                                  exercise,
                                                                  "title",
                                                                  $event.target
                                                                    .value
                                                                )
                                                              }
                                                            }
                                                          }),
                                                          _vm._v(" "),
                                                          _c("span", [
                                                            _vm._v(
                                                              _vm._s(
                                                                _vm.getError(
                                                                  _vm.errors.first(
                                                                    "exercise" +
                                                                      index +
                                                                      "-title"
                                                                  )
                                                                )
                                                              )
                                                            )
                                                          ])
                                                        ]
                                                      ),
                                                      _vm._v(" "),
                                                      _c(
                                                        "div",
                                                        { staticClass: "mb-2" },
                                                        [
                                                          _c(
                                                            "label",
                                                            {
                                                              attrs: {
                                                                for:
                                                                  "exercise" +
                                                                  index +
                                                                  "-repeats"
                                                              }
                                                            },
                                                            [
                                                              _vm._v(
                                                                "Repeats :"
                                                              )
                                                            ]
                                                          ),
                                                          _vm._v(" "),
                                                          _c("input", {
                                                            directives: [
                                                              {
                                                                name:
                                                                  "validate",
                                                                rawName:
                                                                  "v-validate",
                                                                value:
                                                                  "required",
                                                                expression:
                                                                  "'required'"
                                                              },
                                                              {
                                                                name: "model",
                                                                rawName:
                                                                  "v-model",
                                                                value:
                                                                  exercise.repeat,
                                                                expression:
                                                                  "exercise.repeat"
                                                              }
                                                            ],
                                                            staticClass:
                                                              "admin-chlng-fit-top-input",
                                                            attrs: {
                                                              type: "number",
                                                              min: "1",
                                                              name:
                                                                "exercise" +
                                                                index +
                                                                "-repeats",
                                                              id:
                                                                "exercise" +
                                                                index +
                                                                "-repeats"
                                                            },
                                                            domProps: {
                                                              value:
                                                                exercise.repeat
                                                            },
                                                            on: {
                                                              input: function(
                                                                $event
                                                              ) {
                                                                if (
                                                                  $event.target
                                                                    .composing
                                                                ) {
                                                                  return
                                                                }
                                                                _vm.$set(
                                                                  exercise,
                                                                  "repeat",
                                                                  $event.target
                                                                    .value
                                                                )
                                                              }
                                                            }
                                                          }),
                                                          _vm._v(" "),
                                                          _c("span", [
                                                            _vm._v(
                                                              "\n                                                                    " +
                                                                _vm._s(
                                                                  _vm.getError(
                                                                    _vm.errors.first(
                                                                      "exercise" +
                                                                        index +
                                                                        "-repeats"
                                                                    )
                                                                  )
                                                                ) +
                                                                "\n                                                                "
                                                            )
                                                          ])
                                                        ]
                                                      ),
                                                      _vm._v(" "),
                                                      _c(
                                                        "div",
                                                        { staticClass: "mb-2" },
                                                        [
                                                          _c(
                                                            "label",
                                                            {
                                                              attrs: {
                                                                for:
                                                                  "exercise" +
                                                                  index +
                                                                  "-category"
                                                              }
                                                            },
                                                            [
                                                              _vm._v(
                                                                "Category :"
                                                              )
                                                            ]
                                                          ),
                                                          _vm._v(" "),
                                                          _c(
                                                            "select",
                                                            {
                                                              directives: [
                                                                {
                                                                  name:
                                                                    "validate",
                                                                  rawName:
                                                                    "v-validate",
                                                                  value:
                                                                    "required",
                                                                  expression:
                                                                    "'required'"
                                                                },
                                                                {
                                                                  name: "model",
                                                                  rawName:
                                                                    "v-model",
                                                                  value:
                                                                    exercise.category_id,
                                                                  expression:
                                                                    "exercise.category_id"
                                                                }
                                                              ],
                                                              staticClass:
                                                                "admin-chlng-fit-top-input",
                                                              attrs: {
                                                                name:
                                                                  "exercise" +
                                                                  index +
                                                                  "-category",
                                                                id:
                                                                  "exercise" +
                                                                  index +
                                                                  "-category"
                                                              },
                                                              on: {
                                                                change: function(
                                                                  $event
                                                                ) {
                                                                  var $$selectedVal = Array.prototype.filter
                                                                    .call(
                                                                      $event
                                                                        .target
                                                                        .options,
                                                                      function(
                                                                        o
                                                                      ) {
                                                                        return o.selected
                                                                      }
                                                                    )
                                                                    .map(
                                                                      function(
                                                                        o
                                                                      ) {
                                                                        var val =
                                                                          "_value" in
                                                                          o
                                                                            ? o._value
                                                                            : o.value
                                                                        return val
                                                                      }
                                                                    )
                                                                  _vm.$set(
                                                                    exercise,
                                                                    "category_id",
                                                                    $event
                                                                      .target
                                                                      .multiple
                                                                      ? $$selectedVal
                                                                      : $$selectedVal[0]
                                                                  )
                                                                }
                                                              }
                                                            },
                                                            _vm._l(
                                                              _vm.categories,
                                                              function(
                                                                category,
                                                                catIndex
                                                              ) {
                                                                return _c(
                                                                  "option",
                                                                  {
                                                                    key:
                                                                      category.id,
                                                                    domProps: {
                                                                      value:
                                                                        category.id
                                                                    }
                                                                  },
                                                                  [
                                                                    _vm._v(
                                                                      _vm._s(
                                                                        category.title
                                                                      )
                                                                    )
                                                                  ]
                                                                )
                                                              }
                                                            ),
                                                            0
                                                          ),
                                                          _vm._v(" "),
                                                          _c("span", [
                                                            _vm._v(
                                                              _vm._s(
                                                                _vm.getError(
                                                                  _vm.errors.first(
                                                                    "exercise" +
                                                                      index +
                                                                      "-category"
                                                                  )
                                                                )
                                                              )
                                                            )
                                                          ])
                                                        ]
                                                      )
                                                    ]
                                                  ),
                                                  _vm._v(" "),
                                                  _vm._m(0, true)
                                                ]
                                              )
                                            ]
                                          )
                                        ]
                                      )
                                    ]
                                  )
                                : _vm._e()
                            }),
                            _vm._v(" "),
                            _c("div", { staticClass: "row" }, [
                              _c("div", {
                                staticClass: "col-lg-2 col-md-2 col-sm-12"
                              }),
                              _vm._v(" "),
                              _c(
                                "div",
                                {
                                  staticClass: "col-lg-10 col-md-10 col-sm-12"
                                },
                                [
                                  _c(
                                    "div",
                                    { staticClass: "admin-add-user-main pt-0" },
                                    [
                                      _c("div", { staticClass: "row" }, [
                                        !_vm.isExerciseEdit
                                          ? _c(
                                              "div",
                                              {
                                                staticClass:
                                                  "col-lg-6 col-md-12"
                                              },
                                              [
                                                _c(
                                                  "button",
                                                  {
                                                    staticClass:
                                                      "fftp-add-more-btn",
                                                    attrs: { type: "button" },
                                                    on: {
                                                      click: function($event) {
                                                        $event.preventDefault()
                                                        return _vm.addExercise(
                                                          $event
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [
                                                    _c("i", {
                                                      staticClass:
                                                        "fa fa-plus-circle"
                                                    }),
                                                    _vm._v(
                                                      " Add more exercise\n                                                            "
                                                    )
                                                  ]
                                                )
                                              ]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _c("div", {
                                          staticClass: "col-lg-6 col-md-12"
                                        })
                                      ]),
                                      _vm._v(" "),
                                      !_vm.isEdit && !_vm.isExerciseEdit
                                        ? _c(
                                            "button",
                                            {
                                              staticClass: "cnt-btnn2",
                                              attrs: { type: "submit" }
                                            },
                                            [_vm._v("Create Challenge")]
                                          )
                                        : _vm._e(),
                                      _vm._v(" "),
                                      _vm.isEdit && !_vm.isExerciseEdit
                                        ? _c(
                                            "button",
                                            {
                                              staticClass: "cnt-btnn2",
                                              attrs: { type: "submit" }
                                            },
                                            [_vm._v("Update Challenge")]
                                          )
                                        : _vm._e(),
                                      _vm._v(" "),
                                      _vm.isEdit && _vm.isExerciseEdit
                                        ? _c(
                                            "button",
                                            {
                                              staticClass: "cnt-btnn2",
                                              attrs: { type: "submit" }
                                            },
                                            [_vm._v("Update Exercise")]
                                          )
                                        : _vm._e()
                                    ]
                                  )
                                ]
                              )
                            ])
                          ],
                          2
                        )
                      ])
                    ]
                  )
                ])
              ])
            ]
          )
        ])
      ])
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-lg-6 col-md-12" }, [
      _c("div", { staticClass: "mb-2" }, [
        _c("label", [_vm._v("Description:")]),
        _vm._v(" "),
        _c("textarea", { staticClass: "admin-chlng-fit-top-txtara2" })
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/compoments/CropperComponent.vue?vue&type=template&id=3bbe263c&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/compoments/CropperComponent.vue?vue&type=template&id=3bbe263c& ***!
  \*******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "cropper-image-container" }, [
    _c("img", {
      ref: "img" + _vm.elIdx,
      staticClass: "img-fluid",
      staticStyle: { "min-width": "100%" },
      attrs: { src: _vm.image, id: "personal_img" + _vm.elIdx }
    }),
    _vm._v(" "),
    _c(
      "label",
      {
        staticClass: "uplrd-img-btn",
        staticStyle: { "margin-top": "0" },
        attrs: { for: "upload" + _vm.elIdx }
      },
      [
        _c("i", {
          staticClass: "fa fa-camera",
          attrs: { "aria-hidden": "true" }
        })
      ]
    ),
    _vm._v(" "),
    _c(
      "button",
      {
        staticClass: "btn btn-round",
        staticStyle: { display: "none" },
        attrs: { type: "button" },
        on: { click: _vm.crop }
      },
      [_c("i", { staticClass: "fa fa-check" })]
    ),
    _vm._v(" "),
    _c("input", {
      ref: "imageInput" + _vm.elIdx,
      staticClass: "d-none",
      attrs: {
        type: "file",
        accept: "image/*",
        id: "upload" + _vm.elIdx,
        name: "personal_img" + _vm.elIdx
      },
      on: {
        change: function($event) {
          return _vm.readURL()
        }
      }
    })
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/admin/components/challenges/CreateComponent.vue":
/*!**********************************************************************!*\
  !*** ./resources/js/admin/components/challenges/CreateComponent.vue ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CreateComponent_vue_vue_type_template_id_c9790a26___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CreateComponent.vue?vue&type=template&id=c9790a26& */ "./resources/js/admin/components/challenges/CreateComponent.vue?vue&type=template&id=c9790a26&");
/* harmony import */ var _CreateComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CreateComponent.vue?vue&type=script&lang=js& */ "./resources/js/admin/components/challenges/CreateComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _CreateComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CreateComponent_vue_vue_type_template_id_c9790a26___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CreateComponent_vue_vue_type_template_id_c9790a26___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/admin/components/challenges/CreateComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/admin/components/challenges/CreateComponent.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/admin/components/challenges/CreateComponent.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CreateComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/challenges/CreateComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/admin/components/challenges/CreateComponent.vue?vue&type=template&id=c9790a26&":
/*!*****************************************************************************************************!*\
  !*** ./resources/js/admin/components/challenges/CreateComponent.vue?vue&type=template&id=c9790a26& ***!
  \*****************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateComponent_vue_vue_type_template_id_c9790a26___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CreateComponent.vue?vue&type=template&id=c9790a26& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/challenges/CreateComponent.vue?vue&type=template&id=c9790a26&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateComponent_vue_vue_type_template_id_c9790a26___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateComponent_vue_vue_type_template_id_c9790a26___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/compoments/CropperComponent.vue":
/*!******************************************************!*\
  !*** ./resources/js/compoments/CropperComponent.vue ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CropperComponent_vue_vue_type_template_id_3bbe263c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CropperComponent.vue?vue&type=template&id=3bbe263c& */ "./resources/js/compoments/CropperComponent.vue?vue&type=template&id=3bbe263c&");
/* harmony import */ var _CropperComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CropperComponent.vue?vue&type=script&lang=js& */ "./resources/js/compoments/CropperComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _CropperComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./CropperComponent.vue?vue&type=style&index=0&lang=css& */ "./resources/js/compoments/CropperComponent.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _CropperComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CropperComponent_vue_vue_type_template_id_3bbe263c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CropperComponent_vue_vue_type_template_id_3bbe263c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/compoments/CropperComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/compoments/CropperComponent.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/js/compoments/CropperComponent.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CropperComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./CropperComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/compoments/CropperComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CropperComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/compoments/CropperComponent.vue?vue&type=style&index=0&lang=css&":
/*!***************************************************************************************!*\
  !*** ./resources/js/compoments/CropperComponent.vue?vue&type=style&index=0&lang=css& ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_CropperComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader??ref--7-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/vue-loader/lib??vue-loader-options!./CropperComponent.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/compoments/CropperComponent.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_CropperComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_CropperComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_CropperComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_CropperComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_CropperComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/compoments/CropperComponent.vue?vue&type=template&id=3bbe263c&":
/*!*************************************************************************************!*\
  !*** ./resources/js/compoments/CropperComponent.vue?vue&type=template&id=3bbe263c& ***!
  \*************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CropperComponent_vue_vue_type_template_id_3bbe263c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./CropperComponent.vue?vue&type=template&id=3bbe263c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/compoments/CropperComponent.vue?vue&type=template&id=3bbe263c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CropperComponent_vue_vue_type_template_id_3bbe263c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CropperComponent_vue_vue_type_template_id_3bbe263c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);