(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["user-show"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/users/ShowComponent.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/admin/components/users/ShowComponent.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _compoments_TimerComponent__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../compoments/TimerComponent */ "./resources/js/compoments/TimerComponent.vue");
/* harmony import */ var vue_the_mask__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-the-mask */ "./node_modules/vue-the-mask/dist/vue-the-mask.js");
/* harmony import */ var vue_the_mask__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue_the_mask__WEBPACK_IMPORTED_MODULE_1__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    TimerCounter: _compoments_TimerComponent__WEBPACK_IMPORTED_MODULE_0__["default"],
    TheMask: vue_the_mask__WEBPACK_IMPORTED_MODULE_1__["TheMask"]
  },
  data: function data() {
    return {
      statuses: window.statuses,
      starttime: new Date(),
      endtime: new Date(),
      base_url: window.base_url,
      isLoading: true,
      currentTab: 'details',
      editingMode: false,
      user: {},
      friends: [],
      componentForm: {
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      }
    };
  },
  mounted: function mounted() {
    this.endtime.setHours(this.starttime.getHours() + 1);
    this.currentTab = this.$route.params.tab;
    this.getUser();
    if (this.currentTab === 'friends') this.loadFriends();
  },
  methods: {
    getendtime: function getendtime(created_at, duration) {
      var d = new Date(created_at);
      d.setHours(d.getHours() + duration);
      return d;
    },
    getUser: function getUser() {
      var _this = this;

      this.isLoading = true;
      axios.get("/users/".concat(this.$route.params.id)).then(function (_ref) {
        var data = _ref.data;
        _this.user = data;
        _this.isLoading = false;
      });
    },
    loadFriends: function loadFriends() {
      var _this2 = this;

      this.isLoading = true;
      if (this.currentTab !== 'friends') return;
      axios.get("/users/".concat(this.$route.params.id, "/friends")).then(function (_ref2) {
        var data = _ref2.data;
        _this2.friends = data;
        _this2.isLoading = false;
      });
    },
    onPlaceChanged: function onPlaceChanged(place) {
      var fields = {
        locality: 'city',
        administrative_area_level_1: 'address_state',
        country: 'country',
        postal_code: 'postal_code'
      };
      console.log(place);

      for (var i = 0; i < place.place.address_components.length; i++) {
        var addressType = place.place.address_components[i].types[0];

        if (this.componentForm[addressType]) {
          this.user[fields[addressType]] = place.place.address_components[i][this.componentForm[addressType]];
        }
      }
    },
    onNoResult: function onNoResult() {
      console.log('no result place');
    },
    changeTab: function changeTab(tab) {
      if (tab === this.currentTab) return;
      this.currentTab = tab;
      this.$router.push({
        name: 'users.show',
        params: {
          id: this.user.id,
          tab: tab
        }
      });
    },
    redirectTo: function redirectTo(userId) {
      this.$router.push({
        name: 'users.show',
        params: {
          id: userId,
          'tab': 'details'
        }
      });
    },
    redirectToChallengePage: function redirectToChallengePage(challengeId) {
      this.$router.push({
        name: 'user.challenges.show',
        params: {
          id: challengeId,
          'userId': this.user.id
        }
      });
    },
    date_format: function date_format(date) {
      var d = new Date(date);
      var month = d.getMonth() + 1;
      var day = d.getDate();
      var year = d.getFullYear();
      return "".concat(month < 10 ? "0" + month : month, "/").concat(day < 10 ? "0" + day : day, "/").concat(year);
    },
    update: function update() {
      var _this3 = this;

      this.$validator.validateAll().then(function (result) {
        if (!result) return;
        _this3.isLoading = true;
        axios.put("/users/".concat(_this3.user.id), _this3.user).then(function (data) {
          _this3.isLoading = false;

          _this3.$toastr.success('User has been updated successfully', 'Success', {}); // this.$router.push({name: 'users.index'});


          _this3.editingMode = false;
        })["catch"](function (e) {
          return _this3.isLoading = false;
        });
      });
    }
  },
  computed: {
    initatedCount: function initatedCount() {
      var _this4 = this;

      if (!this.user.challenges) return 0;
      return this.user.challenges.filter(function (c) {
        return c.sender_id === _this4.user.id;
      }).length;
    },
    acceptedCount: function acceptedCount() {
      var _this5 = this;

      if (!this.user.challenges) return 0;
      return this.user.challenges.filter(function (c) {
        return c.recipient_id === _this5.user.id && c.status === 1;
      }).length;
    },
    completedCount: function completedCount() {
      if (!this.user.challenges) return 0;
      return this.user.challenges.filter(function (c) {
        return c.status === 5;
      }).length;
    },
    verificationCount: function verificationCount() {
      if (!this.user.challenges) return 0;
      return this.user.challenges.filter(function (c) {
        return c.status === 3;
      }).length;
    }
  },
  watch: {
    'currentTab': function currentTab() {
      if (this.currentTab === 'friends') this.loadFriends();
    },
    '$route.params.id': function $routeParamsId() {
      if (this.currentTab.toLowerCase() !== 'details') this.currentTab = 'details';
      this.getUser();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/compoments/TimerComponent.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/compoments/TimerComponent.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['starttime', 'endtime', 'trans'],
  data: function data() {
    return {
      timer: "",
      wordString: {},
      start: "",
      end: "",
      interval: "",
      days: "",
      minutes: "",
      hours: "",
      seconds: "",
      message: "",
      statusType: "",
      statusText: ""
    };
  },
  created: function created() {
    this.wordString = JSON.parse(this.trans);
  },
  mounted: function mounted() {
    var _this = this;

    this.start = new Date(this.starttime).getTime();
    this.end = new Date(this.endtime).getTime(); // Update the count down every 1 second

    this.timerCount(this.start, this.end);
    this.interval = setInterval(function () {
      _this.timerCount(_this.start, _this.end);
    }, 1000);
  },
  methods: {
    timerCount: function timerCount(start, end) {
      // Get todays date and time
      var now = new Date().getTime(); // Find the distance between now an the count down date

      var distance = start - now;
      var passTime = end - now;

      if (distance < 0 && passTime < 0) {
        this.message = this.wordString.expired;
        this.statusType = "expired";
        this.statusText = this.wordString.status.expired;
        clearInterval(this.interval);
        return;
      } else if (distance < 0 && passTime > 0) {
        this.calcTime(passTime);
        this.message = this.wordString.running;
        this.statusType = "running";
        this.statusText = this.wordString.status.running;
      } else if (distance > 0 && passTime > 0) {
        this.calcTime(distance);
        this.message = this.wordString.upcoming;
        this.statusType = "upcoming";
        this.statusText = this.wordString.status.upcoming;
      }
    },
    calcTime: function calcTime(dist) {
      // Time calculations for days, hours, minutes and seconds
      this.days = Math.floor(dist / (1000 * 60 * 60 * 24));
      this.hours = Math.floor(dist % (1000 * 60 * 60 * 24) / (1000 * 60 * 60));
      this.minutes = Math.floor(dist % (1000 * 60 * 60) / (1000 * 60));
      this.seconds = Math.floor(dist % (1000 * 60) / 1000);
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/users/ShowComponent.vue?vue&type=template&id=7033a65e&":
/*!****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/admin/components/users/ShowComponent.vue?vue&type=template&id=7033a65e& ***!
  \****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "app-content content" },
    [
      _vm.isLoading
        ? _c("BlockUI", { attrs: { message: "Loading..." } }, [
            _c("i", { staticClass: "fa fa-cog fa-spin fa-3x fa-fw" })
          ])
        : _vm._e(),
      _vm._v(" "),
      _c("div", { staticClass: "content-wrapper" }, [
        _c("div", { staticClass: "content-body" }, [
          _c("section", { attrs: { id: "combination-charts" } }, [
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-12" }, [
                _c(
                  "div",
                  { staticClass: "card rounded p-3 admin-overview-main" },
                  [
                    _c("div", { staticClass: "row" }, [
                      _c("div", { staticClass: "col-lg-12" }, [
                        _c("h1", [_vm._v("User Profile")]),
                        _vm._v(" "),
                        !_vm.isLoading
                          ? _c(
                              "div",
                              { staticClass: "admin-user-profile-top" },
                              [
                                _c(
                                  "div",
                                  { staticClass: "row align-items-center" },
                                  [
                                    _c(
                                      "div",
                                      { staticClass: "col-md-3 col-sm-12" },
                                      [
                                        _c(
                                          "div",
                                          {
                                            staticClass:
                                              "admin-user-profile-top-image-main"
                                          },
                                          [
                                            _c("img", {
                                              attrs: {
                                                src: _vm.profileImage(
                                                  _vm.user.image,
                                                  _vm.user.name
                                                ),
                                                alt: _vm.user.name
                                              }
                                            }),
                                            _vm._v(" "),
                                            _c("h6", [
                                              _vm._v(_vm._s(_vm.user.name))
                                            ]),
                                            _vm._v(" "),
                                            _c("span", [
                                              _vm._v(_vm._s(_vm.user.email))
                                            ])
                                          ]
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      { staticClass: "col-md-7 col-sm-12" },
                                      [
                                        _c("div", { staticClass: "row" }, [
                                          _c(
                                            "div",
                                            {
                                              staticClass: "col-md-3 col-sm-12"
                                            },
                                            [
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "admin-user-profile-top-boxs"
                                                },
                                                [
                                                  _c("span", [_vm._v("Level")]),
                                                  _vm._v(" "),
                                                  _c("h5", [
                                                    _vm._v(
                                                      _vm._s(
                                                        _vm.user.state.level
                                                      )
                                                    )
                                                  ])
                                                ]
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "div",
                                            {
                                              staticClass: "col-md-3 col-sm-12"
                                            },
                                            [
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "admin-user-profile-top-boxs"
                                                },
                                                [
                                                  _c("span", [
                                                    _vm._v("Challenges")
                                                  ]),
                                                  _vm._v(" "),
                                                  _c("h5", [
                                                    _vm._v(
                                                      _vm._s(
                                                        _vm.user.state.level
                                                      )
                                                    )
                                                  ])
                                                ]
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "div",
                                            {
                                              staticClass: "col-md-3 col-sm-12"
                                            },
                                            [
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "admin-user-profile-top-boxs"
                                                },
                                                [
                                                  _c("span", [
                                                    _vm._v("Total Points:")
                                                  ]),
                                                  _vm._v(" "),
                                                  _c("h5", [
                                                    _vm._v(
                                                      _vm._s(
                                                        _vm.user.state.points
                                                      )
                                                    )
                                                  ])
                                                ]
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "div",
                                            {
                                              staticClass: "col-md-3 col-sm-12"
                                            },
                                            [
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "admin-user-profile-top-boxs"
                                                },
                                                [
                                                  _c("span", [
                                                    _vm._v("Votes:")
                                                  ]),
                                                  _vm._v(" "),
                                                  _c("h5", [
                                                    _vm._v(
                                                      _vm._s(
                                                        _vm.user.state
                                                          .challenge_votes
                                                      )
                                                    )
                                                  ])
                                                ]
                                              )
                                            ]
                                          )
                                        ])
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      { staticClass: "col-md-2 col-sm-12" },
                                      [
                                        _c(
                                          "div",
                                          {
                                            staticClass:
                                              "admin-user-profile-top-boxs-logo"
                                          },
                                          [
                                            _c("img", {
                                              staticStyle: {
                                                "max-width": "134px"
                                              },
                                              attrs: {
                                                src:
                                                  _vm.base_url +
                                                  "/images/" +
                                                  _vm.user.state.path +
                                                  "_PATH.png",
                                                alt:
                                                  _vm.user.state.path + " PATH"
                                              }
                                            }),
                                            _vm._v(" "),
                                            _c("h5", [
                                              _vm._v(
                                                _vm._s(_vm.user.state.path) +
                                                  " Path"
                                              )
                                            ])
                                          ]
                                        )
                                      ]
                                    )
                                  ]
                                )
                              ]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _c("div", { staticClass: "admin-top-red-nav" }, [
                          _c("ul", [
                            _c(
                              "li",
                              {
                                class: { active: _vm.currentTab === "details" }
                              },
                              [
                                _c(
                                  "a",
                                  {
                                    attrs: { href: "#" },
                                    on: {
                                      click: function($event) {
                                        $event.preventDefault()
                                        return _vm.changeTab("details")
                                      }
                                    }
                                  },
                                  [_vm._v("Details")]
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "li",
                              {
                                class: {
                                  active: _vm.currentTab === "challenge"
                                }
                              },
                              [
                                _c(
                                  "a",
                                  {
                                    attrs: { href: "#" },
                                    on: {
                                      click: function($event) {
                                        $event.preventDefault()
                                        return _vm.changeTab("challenge")
                                      }
                                    }
                                  },
                                  [_vm._v("Challenges")]
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "li",
                              {
                                class: { active: _vm.currentTab === "friends" }
                              },
                              [
                                _c(
                                  "a",
                                  {
                                    attrs: { href: "#" },
                                    on: {
                                      click: function($event) {
                                        $event.preventDefault()
                                        return _vm.changeTab("friends")
                                      }
                                    }
                                  },
                                  [_vm._v("Friends")]
                                )
                              ]
                            )
                          ]),
                          _vm._v(" "),
                          !_vm.editingMode
                            ? _c(
                                "a",
                                {
                                  staticClass: "closed-span float-right",
                                  attrs: { href: "#" },
                                  on: {
                                    click: function($event) {
                                      $event.preventDefault()
                                      _vm.editingMode = true
                                    }
                                  }
                                },
                                [_vm._v("Edit")]
                              )
                            : _vm._e()
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "clearfix" }),
                        _vm._v(" "),
                        _vm.currentTab === "friends"
                          ? _c(
                              "div",
                              { staticClass: "row" },
                              _vm._l(_vm.friends, function(friend, index) {
                                return _c(
                                  "div",
                                  {
                                    key: index,
                                    staticClass: "col-lg-3 col-md-6 col-sm-12"
                                  },
                                  [
                                    _c(
                                      "div",
                                      {
                                        staticClass:
                                          "chalng-members-box admin-user-frinds-boxs box-shadow-1",
                                        staticStyle: { cursor: "pointer" },
                                        on: {
                                          click: function($event) {
                                            return _vm.redirectTo(friend.id)
                                          }
                                        }
                                      },
                                      [
                                        _c("img", {
                                          attrs: {
                                            src: _vm.profileImage(
                                              friend.image,
                                              friend.name
                                            ),
                                            alt: ""
                                          }
                                        }),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "flex-column" },
                                          [
                                            _c("p", [
                                              _vm._v(_vm._s(friend.name))
                                            ]),
                                            _vm._v(" "),
                                            _c("span", [
                                              _vm._v(
                                                "Level: " +
                                                  _vm._s(friend.state.level)
                                              )
                                            ]),
                                            _vm._v(" "),
                                            _c("span", [
                                              _vm._v(
                                                "Path: " +
                                                  _vm._s(friend.state.path)
                                              )
                                            ])
                                          ]
                                        )
                                      ]
                                    )
                                  ]
                                )
                              }),
                              0
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.currentTab === "challenge"
                          ? _c(
                              "div",
                              { staticClass: "user-profile-bottom-fftp" },
                              [
                                _c(
                                  "div",
                                  { staticClass: "user-profile-challenges" },
                                  [
                                    _c("div", { staticClass: "row" }, [
                                      _c(
                                        "div",
                                        {
                                          staticClass:
                                            "col-xl-3 col-lg-6 col-md-6 col-sm-12 mb-2"
                                        },
                                        [
                                          _c(
                                            "div",
                                            { staticClass: "employ-top-boxs" },
                                            [
                                              _c("h5", [_vm._v("Initiated")]),
                                              _vm._v(" "),
                                              _c("span", [
                                                _vm._v(
                                                  _vm._s(_vm.initatedCount)
                                                )
                                              ])
                                            ]
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "div",
                                        {
                                          staticClass:
                                            "col-xl-3 col-lg-6 col-md-6 col-sm-12 mb-2"
                                        },
                                        [
                                          _c(
                                            "div",
                                            { staticClass: "employ-top-boxs" },
                                            [
                                              _c("h5", [_vm._v("Accepted")]),
                                              _vm._v(" "),
                                              _c(
                                                "span",
                                                { staticClass: "bg-danger" },
                                                [
                                                  _vm._v(
                                                    _vm._s(_vm.acceptedCount)
                                                  )
                                                ]
                                              )
                                            ]
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "div",
                                        {
                                          staticClass:
                                            "col-xl-3 col-lg-6 col-md-6 col-sm-12 mb-2"
                                        },
                                        [
                                          _c(
                                            "div",
                                            { staticClass: "employ-top-boxs" },
                                            [
                                              _c("h5", [_vm._v("Completed")]),
                                              _vm._v(" "),
                                              _c("span", [
                                                _vm._v(
                                                  _vm._s(_vm.completedCount)
                                                )
                                              ])
                                            ]
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "div",
                                        {
                                          staticClass:
                                            "col-xl-3 col-lg-6 col-md-6 col-sm-12 mb-2"
                                        },
                                        [
                                          _c(
                                            "div",
                                            { staticClass: "employ-top-boxs" },
                                            [
                                              _c("h5", [
                                                _vm._v("Verification")
                                              ]),
                                              _vm._v(" "),
                                              _c(
                                                "span",
                                                { staticClass: "bg-danger" },
                                                [
                                                  _vm._v(
                                                    _vm._s(
                                                      _vm.verificationCount
                                                    )
                                                  )
                                                ]
                                              )
                                            ]
                                          )
                                        ]
                                      )
                                    ])
                                  ]
                                ),
                                _vm._v(" "),
                                _c("h1", [_vm._v("All Challenges")]),
                                _vm._v(" "),
                                _vm.user.challenges
                                  ? _c(
                                      "div",
                                      { staticClass: "row" },
                                      [
                                        _vm.user.challenges.length === 0
                                          ? _c(
                                              "div",
                                              {
                                                staticClass:
                                                  "col-xl-3 col-lg-6 col-md-6 col-sm-12 mb-2"
                                              },
                                              [_vm._m(0)]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm._l(_vm.user.challenges, function(
                                          challenge
                                        ) {
                                          return _vm.user.challenges.length
                                            ? _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "col-xl-3 col-lg-6 col-md-6 col-sm-12 mb-2",
                                                  staticStyle: {
                                                    cursor: "pointer"
                                                  },
                                                  on: {
                                                    click: function($event) {
                                                      return _vm.redirectToChallengePage(
                                                        challenge.id
                                                      )
                                                    }
                                                  }
                                                },
                                                [
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass:
                                                        "user-profile-btm-box"
                                                    },
                                                    [
                                                      _c(
                                                        "div",
                                                        {
                                                          staticClass:
                                                            "top-head"
                                                        },
                                                        [
                                                          _c("h3", [
                                                            _vm._v(
                                                              _vm._s(
                                                                challenge
                                                                  .challenge_data
                                                                  .title
                                                              )
                                                            )
                                                          ]),
                                                          _vm._v(" "),
                                                          _c(
                                                            "div",
                                                            {
                                                              staticClass:
                                                                "d-flex flex-row align-items-center justify-content-between"
                                                            },
                                                            [
                                                              _c(
                                                                "div",
                                                                {
                                                                  staticClass:
                                                                    "flex-column"
                                                                },
                                                                [
                                                                  _c("img", {
                                                                    staticClass:
                                                                      "pro-img",
                                                                    attrs: {
                                                                      src:
                                                                        challenge
                                                                          .sender
                                                                          .image,
                                                                      alt: ""
                                                                    }
                                                                  }),
                                                                  _vm._v(" "),
                                                                  _c("span", [
                                                                    _vm._v(
                                                                      _vm._s(
                                                                        challenge
                                                                          .sender
                                                                          .name
                                                                      ) +
                                                                        " " +
                                                                        _vm._s(
                                                                          challenge
                                                                            .sender
                                                                            .id
                                                                        )
                                                                    )
                                                                  ])
                                                                ]
                                                              ),
                                                              _vm._v(" "),
                                                              _c("span", [
                                                                _vm._v("VS")
                                                              ]),
                                                              _vm._v(" "),
                                                              _c(
                                                                "div",
                                                                {
                                                                  staticClass:
                                                                    "flex-column"
                                                                },
                                                                [
                                                                  _c("img", {
                                                                    staticClass:
                                                                      "pro-img",
                                                                    attrs: {
                                                                      src:
                                                                        challenge
                                                                          .recipient
                                                                          .image,
                                                                      alt: ""
                                                                    }
                                                                  }),
                                                                  _vm._v(" "),
                                                                  _c("span", [
                                                                    _vm._v(
                                                                      _vm._s(
                                                                        challenge
                                                                          .recipient
                                                                          .name
                                                                      ) +
                                                                        " " +
                                                                        _vm._s(
                                                                          challenge.id
                                                                        )
                                                                    )
                                                                  ])
                                                                ]
                                                              )
                                                            ]
                                                          )
                                                        ]
                                                      ),
                                                      _vm._v(" "),
                                                      _c(
                                                        "span",
                                                        {
                                                          staticClass:
                                                            "active-span"
                                                        },
                                                        [
                                                          _vm._v(
                                                            _vm._s(
                                                              _vm.statuses[
                                                                challenge.status
                                                              ]
                                                            )
                                                          )
                                                        ]
                                                      ),
                                                      _vm._v(" "),
                                                      _c(
                                                        "div",
                                                        { staticClass: "p-1" },
                                                        [
                                                          _c("h4", [
                                                            _c("span", [
                                                              _vm._v("Exercise")
                                                            ]),
                                                            _vm._v(
                                                              " " +
                                                                _vm._s(
                                                                  challenge
                                                                    .challenge_data
                                                                    .title
                                                                )
                                                            )
                                                          ]),
                                                          _vm._v(" "),
                                                          _c(
                                                            "div",
                                                            {
                                                              staticClass:
                                                                "d-flex text-center justify-content-around"
                                                            },
                                                            [
                                                              _c(
                                                                "span",
                                                                {
                                                                  staticClass:
                                                                    "date-time"
                                                                },
                                                                [
                                                                  _c("i", {
                                                                    staticClass:
                                                                      "fa fa-clock-o",
                                                                    attrs: {
                                                                      "aria-hidden":
                                                                        "true"
                                                                    }
                                                                  }),
                                                                  _vm._v(" "),
                                                                  _c(
                                                                    "timer-counter",
                                                                    {
                                                                      attrs: {
                                                                        starttime:
                                                                          challenge.created_at,
                                                                        endtime:
                                                                          challenge.time_to_complete,
                                                                        trans:
                                                                          '{\n                                                                                     "day":"Day",\n                                                                                     "hours":"Hours",\n                                                                                     "minutes":"Minuts",\n                                                                                     "seconds":"Seconds",\n                                                                                     "expired":"Challenge has been expired.",\n                                                                                     "running":"Left.",\n                                                                                     "upcoming":"Till start of event.",\n                                                                                     "status": {\n                                                                                        "expired":"Expired",\n                                                                                        "running":"Running",\n                                                                                        "upcoming":"Future"\n                                                                               }}'
                                                                      }
                                                                    }
                                                                  )
                                                                ],
                                                                1
                                                              ),
                                                              _vm._v(" "),
                                                              _c(
                                                                "span",
                                                                {
                                                                  staticClass:
                                                                    "date-time"
                                                                },
                                                                [
                                                                  _c("i", {
                                                                    staticClass:
                                                                      "fa fa-calendar",
                                                                    attrs: {
                                                                      "aria-hidden":
                                                                        "true"
                                                                    }
                                                                  }),
                                                                  _vm._v(
                                                                    " Date " +
                                                                      _vm._s(
                                                                        _vm.date_format(
                                                                          challenge.created_at.substr(
                                                                            0,
                                                                            10
                                                                          )
                                                                        )
                                                                      )
                                                                  )
                                                                ]
                                                              )
                                                            ]
                                                          )
                                                        ]
                                                      )
                                                    ]
                                                  )
                                                ]
                                              )
                                            : _vm._e()
                                        })
                                      ],
                                      2
                                    )
                                  : _vm._e()
                              ]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.currentTab === "details"
                          ? _c(
                              "form",
                              {
                                staticClass: "row",
                                on: {
                                  submit: function($event) {
                                    $event.preventDefault()
                                    return _vm.update()
                                  }
                                }
                              },
                              [
                                _c(
                                  "div",
                                  {
                                    staticClass: "col-xl-6 col-lg-6 col-md-12"
                                  },
                                  [
                                    _c(
                                      "div",
                                      {
                                        staticClass:
                                          "row user-profille-inner-row"
                                      },
                                      [
                                        _vm._m(1),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "col-md-7 col-sm-12" },
                                          [
                                            _c("span", [
                                              _vm._v(_vm._s(_vm.user.id))
                                            ])
                                          ]
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      {
                                        staticClass:
                                          "row user-profille-inner-row"
                                      },
                                      [
                                        _vm._m(2),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "col-md-7 col-sm-12" },
                                          [
                                            _vm.editingMode
                                              ? _c("input", {
                                                  directives: [
                                                    {
                                                      name: "model",
                                                      rawName: "v-model",
                                                      value: _vm.user.email,
                                                      expression: "user.email"
                                                    }
                                                  ],
                                                  staticClass:
                                                    "admin-chlng-fit-top-input",
                                                  attrs: {
                                                    type: "text",
                                                    readonly: ""
                                                  },
                                                  domProps: {
                                                    value: _vm.user.email
                                                  },
                                                  on: {
                                                    input: function($event) {
                                                      if (
                                                        $event.target.composing
                                                      ) {
                                                        return
                                                      }
                                                      _vm.$set(
                                                        _vm.user,
                                                        "email",
                                                        $event.target.value
                                                      )
                                                    }
                                                  }
                                                })
                                              : _c("span", [
                                                  _vm._v(_vm._s(_vm.user.email))
                                                ])
                                          ]
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      {
                                        staticClass:
                                          "row user-profille-inner-row"
                                      },
                                      [
                                        _vm._m(3),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "col-md-7 col-sm-12" },
                                          [
                                            _vm.editingMode
                                              ? _c("VueGooglePlaces", {
                                                  staticClass:
                                                    "admin-chlng-fit-top-input",
                                                  attrs: {
                                                    "api-key":
                                                      "AIzaSyAHPUufTlBkF5NfBT3uhS9K4BbW2N-mkb4",
                                                    enableGeolocation: true,
                                                    enableGeocode: true,
                                                    version: "3.36",
                                                    placeholder:
                                                      "Input your place"
                                                  },
                                                  on: {
                                                    placechanged:
                                                      _vm.onPlaceChanged,
                                                    noresult: _vm.onNoResult
                                                  }
                                                })
                                              : _c("span", [
                                                  _vm._v(
                                                    _vm._s(_vm.user.address)
                                                  )
                                                ])
                                          ],
                                          1
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      {
                                        staticClass:
                                          "row user-profille-inner-row"
                                      },
                                      [
                                        _vm._m(4),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "col-md-7 col-sm-12" },
                                          [
                                            _vm.editingMode
                                              ? _c("input", {
                                                  directives: [
                                                    {
                                                      name: "model",
                                                      rawName: "v-model",
                                                      value: _vm.user.country,
                                                      expression: "user.country"
                                                    }
                                                  ],
                                                  staticClass:
                                                    "admin-chlng-fit-top-input",
                                                  attrs: { type: "text" },
                                                  domProps: {
                                                    value: _vm.user.country
                                                  },
                                                  on: {
                                                    input: function($event) {
                                                      if (
                                                        $event.target.composing
                                                      ) {
                                                        return
                                                      }
                                                      _vm.$set(
                                                        _vm.user,
                                                        "country",
                                                        $event.target.value
                                                      )
                                                    }
                                                  }
                                                })
                                              : _c("span", [
                                                  _vm._v(
                                                    _vm._s(_vm.user.country)
                                                  )
                                                ])
                                          ]
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      {
                                        staticClass:
                                          "row user-profille-inner-row"
                                      },
                                      [
                                        _vm._m(5),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "col-md-7 col-sm-12" },
                                          [
                                            _vm.editingMode
                                              ? _c("input", {
                                                  directives: [
                                                    {
                                                      name: "model",
                                                      rawName: "v-model",
                                                      value:
                                                        _vm.user.address_state,
                                                      expression:
                                                        "user.address_state"
                                                    }
                                                  ],
                                                  staticClass:
                                                    "admin-chlng-fit-top-input",
                                                  attrs: { type: "text" },
                                                  domProps: {
                                                    value:
                                                      _vm.user.address_state
                                                  },
                                                  on: {
                                                    input: function($event) {
                                                      if (
                                                        $event.target.composing
                                                      ) {
                                                        return
                                                      }
                                                      _vm.$set(
                                                        _vm.user,
                                                        "address_state",
                                                        $event.target.value
                                                      )
                                                    }
                                                  }
                                                })
                                              : _c("span", [
                                                  _vm._v(
                                                    _vm._s(
                                                      _vm.user.address_state
                                                    )
                                                  )
                                                ])
                                          ]
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      {
                                        staticClass:
                                          "row user-profille-inner-row"
                                      },
                                      [
                                        _vm._m(6),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "col-md-7 col-sm-12" },
                                          [
                                            _vm.editingMode
                                              ? _c("input", {
                                                  directives: [
                                                    {
                                                      name: "model",
                                                      rawName: "v-model",
                                                      value: _vm.user.city,
                                                      expression: "user.city"
                                                    }
                                                  ],
                                                  staticClass:
                                                    "admin-chlng-fit-top-input",
                                                  attrs: { type: "text" },
                                                  domProps: {
                                                    value: _vm.user.city
                                                  },
                                                  on: {
                                                    input: function($event) {
                                                      if (
                                                        $event.target.composing
                                                      ) {
                                                        return
                                                      }
                                                      _vm.$set(
                                                        _vm.user,
                                                        "city",
                                                        $event.target.value
                                                      )
                                                    }
                                                  }
                                                })
                                              : _c("span", [
                                                  _vm._v(_vm._s(_vm.user.city))
                                                ])
                                          ]
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      {
                                        staticClass:
                                          "row user-profille-inner-row"
                                      },
                                      [
                                        _vm._m(7),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "col-md-7 col-sm-12" },
                                          [
                                            _vm.editingMode
                                              ? _c("input", {
                                                  directives: [
                                                    {
                                                      name: "model",
                                                      rawName: "v-model",
                                                      value:
                                                        _vm.user.postal_code,
                                                      expression:
                                                        "user.postal_code"
                                                    }
                                                  ],
                                                  staticClass:
                                                    "admin-chlng-fit-top-input",
                                                  attrs: { type: "text" },
                                                  domProps: {
                                                    value: _vm.user.postal_code
                                                  },
                                                  on: {
                                                    input: function($event) {
                                                      if (
                                                        $event.target.composing
                                                      ) {
                                                        return
                                                      }
                                                      _vm.$set(
                                                        _vm.user,
                                                        "postal_code",
                                                        $event.target.value
                                                      )
                                                    }
                                                  }
                                                })
                                              : _c("span", [
                                                  _vm._v(
                                                    _vm._s(_vm.user.postal_code)
                                                  )
                                                ])
                                          ]
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      {
                                        staticClass:
                                          "row user-profille-inner-row"
                                      },
                                      [
                                        _vm._m(8),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "col-md-7 col-sm-12" },
                                          [
                                            _vm.editingMode
                                              ? _c("the-mask", {
                                                  directives: [
                                                    {
                                                      name: "validate",
                                                      rawName: "v-validate",
                                                      value:
                                                        "required|numeric|digits:10",
                                                      expression:
                                                        "`required|numeric|digits:10`"
                                                    }
                                                  ],
                                                  staticClass:
                                                    "admin-chlng-fit-top-input",
                                                  attrs: {
                                                    mask: "+1 (###) ###-####",
                                                    name: "contact",
                                                    placeholder:
                                                      "+1 (xxx)-xxx-xxxx"
                                                  },
                                                  model: {
                                                    value: _vm.user.contact,
                                                    callback: function($$v) {
                                                      _vm.$set(
                                                        _vm.user,
                                                        "contact",
                                                        $$v
                                                      )
                                                    },
                                                    expression: "user.contact"
                                                  }
                                                })
                                              : _c("span", [
                                                  _vm._v(
                                                    _vm._s(_vm.user.contact)
                                                  )
                                                ]),
                                            _vm._v(" "),
                                            _c("span", [
                                              _vm._v(
                                                _vm._s(
                                                  _vm.errors.first("contact")
                                                )
                                              )
                                            ])
                                          ],
                                          1
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    !_vm.editingMode
                                      ? _c(
                                          "div",
                                          {
                                            staticClass:
                                              "row user-profille-inner-row"
                                          },
                                          [
                                            _vm._m(9),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              {
                                                staticClass:
                                                  "col-md-7 col-sm-12"
                                              },
                                              [
                                                _c("span", [
                                                  _vm._v(
                                                    _vm._s(_vm.user.created_at)
                                                  )
                                                ])
                                              ]
                                            )
                                          ]
                                        )
                                      : _vm._e(),
                                    _vm._v(" "),
                                    _vm.editingMode
                                      ? _c(
                                          "button",
                                          {
                                            staticClass: "cnt-btnn",
                                            attrs: { type: "submit" }
                                          },
                                          [_vm._v("Update")]
                                        )
                                      : _vm._e()
                                  ]
                                ),
                                _vm._v(" "),
                                _c("div", {
                                  staticClass: "col-xl-6 col-lg-6 col-md-12"
                                })
                              ]
                            )
                          : _vm._e()
                      ])
                    ])
                  ]
                )
              ])
            ])
          ])
        ])
      ])
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "alert" }, [
      _c("p", [_vm._v("No challenge found")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-5 col-sm-12" }, [
      _c("p", [
        _c("i", {
          staticClass: "fa fa-user-circle",
          attrs: { "aria-hidden": "true" }
        }),
        _vm._v(" User ID: ")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-5 col-sm-12" }, [
      _c("p", [
        _c("i", {
          staticClass: "fa fa-envelope",
          attrs: { "aria-hidden": "true" }
        }),
        _vm._v(" Email: ")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-5 col-sm-12" }, [
      _c("p", [
        _c("i", {
          staticClass: "fa fa-map-marker",
          attrs: { "aria-hidden": "true" }
        }),
        _vm._v(" Address: ")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-5 col-sm-12" }, [
      _c("p", [
        _c("i", {
          staticClass: "fa fa-map-marker",
          attrs: { "aria-hidden": "true" }
        }),
        _vm._v("Country: ")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-5 col-sm-12" }, [
      _c("p", [
        _c("i", {
          staticClass: "fa fa-map-marker",
          attrs: { "aria-hidden": "true" }
        }),
        _vm._v(" State: ")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-5 col-sm-12" }, [
      _c("p", [
        _c("i", {
          staticClass: "fa fa-map-marker",
          attrs: { "aria-hidden": "true" }
        }),
        _vm._v(" City: ")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-5 col-sm-12" }, [
      _c("p", [
        _c("i", {
          staticClass: "fa fa-map-marker",
          attrs: { "aria-hidden": "true" }
        }),
        _vm._v(" Postal Code: ")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-5 col-sm-12" }, [
      _c("p", [
        _c("i", {
          staticClass: "fa fa-phone",
          attrs: { "aria-hidden": "true" }
        }),
        _vm._v(" Phone: ")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-5 col-sm-12" }, [
      _c("p", [
        _c("i", {
          staticClass: "fa fa-user",
          attrs: { "aria-hidden": "true" }
        }),
        _vm._v(" Member Since: ")
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/compoments/TimerComponent.vue?vue&type=template&id=09a9d8f8&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/compoments/TimerComponent.vue?vue&type=template&id=09a9d8f8& ***!
  \*****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _vm.days > 0
        ? [
            _vm._v(
              "\n            " +
                _vm._s(_vm.days) +
                " " +
                _vm._s(_vm.wordString.day) +
                "\n        "
            )
          ]
        : _vm._e(),
      _vm._v(" "),
      _vm.hours > 0
        ? [
            _vm._v(
              "\n            " +
                _vm._s(_vm.hours) +
                " " +
                _vm._s(_vm.wordString.hours) +
                "\n        "
            )
          ]
        : _vm._e(),
      _vm._v(" "),
      _vm.minutes > 0
        ? [
            _vm._v(
              "\n            " +
                _vm._s(_vm.minutes) +
                " " +
                _vm._s(_vm.wordString.minutes) +
                "\n        "
            )
          ]
        : _vm._e(),
      _vm._v(" "),
      _vm.seconds > 0
        ? [
            _vm._v(
              "\n            " +
                _vm._s(_vm.seconds) +
                " " +
                _vm._s(_vm.wordString.seconds) +
                "\n        "
            )
          ]
        : _vm._e(),
      _vm._v(" "),
      _c("div", { staticClass: "message" }, [_vm._v(_vm._s(_vm.message))])
    ],
    2
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-the-mask/dist/vue-the-mask.js":
/*!********************************************************!*\
  !*** ./node_modules/vue-the-mask/dist/vue-the-mask.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

(function(e,t){ true?module.exports=t():undefined})(this,function(){return function(e){function t(r){if(n[r])return n[r].exports;var a=n[r]={i:r,l:!1,exports:{}};return e[r].call(a.exports,a,a.exports,t),a.l=!0,a.exports}var n={};return t.m=e,t.c=n,t.i=function(e){return e},t.d=function(e,n,r){t.o(e,n)||Object.defineProperty(e,n,{configurable:!1,enumerable:!0,get:r})},t.n=function(e){var n=e&&e.__esModule?function(){return e.default}:function(){return e};return t.d(n,"a",n),n},t.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},t.p=".",t(t.s=10)}([function(e,t){e.exports={"#":{pattern:/\d/},X:{pattern:/[0-9a-zA-Z]/},S:{pattern:/[a-zA-Z]/},A:{pattern:/[a-zA-Z]/,transform:function(e){return e.toLocaleUpperCase()}},a:{pattern:/[a-zA-Z]/,transform:function(e){return e.toLocaleLowerCase()}},"!":{escape:!0}}},function(e,t,n){"use strict";function r(e){var t=document.createEvent("Event");return t.initEvent(e,!0,!0),t}var a=n(2),o=n(0),i=n.n(o);t.a=function(e,t){var o=t.value;if((Array.isArray(o)||"string"==typeof o)&&(o={mask:o,tokens:i.a}),"INPUT"!==e.tagName.toLocaleUpperCase()){var u=e.getElementsByTagName("input");if(1!==u.length)throw new Error("v-mask directive requires 1 input, found "+u.length);e=u[0]}e.oninput=function(t){if(t.isTrusted){var i=e.selectionEnd,u=e.value[i-1];for(e.value=n.i(a.a)(e.value,o.mask,!0,o.tokens);i<e.value.length&&e.value.charAt(i-1)!==u;)i++;e===document.activeElement&&(e.setSelectionRange(i,i),setTimeout(function(){e.setSelectionRange(i,i)},0)),e.dispatchEvent(r("input"))}};var s=n.i(a.a)(e.value,o.mask,!0,o.tokens);s!==e.value&&(e.value=s,e.dispatchEvent(r("input")))}},function(e,t,n){"use strict";var r=n(6),a=n(5);t.a=function(e,t){var o=!(arguments.length>2&&void 0!==arguments[2])||arguments[2],i=arguments[3];return Array.isArray(t)?n.i(a.a)(r.a,t,i)(e,t,o,i):n.i(r.a)(e,t,o,i)}},function(e,t,n){"use strict";function r(e){e.component(s.a.name,s.a),e.directive("mask",i.a)}Object.defineProperty(t,"__esModule",{value:!0});var a=n(0),o=n.n(a),i=n(1),u=n(7),s=n.n(u);n.d(t,"TheMask",function(){return s.a}),n.d(t,"mask",function(){return i.a}),n.d(t,"tokens",function(){return o.a}),n.d(t,"version",function(){return c});var c="0.11.1";t.default=r,"undefined"!=typeof window&&window.Vue&&window.Vue.use(r)},function(e,t,n){"use strict";Object.defineProperty(t,"__esModule",{value:!0});var r=n(1),a=n(0),o=n.n(a),i=n(2);t.default={name:"TheMask",props:{value:[String,Number],mask:{type:[String,Array],required:!0},masked:{type:Boolean,default:!1},tokens:{type:Object,default:function(){return o.a}}},directives:{mask:r.a},data:function(){return{lastValue:null,display:this.value}},watch:{value:function(e){e!==this.lastValue&&(this.display=e)},masked:function(){this.refresh(this.display)}},computed:{config:function(){return{mask:this.mask,tokens:this.tokens,masked:this.masked}}},methods:{onInput:function(e){e.isTrusted||this.refresh(e.target.value)},refresh:function(e){this.display=e;var e=n.i(i.a)(e,this.mask,this.masked,this.tokens);e!==this.lastValue&&(this.lastValue=e,this.$emit("input",e))}}}},function(e,t,n){"use strict";function r(e,t,n){return t=t.sort(function(e,t){return e.length-t.length}),function(r,a){for(var o=!(arguments.length>2&&void 0!==arguments[2])||arguments[2],i=0;i<t.length;){var u=t[i];i++;var s=t[i];if(!(s&&e(r,s,!0,n).length>u.length))return e(r,u,o,n)}return""}}t.a=r},function(e,t,n){"use strict";function r(e,t){var n=!(arguments.length>2&&void 0!==arguments[2])||arguments[2],r=arguments[3];e=e||"",t=t||"";for(var a=0,o=0,i="";a<t.length&&o<e.length;){var u=t[a],s=r[u],c=e[o];s&&!s.escape?(s.pattern.test(c)&&(i+=s.transform?s.transform(c):c,a++),o++):(s&&s.escape&&(a++,u=t[a]),n&&(i+=u),c===u&&o++,a++)}for(var f="";a<t.length&&n;){var u=t[a];if(r[u]){f="";break}f+=u,a++}return i+f}t.a=r},function(e,t,n){var r=n(8)(n(4),n(9),null,null);e.exports=r.exports},function(e,t){e.exports=function(e,t,n,r){var a,o=e=e||{},i=typeof e.default;"object"!==i&&"function"!==i||(a=e,o=e.default);var u="function"==typeof o?o.options:o;if(t&&(u.render=t.render,u.staticRenderFns=t.staticRenderFns),n&&(u._scopeId=n),r){var s=u.computed||(u.computed={});Object.keys(r).forEach(function(e){var t=r[e];s[e]=function(){return t}})}return{esModule:a,exports:o,options:u}}},function(e,t){e.exports={render:function(){var e=this,t=e.$createElement;return(e._self._c||t)("input",{directives:[{name:"mask",rawName:"v-mask",value:e.config,expression:"config"}],attrs:{type:"text"},domProps:{value:e.display},on:{input:e.onInput}})},staticRenderFns:[]}},function(e,t,n){e.exports=n(3)}])});

/***/ }),

/***/ "./resources/js/admin/components/users/ShowComponent.vue":
/*!***************************************************************!*\
  !*** ./resources/js/admin/components/users/ShowComponent.vue ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ShowComponent_vue_vue_type_template_id_7033a65e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ShowComponent.vue?vue&type=template&id=7033a65e& */ "./resources/js/admin/components/users/ShowComponent.vue?vue&type=template&id=7033a65e&");
/* harmony import */ var _ShowComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ShowComponent.vue?vue&type=script&lang=js& */ "./resources/js/admin/components/users/ShowComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ShowComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ShowComponent_vue_vue_type_template_id_7033a65e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ShowComponent_vue_vue_type_template_id_7033a65e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/admin/components/users/ShowComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/admin/components/users/ShowComponent.vue?vue&type=script&lang=js&":
/*!****************************************************************************************!*\
  !*** ./resources/js/admin/components/users/ShowComponent.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ShowComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/users/ShowComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/admin/components/users/ShowComponent.vue?vue&type=template&id=7033a65e&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/admin/components/users/ShowComponent.vue?vue&type=template&id=7033a65e& ***!
  \**********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_template_id_7033a65e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ShowComponent.vue?vue&type=template&id=7033a65e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/users/ShowComponent.vue?vue&type=template&id=7033a65e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_template_id_7033a65e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_template_id_7033a65e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/compoments/TimerComponent.vue":
/*!****************************************************!*\
  !*** ./resources/js/compoments/TimerComponent.vue ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _TimerComponent_vue_vue_type_template_id_09a9d8f8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TimerComponent.vue?vue&type=template&id=09a9d8f8& */ "./resources/js/compoments/TimerComponent.vue?vue&type=template&id=09a9d8f8&");
/* harmony import */ var _TimerComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./TimerComponent.vue?vue&type=script&lang=js& */ "./resources/js/compoments/TimerComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _TimerComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _TimerComponent_vue_vue_type_template_id_09a9d8f8___WEBPACK_IMPORTED_MODULE_0__["render"],
  _TimerComponent_vue_vue_type_template_id_09a9d8f8___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/compoments/TimerComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/compoments/TimerComponent.vue?vue&type=script&lang=js&":
/*!*****************************************************************************!*\
  !*** ./resources/js/compoments/TimerComponent.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TimerComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./TimerComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/compoments/TimerComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TimerComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/compoments/TimerComponent.vue?vue&type=template&id=09a9d8f8&":
/*!***********************************************************************************!*\
  !*** ./resources/js/compoments/TimerComponent.vue?vue&type=template&id=09a9d8f8& ***!
  \***********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TimerComponent_vue_vue_type_template_id_09a9d8f8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./TimerComponent.vue?vue&type=template&id=09a9d8f8& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/compoments/TimerComponent.vue?vue&type=template&id=09a9d8f8&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TimerComponent_vue_vue_type_template_id_09a9d8f8___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TimerComponent_vue_vue_type_template_id_09a9d8f8___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);