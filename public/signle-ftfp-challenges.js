(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["signle-ftfp-challenges"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/compoments/challenges/ShowComponent.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/admin/compoments/challenges/ShowComponent.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _pooling_requests_ShowComponent__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../pooling-requests/ShowComponent */ "./resources/js/admin/compoments/pooling-requests/ShowComponent.vue");
/* harmony import */ var vue_owl_carousel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-owl-carousel */ "./node_modules/vue-owl-carousel/dist/vue-owl-carousel.js");
/* harmony import */ var vue_owl_carousel__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue_owl_carousel__WEBPACK_IMPORTED_MODULE_1__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    PoolingShow: _pooling_requests_ShowComponent__WEBPACK_IMPORTED_MODULE_0__["default"],
    carousel: vue_owl_carousel__WEBPACK_IMPORTED_MODULE_1___default.a
  },
  data: function data() {
    return {
      base_url: window.base_url,
      statuses: window.statuses,
      isLoading: true,
      challenge: {},
      user: null,
      userId: null,
      challengeRequest: null
    };
  },
  mounted: function mounted() {
    this.userId = this.$route.params.userId;
    this.getChallenge(this.$route.params.id);
    if (this.userId) this.getUser();
  },
  methods: {
    // get user if this is user challenge detail page
    getUser: function getUser() {
      var _this = this;

      this.isLoading = true;
      axios.get("/users/".concat(this.$route.params.userId)).then(function (_ref) {
        var data = _ref.data;
        _this.user = data;
        _this.isLoading = false;
      });
    },
    getChallenge: function getChallenge(id) {
      var _this2 = this;

      this.isLoading = true;
      var shouldReturnFromRequest = this.userId ? '?challengeRequest=yes' : '';
      axios.get("/challenges/".concat(id).concat(shouldReturnFromRequest)).then(function (data) {
        _this2.challenge = data.data;
        _this2.isLoading = false;
      });
      /*axios.get(`/challenges/${id}`)
          .then(data => {
              this.challenge = data.data;
              this.isLoading = false;
          });*/

      if (this.userId) {
        axios.get("/pooling-requests/".concat(this.$route.params.id)).then(function (data) {
          _this2.challengeRequest = data.data;
          _this2.challengeRequest.challenge = JSON.parse(data.data.challenge);
        });
      }
    },
    deleteExercise: function deleteExercise(index) {
      var _this3 = this;

      this.$dialog.confirm('Do you want to delete this exercise.?', {
        okText: 'Proceed'
      }).then(function (dialog) {
        _this3.isLoading = true;
        axios["delete"]("/challenges/".concat(_this3.challenge.id, "/exercises/").concat(_this3.challenge.exercises[index].id)).then(function (data) {
          _this3.isLoading = false;

          _this3.challenge.exercises.splice(index, 1);

          _this3.$toastr.success('User status has changed', 'Success', {});

          dialog.close();
        })["catch"](function (e) {
          _this3.isLoading = false;
          dialog.close();

          _this3.$toastr.error(e.response.statusText, 'Error');
        });
      });
    },
    changeTab: function changeTab(tab) {
      this.$router.push({
        name: 'users.show',
        params: {
          id: this.user.id,
          tab: tab
        }
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/compoments/challenges/ShowComponent.vue?vue&type=style&index=0&lang=css&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/admin/compoments/challenges/ShowComponent.vue?vue&type=style&index=0&lang=css& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.owl-carousel .owl-item img {\n    width: 172px;\n}\n.owl-theme .owl-dots .owl-dot span {\n    background: #ee2c48;\n    width: 13px;\n    height: 13px;\n    margin: 4px;\n}\n.owl-theme .owl-dots .owl-dot.active span, .owl-theme .owl-dots .owl-dot:hover span {\n    background: #ee2c48;\n    opacity: 0.5;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/compoments/challenges/ShowComponent.vue?vue&type=style&index=0&lang=css&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/admin/compoments/challenges/ShowComponent.vue?vue&type=style&index=0&lang=css& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader??ref--7-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ShowComponent.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/compoments/challenges/ShowComponent.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/compoments/challenges/ShowComponent.vue?vue&type=template&id=bed95926&":
/*!*********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/admin/compoments/challenges/ShowComponent.vue?vue&type=template&id=bed95926& ***!
  \*********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "app-content content" },
    [
      _vm.isLoading
        ? _c("BlockUI", { attrs: { message: "Loading..." } }, [
            _c("i", { staticClass: "fa fa-cog fa-spin fa-3x fa-fw" })
          ])
        : _vm._e(),
      _vm._v(" "),
      _c("div", { staticClass: "content-wrapper" }, [
        _c("div", { staticClass: "content-body" }, [
          !_vm.isLoading
            ? _c("section", { attrs: { id: "combination-charts" } }, [
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-12" }, [
                    _c(
                      "div",
                      { staticClass: "card rounded p-3 admin-overview-main" },
                      [
                        _c("div", { staticClass: "row" }, [
                          _c(
                            "div",
                            { staticClass: "col-lg-12" },
                            [
                              _c(
                                "h1",
                                [
                                  _c(
                                    "router-link",
                                    {
                                      staticClass: "profile-back",
                                      attrs: {
                                        to: { name: "challenges.index" }
                                      }
                                    },
                                    [
                                      _c("i", {
                                        staticClass: "fa fa-angle-left",
                                        attrs: { "aria-hidden": "true" }
                                      })
                                    ]
                                  ),
                                  _vm._v(
                                    " Challenges Details\n                                    "
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              !_vm.user
                                ? _c("div", { staticClass: "row" }, [
                                    _c(
                                      "div",
                                      { staticClass: "col-md-6 col-sm-12" },
                                      [
                                        _c(
                                          "div",
                                          {
                                            staticClass:
                                              "admin-top-red-nav mt-0"
                                          },
                                          [
                                            _c("ul", [
                                              _c(
                                                "li",
                                                {
                                                  class: {
                                                    active:
                                                      _vm.challenge.path ===
                                                      "East"
                                                  }
                                                },
                                                [
                                                  _c(
                                                    "a",
                                                    {
                                                      attrs: { href: "#" },
                                                      on: {
                                                        click: function(
                                                          $event
                                                        ) {
                                                          $event.preventDefault()
                                                        }
                                                      }
                                                    },
                                                    [_vm._v("East Path")]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "li",
                                                {
                                                  class: {
                                                    active:
                                                      _vm.challenge.path ===
                                                      "West"
                                                  }
                                                },
                                                [
                                                  _c(
                                                    "a",
                                                    {
                                                      attrs: { href: "#" },
                                                      on: {
                                                        click: function(
                                                          $event
                                                        ) {
                                                          $event.preventDefault()
                                                        }
                                                      }
                                                    },
                                                    [_vm._v("West Path ")]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "li",
                                                {
                                                  class: {
                                                    active:
                                                      _vm.challenge.path ===
                                                      "North"
                                                  }
                                                },
                                                [
                                                  _c(
                                                    "a",
                                                    {
                                                      attrs: { href: "#" },
                                                      on: {
                                                        click: function(
                                                          $event
                                                        ) {
                                                          $event.preventDefault()
                                                        }
                                                      }
                                                    },
                                                    [_vm._v("North Path ")]
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "li",
                                                {
                                                  class: {
                                                    active:
                                                      _vm.challenge.path ===
                                                      "South"
                                                  }
                                                },
                                                [
                                                  _c(
                                                    "a",
                                                    {
                                                      attrs: { href: "#" },
                                                      on: {
                                                        click: function(
                                                          $event
                                                        ) {
                                                          $event.preventDefault()
                                                        }
                                                      }
                                                    },
                                                    [_vm._v("South Path ")]
                                                  )
                                                ]
                                              )
                                            ])
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c("div", { staticClass: "clearfix" })
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      { staticClass: "col-md-6 col-sm-12" },
                                      [
                                        _c(
                                          "router-link",
                                          {
                                            staticClass: "green-btn-project",
                                            attrs: {
                                              to: { name: "challenges.create" }
                                            }
                                          },
                                          [
                                            _c("i", {
                                              staticClass: "fa fa-plus-circle",
                                              attrs: { "aria-hidden": "true" }
                                            }),
                                            _vm._v(
                                              " Add Challenge\n                                            "
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "router-link",
                                          {
                                            staticClass: "green-btn-project2",
                                            attrs: {
                                              to: { name: "fit_to_path.create" }
                                            }
                                          },
                                          [
                                            _vm._v(
                                              "Set level & Leverage points"
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "router-link",
                                          {
                                            staticClass: "fit-setting-a",
                                            attrs: { to: { name: "settings" } }
                                          },
                                          [
                                            _c("i", {
                                              staticClass: "fa fa-cog",
                                              attrs: { "aria-hidden": "true" }
                                            })
                                          ]
                                        )
                                      ],
                                      1
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      { staticClass: "admin-top-blue-nav" },
                                      [
                                        _c("ul", [
                                          _c(
                                            "li",
                                            {
                                              class: {
                                                active:
                                                  _vm.challenge.level ===
                                                  "Level 1"
                                              }
                                            },
                                            [
                                              _c(
                                                "a",
                                                {
                                                  attrs: { href: "#" },
                                                  on: {
                                                    click: function($event) {
                                                      $event.preventDefault()
                                                    }
                                                  }
                                                },
                                                [_vm._v("Level 1")]
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "li",
                                            {
                                              class: {
                                                active:
                                                  _vm.challenge.level ===
                                                  "Level 2"
                                              }
                                            },
                                            [
                                              _c(
                                                "a",
                                                {
                                                  attrs: { href: "#" },
                                                  on: {
                                                    click: function($event) {
                                                      $event.preventDefault()
                                                    }
                                                  }
                                                },
                                                [_vm._v("Level 2")]
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "li",
                                            {
                                              class: {
                                                active:
                                                  _vm.challenge.level ===
                                                  "Level 3"
                                              }
                                            },
                                            [
                                              _c(
                                                "a",
                                                {
                                                  attrs: { href: "#" },
                                                  on: {
                                                    click: function($event) {
                                                      $event.preventDefault()
                                                    }
                                                  }
                                                },
                                                [_vm._v("Level 3")]
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "li",
                                            {
                                              class: {
                                                active:
                                                  _vm.challenge.level ===
                                                  "Level 4"
                                              }
                                            },
                                            [
                                              _c(
                                                "a",
                                                {
                                                  attrs: { href: "#" },
                                                  on: {
                                                    click: function($event) {
                                                      $event.preventDefault()
                                                    }
                                                  }
                                                },
                                                [_vm._v("Level 4")]
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "li",
                                            {
                                              class: {
                                                active:
                                                  _vm.challenge.level ===
                                                  "Level 5"
                                              }
                                            },
                                            [
                                              _c(
                                                "a",
                                                {
                                                  attrs: { href: "#" },
                                                  on: {
                                                    click: function($event) {
                                                      $event.preventDefault()
                                                    }
                                                  }
                                                },
                                                [_vm._v("Level 5")]
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "li",
                                            {
                                              class: {
                                                active:
                                                  _vm.challenge.level ===
                                                  "Level 6"
                                              }
                                            },
                                            [
                                              _c(
                                                "a",
                                                {
                                                  attrs: { href: "#" },
                                                  on: {
                                                    click: function($event) {
                                                      $event.preventDefault()
                                                    }
                                                  }
                                                },
                                                [_vm._v("Level 6")]
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "li",
                                            {
                                              class: {
                                                active:
                                                  _vm.challenge.level ===
                                                  "Level 7"
                                              }
                                            },
                                            [
                                              _c(
                                                "a",
                                                {
                                                  attrs: { href: "#" },
                                                  on: {
                                                    click: function($event) {
                                                      $event.preventDefault()
                                                    }
                                                  }
                                                },
                                                [_vm._v("Level 7")]
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "li",
                                            {
                                              class: {
                                                active:
                                                  _vm.challenge.level ===
                                                  "Level 8"
                                              }
                                            },
                                            [
                                              _c(
                                                "a",
                                                {
                                                  attrs: { href: "#" },
                                                  on: {
                                                    click: function($event) {
                                                      $event.preventDefault()
                                                    }
                                                  }
                                                },
                                                [_vm._v("Level 8")]
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "li",
                                            {
                                              class: {
                                                active:
                                                  _vm.challenge.level ===
                                                  "Level 9"
                                              }
                                            },
                                            [
                                              _c(
                                                "a",
                                                {
                                                  attrs: { href: "#" },
                                                  on: {
                                                    click: function($event) {
                                                      $event.preventDefault()
                                                    }
                                                  }
                                                },
                                                [_vm._v("Level 9")]
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "li",
                                            {
                                              class: {
                                                active:
                                                  _vm.challenge.level ===
                                                  "Level 10"
                                              }
                                            },
                                            [
                                              _c(
                                                "a",
                                                {
                                                  attrs: { href: "#" },
                                                  on: {
                                                    click: function($event) {
                                                      $event.preventDefault()
                                                    }
                                                  }
                                                },
                                                [_vm._v("Level 10")]
                                              )
                                            ]
                                          )
                                        ])
                                      ]
                                    )
                                  ])
                                : _vm._e(),
                              _vm._v(" "),
                              _vm.user
                                ? _c(
                                    "div",
                                    { staticClass: "admin-user-profile-top" },
                                    [
                                      _c(
                                        "div",
                                        {
                                          staticClass: "row align-items-center"
                                        },
                                        [
                                          _c(
                                            "div",
                                            {
                                              staticClass: "col-md-3 col-sm-12"
                                            },
                                            [
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "admin-user-profile-top-image-main"
                                                },
                                                [
                                                  _c("img", {
                                                    attrs: {
                                                      src: _vm.challenge.image,
                                                      alt: ""
                                                    }
                                                  }),
                                                  _vm._v(" "),
                                                  _c("h6", [
                                                    _vm._v(
                                                      _vm._s(_vm.user.name)
                                                    )
                                                  ]),
                                                  _vm._v(" "),
                                                  _c("span", [
                                                    _vm._v(
                                                      _vm._s(_vm.user.email)
                                                    )
                                                  ])
                                                ]
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "div",
                                            {
                                              staticClass: "col-md-7 col-sm-12"
                                            },
                                            [
                                              _c(
                                                "div",
                                                { staticClass: "row" },
                                                [
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass:
                                                        "col-md-3 col-sm-12"
                                                    },
                                                    [
                                                      _c(
                                                        "div",
                                                        {
                                                          staticClass:
                                                            "admin-user-profile-top-boxs"
                                                        },
                                                        [
                                                          _c("span", [
                                                            _vm._v("Level")
                                                          ]),
                                                          _vm._v(" "),
                                                          _c("h5", [
                                                            _vm._v(
                                                              _vm._s(
                                                                _vm.challenge.level.replace(
                                                                  "Level ",
                                                                  ""
                                                                )
                                                              )
                                                            )
                                                          ])
                                                        ]
                                                      )
                                                    ]
                                                  ),
                                                  _vm._v(" "),
                                                  _vm._m(0),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass:
                                                        "col-md-3 col-sm-12"
                                                    },
                                                    [
                                                      _c(
                                                        "div",
                                                        {
                                                          staticClass:
                                                            "admin-user-profile-top-boxs"
                                                        },
                                                        [
                                                          _c("span", [
                                                            _vm._v(
                                                              "Total Points:"
                                                            )
                                                          ]),
                                                          _vm._v(" "),
                                                          _c("h5", [
                                                            _vm._v(
                                                              _vm._s(
                                                                _vm.challenge
                                                                  .reward_points
                                                              )
                                                            )
                                                          ])
                                                        ]
                                                      )
                                                    ]
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass:
                                                        "col-md-3 col-sm-12"
                                                    },
                                                    [
                                                      _c(
                                                        "div",
                                                        {
                                                          staticClass:
                                                            "admin-user-profile-top-boxs"
                                                        },
                                                        [
                                                          _c("span", [
                                                            _vm._v("Votes:")
                                                          ]),
                                                          _vm._v(" "),
                                                          _c("h5", [
                                                            _vm._v(
                                                              _vm._s(
                                                                _vm
                                                                  .challengeRequest
                                                                  .votes.length
                                                              )
                                                            )
                                                          ])
                                                        ]
                                                      )
                                                    ]
                                                  )
                                                ]
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "div",
                                            {
                                              staticClass: "col-md-2 col-sm-12"
                                            },
                                            [
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "admin-user-profile-top-boxs-logo"
                                                },
                                                [
                                                  _c("img", {
                                                    attrs: {
                                                      src:
                                                        _vm.base_url +
                                                        "/images/User-Profile_03.png",
                                                      alt: ""
                                                    }
                                                  }),
                                                  _vm._v(" "),
                                                  _c("h5", {
                                                    domProps: {
                                                      textContent: _vm._s(
                                                        _vm.challenge.path +
                                                          " Path"
                                                      )
                                                    }
                                                  })
                                                ]
                                              )
                                            ]
                                          )
                                        ]
                                      )
                                    ]
                                  )
                                : _vm._e(),
                              _vm._v(" "),
                              _c("div", { staticClass: "admin-top-red-nav" }, [
                                _c("ul", [
                                  _c("li", [
                                    _c(
                                      "a",
                                      {
                                        attrs: { href: "#" },
                                        on: {
                                          click: function($event) {
                                            $event.preventDefault()
                                            return _vm.changeTab("details")
                                          }
                                        }
                                      },
                                      [_vm._v("Details")]
                                    )
                                  ]),
                                  _vm._v(" "),
                                  _c("li", { staticClass: "active" }, [
                                    _c(
                                      "a",
                                      {
                                        attrs: { href: "#" },
                                        on: {
                                          click: function($event) {
                                            $event.preventDefault()
                                            return _vm.changeTab("challenge")
                                          }
                                        }
                                      },
                                      [_vm._v("Challenges")]
                                    )
                                  ]),
                                  _vm._v(" "),
                                  _c("li", [
                                    _c(
                                      "a",
                                      {
                                        attrs: { href: "#" },
                                        on: {
                                          click: function($event) {
                                            $event.preventDefault()
                                            return _vm.changeTab("friends")
                                          }
                                        }
                                      },
                                      [_vm._v("Friends")]
                                    )
                                  ])
                                ]),
                                _vm._v(" "),
                                !_vm.editingMode
                                  ? _c(
                                      "a",
                                      {
                                        staticClass: "closed-span float-right",
                                        attrs: { href: "#" },
                                        on: {
                                          click: function($event) {
                                            $event.preventDefault()
                                            _vm.editingMode = true
                                          }
                                        }
                                      },
                                      [_vm._v("Edit")]
                                    )
                                  : _vm._e()
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "clearfix" }),
                              _vm._v(" "),
                              _c("h1", [_vm._v("Challenge Detial")]),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "row chlng-detail-top" },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass:
                                        "col-lg-2 col-md-12 col-sm-12"
                                    },
                                    [
                                      _c("img", {
                                        staticClass: "img-full",
                                        attrs: {
                                          src: _vm.challenge.image,
                                          alt: ""
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "div",
                                    {
                                      staticClass:
                                        "col-lg-9 col-md-12 col-sm-12"
                                    },
                                    [
                                      _c(
                                        "div",
                                        {
                                          staticClass:
                                            "d-flex justify-content-between pt-1"
                                        },
                                        [
                                          _c("h2", [
                                            _vm._v(
                                              _vm._s(_vm.challenge.title) + " "
                                            ),
                                            _c("span", [
                                              _vm._v(
                                                "Posted: " +
                                                  _vm._s(
                                                    _vm.challenge.created_at.substr(
                                                      0,
                                                      10
                                                    )
                                                  )
                                              )
                                            ])
                                          ]),
                                          _vm._v(" "),
                                          !_vm.user
                                            ? _c(
                                                "router-link",
                                                {
                                                  staticClass: "closed-span",
                                                  attrs: {
                                                    to: {
                                                      name: "challenges.edit",
                                                      params: {
                                                        id: _vm.challenge.id
                                                      }
                                                    }
                                                  }
                                                },
                                                [
                                                  _c("span", {
                                                    staticClass: "fa fa-edit"
                                                  }),
                                                  _vm._v(
                                                    " Edit\n                                                "
                                                  )
                                                ]
                                              )
                                            : _vm._e(),
                                          _vm._v(" "),
                                          _vm.challengeRequest
                                            ? _c(
                                                "span",
                                                { staticClass: "closed-span" },
                                                [
                                                  _vm._v(
                                                    _vm._s(
                                                      _vm.statuses[
                                                        _vm.challengeRequest
                                                          .status
                                                      ]
                                                    )
                                                  )
                                                ]
                                              )
                                            : _vm._e()
                                        ],
                                        1
                                      ),
                                      _vm._v(" "),
                                      _vm.user
                                        ? _c("p", {
                                            domProps: {
                                              textContent: _vm._s(
                                                _vm.challenge.description
                                              )
                                            }
                                          })
                                        : _vm._e(),
                                      _vm._v(" "),
                                      !_vm.user
                                        ? _c("div", { staticClass: "row" }, [
                                            _c(
                                              "div",
                                              { staticClass: "col-md-2" },
                                              [
                                                _c(
                                                  "div",
                                                  {
                                                    staticClass:
                                                      "chalng-members-box box-shadow-1"
                                                  },
                                                  [
                                                    _c(
                                                      "div",
                                                      {
                                                        staticClass: "flex-row"
                                                      },
                                                      [
                                                        _c("p", [
                                                          _vm._v(
                                                            "Total Exercises"
                                                          )
                                                        ]),
                                                        _vm._v(" "),
                                                        _c("span", [
                                                          _vm._v(
                                                            _vm._s(
                                                              _vm.challenge
                                                                .exercises
                                                                .length
                                                            )
                                                          )
                                                        ])
                                                      ]
                                                    )
                                                  ]
                                                )
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              { staticClass: "col-md-2" },
                                              [
                                                _c(
                                                  "div",
                                                  {
                                                    staticClass:
                                                      "chalng-members-box box-shadow-1"
                                                  },
                                                  [
                                                    _c(
                                                      "div",
                                                      {
                                                        staticClass: "flex-row"
                                                      },
                                                      [
                                                        _c("p", [
                                                          _vm._v(
                                                            "Reward Points"
                                                          )
                                                        ]),
                                                        _vm._v(" "),
                                                        _c("span", [
                                                          _vm._v(
                                                            _vm._s(
                                                              _vm.challenge
                                                                .reward_points
                                                            )
                                                          )
                                                        ])
                                                      ]
                                                    )
                                                  ]
                                                )
                                              ]
                                            )
                                          ])
                                        : _vm._e()
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("div", {
                                    staticClass: "col-lg-1 col-md-12 col-sm-12"
                                  })
                                ]
                              ),
                              _vm._v(" "),
                              _vm.user
                                ? _c(
                                    "div",
                                    { staticClass: "row mt-2" },
                                    [
                                      _c(
                                        "carousel",
                                        {
                                          staticStyle: {
                                            overflow: "hidden",
                                            width: "100%"
                                          },
                                          attrs: { items: 2, nav: false }
                                        },
                                        _vm._l(
                                          _vm.user
                                            ? _vm.challengeRequest.challenge
                                                .exercises
                                            : _vm.challenge.exercises,
                                          function(exercise, index) {
                                            return _c(
                                              "div",
                                              {
                                                key: exercise.id,
                                                staticClass: "col"
                                              },
                                              [
                                                _c(
                                                  "div",
                                                  {
                                                    staticClass:
                                                      "chalng-details-box"
                                                  },
                                                  [
                                                    _c("img", {
                                                      attrs: {
                                                        src: exercise.image,
                                                        alt: exercise.title
                                                      }
                                                    }),
                                                    _vm._v(" "),
                                                    _c(
                                                      "div",
                                                      {
                                                        staticClass:
                                                          "flex-column"
                                                      },
                                                      [
                                                        _c("h5", {
                                                          domProps: {
                                                            textContent: _vm._s(
                                                              exercise.title
                                                            )
                                                          }
                                                        }),
                                                        _vm._v(" "),
                                                        _c("p", {
                                                          domProps: {
                                                            textContent: _vm._s(
                                                              exercise.description
                                                            )
                                                          }
                                                        }),
                                                        _vm._v(" "),
                                                        _c(
                                                          "div",
                                                          {
                                                            staticClass:
                                                              "d-flex flex-row justify-content-between"
                                                          },
                                                          [
                                                            _c("h6", [
                                                              _vm._v(
                                                                "Sets: " +
                                                                  _vm._s(
                                                                    exercise.sets
                                                                  )
                                                              )
                                                            ]),
                                                            _vm._v(" "),
                                                            !_vm.user
                                                              ? _c(
                                                                  "div",
                                                                  {
                                                                    staticClass:
                                                                      "close-challenges-new-bottom-boxs-right fftp-card-icon-a"
                                                                  },
                                                                  [
                                                                    _c(
                                                                      "a",
                                                                      {
                                                                        on: {
                                                                          click: function(
                                                                            $event
                                                                          ) {
                                                                            return _vm.deleteExercise(
                                                                              index
                                                                            )
                                                                          }
                                                                        }
                                                                      },
                                                                      [
                                                                        _c(
                                                                          "i",
                                                                          {
                                                                            staticClass:
                                                                              "fa fa-trash",
                                                                            attrs: {
                                                                              "aria-hidden":
                                                                                "true"
                                                                            }
                                                                          }
                                                                        )
                                                                      ]
                                                                    ),
                                                                    _vm._v(" "),
                                                                    _c(
                                                                      "router-link",
                                                                      {
                                                                        attrs: {
                                                                          to: {
                                                                            name:
                                                                              "challenges.exercise.edit",
                                                                            params: {
                                                                              id:
                                                                                _vm
                                                                                  .challenge
                                                                                  .id,
                                                                              exerciseId:
                                                                                exercise.id
                                                                            }
                                                                          }
                                                                        }
                                                                      },
                                                                      [
                                                                        _c(
                                                                          "i",
                                                                          {
                                                                            staticClass:
                                                                              "fa fa-pencil-square-o",
                                                                            attrs: {
                                                                              "aria-hidden":
                                                                                "true"
                                                                            }
                                                                          }
                                                                        )
                                                                      ]
                                                                    )
                                                                  ],
                                                                  1
                                                                )
                                                              : _vm._e()
                                                          ]
                                                        )
                                                      ]
                                                    )
                                                  ]
                                                )
                                              ]
                                            )
                                          }
                                        ),
                                        0
                                      )
                                    ],
                                    1
                                  )
                                : _c(
                                    "div",
                                    { staticClass: "row mt-2" },
                                    _vm._l(
                                      _vm.user
                                        ? _vm.challengeRequest.challenge
                                            .exercises
                                        : _vm.challenge.exercises,
                                      function(exercise, index) {
                                        return _c(
                                          "div",
                                          {
                                            key: exercise.id,
                                            staticClass:
                                              "col-lg-6 col-md-12 col-sm-12 col-xs-12"
                                          },
                                          [
                                            _c(
                                              "div",
                                              {
                                                staticClass:
                                                  "chalng-details-box"
                                              },
                                              [
                                                _c("img", {
                                                  attrs: {
                                                    src: exercise.image,
                                                    alt: exercise.title
                                                  }
                                                }),
                                                _vm._v(" "),
                                                _c(
                                                  "div",
                                                  {
                                                    staticClass: "flex-column"
                                                  },
                                                  [
                                                    _c("h5", {
                                                      domProps: {
                                                        textContent: _vm._s(
                                                          exercise.title
                                                        )
                                                      }
                                                    }),
                                                    _vm._v(" "),
                                                    _c("p", {
                                                      domProps: {
                                                        textContent: _vm._s(
                                                          exercise.description
                                                        )
                                                      }
                                                    }),
                                                    _vm._v(" "),
                                                    _c(
                                                      "div",
                                                      {
                                                        staticClass:
                                                          "d-flex flex-row justify-content-between"
                                                      },
                                                      [
                                                        _c("h6", [
                                                          _vm._v(
                                                            "Sets: " +
                                                              _vm._s(
                                                                exercise.sets
                                                              )
                                                          )
                                                        ]),
                                                        _vm._v(" "),
                                                        !_vm.user
                                                          ? _c(
                                                              "div",
                                                              {
                                                                staticClass:
                                                                  "close-challenges-new-bottom-boxs-right fftp-card-icon-a"
                                                              },
                                                              [
                                                                _c(
                                                                  "a",
                                                                  {
                                                                    on: {
                                                                      click: function(
                                                                        $event
                                                                      ) {
                                                                        return _vm.deleteExercise(
                                                                          index
                                                                        )
                                                                      }
                                                                    }
                                                                  },
                                                                  [
                                                                    _c("i", {
                                                                      staticClass:
                                                                        "fa fa-trash",
                                                                      attrs: {
                                                                        "aria-hidden":
                                                                          "true"
                                                                      }
                                                                    })
                                                                  ]
                                                                ),
                                                                _vm._v(" "),
                                                                _c(
                                                                  "router-link",
                                                                  {
                                                                    attrs: {
                                                                      to: {
                                                                        name:
                                                                          "challenges.exercise.edit",
                                                                        params: {
                                                                          id:
                                                                            _vm
                                                                              .challenge
                                                                              .id,
                                                                          exerciseId:
                                                                            exercise.id
                                                                        }
                                                                      }
                                                                    }
                                                                  },
                                                                  [
                                                                    _c("i", {
                                                                      staticClass:
                                                                        "fa fa-pencil-square-o",
                                                                      attrs: {
                                                                        "aria-hidden":
                                                                          "true"
                                                                      }
                                                                    })
                                                                  ]
                                                                )
                                                              ],
                                                              1
                                                            )
                                                          : _vm._e()
                                                      ]
                                                    )
                                                  ]
                                                )
                                              ]
                                            )
                                          ]
                                        )
                                      }
                                    ),
                                    0
                                  ),
                              _vm._v(" "),
                              _vm.user
                                ? _c("pooling-show", {
                                    attrs: {
                                      "challenge-data": _vm.challengeRequest
                                    }
                                  })
                                : _vm._e()
                            ],
                            1
                          )
                        ])
                      ]
                    )
                  ])
                ])
              ])
            : _vm._e()
        ])
      ])
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-3 col-sm-12" }, [
      _c("div", { staticClass: "admin-user-profile-top-boxs" }, [
        _c("span", [_vm._v("Challenges")]),
        _vm._v(" "),
        _c("h5", [_vm._v("09")])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/admin/compoments/challenges/ShowComponent.vue":
/*!********************************************************************!*\
  !*** ./resources/js/admin/compoments/challenges/ShowComponent.vue ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ShowComponent_vue_vue_type_template_id_bed95926___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ShowComponent.vue?vue&type=template&id=bed95926& */ "./resources/js/admin/compoments/challenges/ShowComponent.vue?vue&type=template&id=bed95926&");
/* harmony import */ var _ShowComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ShowComponent.vue?vue&type=script&lang=js& */ "./resources/js/admin/compoments/challenges/ShowComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _ShowComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ShowComponent.vue?vue&type=style&index=0&lang=css& */ "./resources/js/admin/compoments/challenges/ShowComponent.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _ShowComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ShowComponent_vue_vue_type_template_id_bed95926___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ShowComponent_vue_vue_type_template_id_bed95926___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/admin/compoments/challenges/ShowComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/admin/compoments/challenges/ShowComponent.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/admin/compoments/challenges/ShowComponent.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ShowComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/compoments/challenges/ShowComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/admin/compoments/challenges/ShowComponent.vue?vue&type=style&index=0&lang=css&":
/*!*****************************************************************************************************!*\
  !*** ./resources/js/admin/compoments/challenges/ShowComponent.vue?vue&type=style&index=0&lang=css& ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader??ref--7-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ShowComponent.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/compoments/challenges/ShowComponent.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/admin/compoments/challenges/ShowComponent.vue?vue&type=template&id=bed95926&":
/*!***************************************************************************************************!*\
  !*** ./resources/js/admin/compoments/challenges/ShowComponent.vue?vue&type=template&id=bed95926& ***!
  \***************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_template_id_bed95926___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ShowComponent.vue?vue&type=template&id=bed95926& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/compoments/challenges/ShowComponent.vue?vue&type=template&id=bed95926&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_template_id_bed95926___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowComponent_vue_vue_type_template_id_bed95926___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);