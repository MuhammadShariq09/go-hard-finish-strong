(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["index-trivia-challenge"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/trivia-challenges/Index.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/admin/components/trivia-challenges/Index.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      base_url: window.base_url,
      isLoading: true,
      challenges: {},
      per_page: 10,
      blockedOnly: false,
      search_term: '',
      sortKey: 'created_at',
      freeTrivia: true
    };
  },
  mounted: function mounted() {
    this.freeTrivia = this.$route.name.indexOf('cost') >= 0;
    this.loadChallenges(1);
  },
  methods: {
    loadChallenges: function loadChallenges() {
      var _this = this;

      var page = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
      axios.get("/trivia/challenges?paid=".concat(this.freeTrivia ? 0 : 1, "&status=").concat(Number(!this.blockedOnly), "&page=").concat(page, "&per_page=").concat(this.per_page, "&search_term=").concat(this.search_term)).then(function (data) {
        _this.challenges = data.data;
        _this.isLoading = false;
      });
    },
    blockUser: function blockUser(index) {
      var _this2 = this;

      this.$dialog.confirm('Do you want to delete this challenge.?', {
        okText: 'Proceed'
      }).then(function (dialog) {
        var challenge = _this2.challenges.data[index];
        _this2.isLoading = true;
        axios.put("/challenges/".concat(challenge.id, "/status")).then(function (_ref) {
          var challenge = _ref.challenge;
          _this2.isLoading = false;

          _this2.challenges.data.splice(index, 1);

          _this2.$toastr.success('User status has changed', 'Success', {});

          dialog.close();
        })["catch"](function (e) {
          _this2.$toastr.error(e.response.statusText, 'Error');

          _this2.isLoading = false;
          dialog.close();
        });
      });
    }
  },
  watch: {
    'per_page': function per_page() {
      this.loadChallenges();
    },
    'blockedOnly': function blockedOnly() {
      this.loadChallenges();
    },
    '$route.name': function $routeName() {
      this.freeTrivia = this.$route.name.indexOf('cost') >= 0;
      this.loadChallenges();
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/trivia-challenges/Index.vue?vue&type=style&index=0&lang=css&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/admin/components/trivia-challenges/Index.vue?vue&type=style&index=0&lang=css& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\ntable {\n    width: 100%;\n    border-spacing: 0 26px;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/trivia-challenges/Index.vue?vue&type=style&index=0&lang=css&":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/admin/components/trivia-challenges/Index.vue?vue&type=style&index=0&lang=css& ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader??ref--7-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Index.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/trivia-challenges/Index.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/trivia-challenges/Index.vue?vue&type=template&id=796b50bc&":
/*!********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/admin/components/trivia-challenges/Index.vue?vue&type=template&id=796b50bc& ***!
  \********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "app-content content" },
    [
      _vm.isLoading
        ? _c("BlockUI", { attrs: { message: "Loading..." } }, [
            _c("i", { staticClass: "fa fa-cog fa-spin fa-3x fa-fw" })
          ])
        : _vm._e(),
      _vm._v(" "),
      _c("div", { staticClass: "content-wrapper" }, [
        _c("div", { staticClass: "content-body" }, [
          _c("section", { attrs: { id: "combination-charts" } }, [
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-12" }, [
                _c(
                  "div",
                  { staticClass: "card rounded p-3 admin-overview-main" },
                  [
                    _c("div", { staticClass: "row" }, [
                      _vm._m(0),
                      _vm._v(" "),
                      _vm._m(1),
                      _vm._v(" "),
                      _c(
                        "ul",
                        {
                          staticClass:
                            "nav nav-tabs nav-underline no-hover-bg mt-2"
                        },
                        [
                          _c(
                            "li",
                            { staticClass: "nav-item" },
                            [
                              _c(
                                "router-link",
                                {
                                  staticClass: "nav-link",
                                  class: { active: !_vm.freeTrivia },
                                  attrs: {
                                    to: { name: "trivia.challenges.index" }
                                  }
                                },
                                [_vm._v("Free Trivia challenges ")]
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "li",
                            { staticClass: "nav-item" },
                            [
                              _c(
                                "router-link",
                                {
                                  staticClass: "nav-link",
                                  class: { active: _vm.freeTrivia },
                                  attrs: {
                                    to: { name: "trivia.challenges.cost.index" }
                                  }
                                },
                                [_vm._v("Cost Trivia challenges ")]
                              )
                            ],
                            1
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-lg-12" }, [
                        _c(
                          "div",
                          { staticClass: "maain-tabble table-responsive" },
                          [
                            _c(
                              "div",
                              {
                                staticClass:
                                  "dataTables_wrapper container-fluid dt-bootstrap4 no-footer",
                                attrs: { id: "DataTables_Table_0_wrapper" }
                              },
                              [
                                _c("div", { staticClass: "row" }, [
                                  _c(
                                    "div",
                                    { staticClass: "col-sm-12 col-md-6" },
                                    [
                                      _c("table-length", {
                                        on: {
                                          lengthChanged: function($event) {
                                            _vm.per_page = $event
                                          }
                                        }
                                      })
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "div",
                                    { staticClass: "col-sm-12 col-md-6" },
                                    [
                                      _c(
                                        "div",
                                        {
                                          staticClass: "dataTables_filter",
                                          attrs: {
                                            id: "DataTables_Table_0_filter"
                                          }
                                        },
                                        [
                                          _c("label", [
                                            _vm._v(
                                              "Search:\n                                                            "
                                            ),
                                            _c("input", {
                                              directives: [
                                                {
                                                  name: "model",
                                                  rawName: "v-model",
                                                  value: _vm.search_term,
                                                  expression: "search_term"
                                                }
                                              ],
                                              staticClass:
                                                "form-control form-control-sm",
                                              attrs: {
                                                type: "search",
                                                placeholder: "",
                                                "aria-controls":
                                                  "DataTables_Table_0"
                                              },
                                              domProps: {
                                                value: _vm.search_term
                                              },
                                              on: {
                                                input: [
                                                  function($event) {
                                                    if (
                                                      $event.target.composing
                                                    ) {
                                                      return
                                                    }
                                                    _vm.search_term =
                                                      $event.target.value
                                                  },
                                                  _vm.loadChallenges
                                                ]
                                              }
                                            })
                                          ])
                                        ]
                                      )
                                    ]
                                  )
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "row" }, [
                                  _c("div", { staticClass: "col-sm-12" }, [
                                    _c(
                                      "table",
                                      {
                                        staticClass:
                                          "table table-striped table-bordered zero-configuration dataTable no-footer",
                                        attrs: { id: "DataTables_Table_0" }
                                      },
                                      [
                                        _c("thead", [
                                          _c("tr", [
                                            _c("th", [_vm._v("SN.o")]),
                                            _vm._v(" "),
                                            _c("th", [_vm._v("Challenge")]),
                                            _vm._v(" "),
                                            _c("th", [
                                              _vm._v("Total Challenges")
                                            ]),
                                            _vm._v(" "),
                                            _c("th", [
                                              _vm._v("Points Per Question")
                                            ]),
                                            _vm._v(" "),
                                            _c("th", [_vm._v("Total Time")]),
                                            _vm._v(" "),
                                            _vm.freeTrivia
                                              ? _c("th", [_vm._v("Fee")])
                                              : _vm._e(),
                                            _vm._v(" "),
                                            _c("th", [_vm._v("Action")])
                                          ])
                                        ]),
                                        _vm._v(" "),
                                        _c(
                                          "tbody",
                                          _vm._l(_vm.challenges.data, function(
                                            challenge,
                                            index
                                          ) {
                                            return _c(
                                              "tr",
                                              { key: challenge.id },
                                              [
                                                _c("td", [
                                                  _vm._v(_vm._s(index + 1))
                                                ]),
                                                _vm._v(" "),
                                                _c("td", [
                                                  _vm._v(
                                                    _vm._s(challenge.title)
                                                  )
                                                ]),
                                                _vm._v(" "),
                                                _c("td", [
                                                  _vm._v(
                                                    _vm._s(
                                                      challenge.questions_count
                                                    )
                                                  )
                                                ]),
                                                _vm._v(" "),
                                                _c("td", [
                                                  _vm._v(
                                                    _vm._s(challenge.points)
                                                  )
                                                ]),
                                                _vm._v(" "),
                                                _c("td", [
                                                  _vm._v(_vm._s(challenge.time))
                                                ]),
                                                _vm._v(" "),
                                                _vm.freeTrivia
                                                  ? _c("td", [
                                                      _vm._v(
                                                        _vm._s(challenge.amount)
                                                      )
                                                    ])
                                                  : _vm._e(),
                                                _vm._v(" "),
                                                _c("td", [
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass:
                                                        "btn-group mr-1 mb-1"
                                                    },
                                                    [
                                                      _vm._m(2, true),
                                                      _vm._v(" "),
                                                      _c(
                                                        "div",
                                                        {
                                                          staticClass:
                                                            "dropdown-menu",
                                                          staticStyle: {
                                                            position:
                                                              "absolute",
                                                            transform:
                                                              "translate3d(0px, 21px, 0px)",
                                                            top: "0px",
                                                            left: "0px",
                                                            "will-change":
                                                              "transform"
                                                          },
                                                          attrs: {
                                                            "x-placement":
                                                              "bottom-start"
                                                          }
                                                        },
                                                        [
                                                          _c(
                                                            "router-link",
                                                            {
                                                              staticClass:
                                                                "dropdown-item",
                                                              attrs: {
                                                                to: {
                                                                  name:
                                                                    "challenges.edit",
                                                                  params: {
                                                                    id:
                                                                      challenge.id
                                                                  }
                                                                }
                                                              }
                                                            },
                                                            [
                                                              _c("i", {
                                                                staticClass:
                                                                  "fa fa-pencil"
                                                              }),
                                                              _vm._v(
                                                                "Edit\n                                                                        "
                                                              )
                                                            ]
                                                          ),
                                                          _vm._v(" "),
                                                          _c(
                                                            "router-link",
                                                            {
                                                              staticClass:
                                                                "dropdown-item",
                                                              attrs: {
                                                                to: {
                                                                  name:
                                                                    "challenges.show",
                                                                  params: {
                                                                    id:
                                                                      challenge.id,
                                                                    tab:
                                                                      "details"
                                                                  }
                                                                }
                                                              }
                                                            },
                                                            [
                                                              _c("i", {
                                                                staticClass:
                                                                  "fa fa-eye"
                                                              }),
                                                              _vm._v(
                                                                "View\n                                                                        "
                                                              )
                                                            ]
                                                          ),
                                                          _vm._v(" "),
                                                          _c(
                                                            "a",
                                                            {
                                                              staticClass:
                                                                "dropdown-item",
                                                              attrs: {
                                                                href: "#"
                                                              },
                                                              on: {
                                                                click: function(
                                                                  $event
                                                                ) {
                                                                  $event.preventDefault()
                                                                  return _vm.blockUser(
                                                                    index
                                                                  )
                                                                }
                                                              }
                                                            },
                                                            [
                                                              _c("i", {
                                                                staticClass:
                                                                  "fa fa-ban"
                                                              }),
                                                              _vm._v(
                                                                _vm._s(
                                                                  !challenge.status
                                                                    ? "UnBlocked User"
                                                                    : "Block User"
                                                                ) +
                                                                  "\n                                                                        "
                                                              )
                                                            ]
                                                          )
                                                        ],
                                                        1
                                                      )
                                                    ]
                                                  )
                                                ])
                                              ]
                                            )
                                          }),
                                          0
                                        )
                                      ]
                                    )
                                  ])
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "row" }, [
                                  _c(
                                    "div",
                                    { staticClass: "col-sm-12 col-md-5" },
                                    [
                                      _vm.challenges &&
                                      _vm.challenges.meta &&
                                      _vm.challenges.meta.from
                                        ? _c(
                                            "div",
                                            {
                                              staticClass: "dataTables_info",
                                              attrs: {
                                                id: "DataTables_Table_0_info",
                                                role: "status",
                                                "aria-live": "polite"
                                              }
                                            },
                                            [
                                              _vm._v(
                                                "Showing " +
                                                  _vm._s(
                                                    _vm.challenges.meta.from
                                                  ) +
                                                  " to " +
                                                  _vm._s(
                                                    _vm.challenges.meta.to
                                                  ) +
                                                  " of " +
                                                  _vm._s(
                                                    _vm.challenges.meta.total
                                                  ) +
                                                  " entries"
                                              )
                                            ]
                                          )
                                        : _vm._e()
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "div",
                                    { staticClass: "col-sm-12 col-md-7" },
                                    [
                                      _c(
                                        "div",
                                        {
                                          staticClass:
                                            "dataTables_paginate paging_simple_numbers",
                                          attrs: {
                                            id: "DataTables_Table_0_paginate"
                                          }
                                        },
                                        [
                                          _c(
                                            "pagination",
                                            {
                                              attrs: { data: _vm.challenges },
                                              on: {
                                                "pagination-change-page":
                                                  _vm.loadChallenges
                                              }
                                            },
                                            [
                                              _c(
                                                "span",
                                                {
                                                  attrs: { slot: "prev-nav" },
                                                  slot: "prev-nav"
                                                },
                                                [_vm._v("< Previous")]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "span",
                                                {
                                                  attrs: { slot: "next-nav" },
                                                  slot: "next-nav"
                                                },
                                                [_vm._v("Next >")]
                                              )
                                            ]
                                          )
                                        ],
                                        1
                                      )
                                    ]
                                  )
                                ])
                              ]
                            )
                          ]
                        )
                      ])
                    ])
                  ]
                )
              ])
            ])
          ])
        ])
      ])
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-6 col-sm-12" }, [
      _c("h1", [_vm._v("Fitness Challenges")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-6 col-sm-12" }, [
      _c("a", { staticClass: "green-btn-project", attrs: { href: "#" } }, [
        _c("i", {
          staticClass: "fa fa-plus-circle",
          attrs: { "aria-hidden": "true" }
        }),
        _vm._v(" Create New Challenge  ")
      ]),
      _vm._v(" "),
      _c(
        "a",
        {
          staticClass: "fit-setting-a",
          attrs: { href: "admin-trivia-settings.html" }
        },
        [
          _c("i", {
            staticClass: "fa fa-cog",
            attrs: { "aria-hidden": "true" }
          })
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "btn dropdown-toggle btn-drop-table btn-sm",
        attrs: {
          type: "button",
          "data-toggle": "dropdown",
          "aria-haspopup": "true",
          "aria-expanded": "false"
        }
      },
      [_c("i", { staticClass: "fa fa-ellipsis-v" })]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/admin/components/trivia-challenges/Index.vue":
/*!*******************************************************************!*\
  !*** ./resources/js/admin/components/trivia-challenges/Index.vue ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Index_vue_vue_type_template_id_796b50bc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Index.vue?vue&type=template&id=796b50bc& */ "./resources/js/admin/components/trivia-challenges/Index.vue?vue&type=template&id=796b50bc&");
/* harmony import */ var _Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Index.vue?vue&type=script&lang=js& */ "./resources/js/admin/components/trivia-challenges/Index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Index.vue?vue&type=style&index=0&lang=css& */ "./resources/js/admin/components/trivia-challenges/Index.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Index_vue_vue_type_template_id_796b50bc___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Index_vue_vue_type_template_id_796b50bc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/admin/components/trivia-challenges/Index.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/admin/components/trivia-challenges/Index.vue?vue&type=script&lang=js&":
/*!********************************************************************************************!*\
  !*** ./resources/js/admin/components/trivia-challenges/Index.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/trivia-challenges/Index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/admin/components/trivia-challenges/Index.vue?vue&type=style&index=0&lang=css&":
/*!****************************************************************************************************!*\
  !*** ./resources/js/admin/components/trivia-challenges/Index.vue?vue&type=style&index=0&lang=css& ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader??ref--7-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Index.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/trivia-challenges/Index.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/admin/components/trivia-challenges/Index.vue?vue&type=template&id=796b50bc&":
/*!**************************************************************************************************!*\
  !*** ./resources/js/admin/components/trivia-challenges/Index.vue?vue&type=template&id=796b50bc& ***!
  \**************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_796b50bc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Index.vue?vue&type=template&id=796b50bc& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/admin/components/trivia-challenges/Index.vue?vue&type=template&id=796b50bc&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_796b50bc___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_796b50bc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);