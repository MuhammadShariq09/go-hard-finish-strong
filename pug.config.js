const path = require('path');
const webpack = require('webpack');

module.exports = {
    module: {
        // loaders: [
        //     {
        //         test: /\.js$/,
        //         loader: 'babel-loader',
        //         exclude: [/node_modules/],
        //         query: {
        //             presets: ['es2015'],
        //             plugins: ["transform-object-assign", "transform-runtime"]
        //         }
        //     }
        // ],
        rules: [
            {
                test: /\.pug$/,
                loader: 'pug-plain-loader'
            }
        ]
    },
};
