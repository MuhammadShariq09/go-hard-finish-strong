const server = require('http').Server();
const io = require('socket.io')(server);
const Redis = require('ioredis');
const redis = new Redis();
require('dotenv').config();


const laravelPrefix = 'go_hard_finish_strong_database_private';

redis.psubscribe(`${laravelPrefix}-user-channel`);

redis.on('pmessage', function(channel, s, message){

    message = JSON.parse(message);

    console.log(`${channel.replace('go_hard_finish_strong_database_','')}:${message.event}`);

    // console.log(message, channel.replace('go_hard_finish_strong_database_',''));

    io.emit(`${channel.replace('go_hard_finish_strong_database_','')}:${message.event}`, message);
});

const port = process.env.NODE_ENV === 'production' ? process.env.MIX_PRODUCION_SOCKET_PORT: process.env.MIX_SOCKET_PORT;
// const port = 9999;

server.listen(port, function(){
    console.log(`server is listening on port ${port}`);
});
