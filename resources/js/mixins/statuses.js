export default {
    methods: {
        statuses(status){
            switch(status){
                case 0:
                    return 'PENDING';
                case 1:
                    return 'ACCEPTED';
                case 2:
                    return 'REJECTED';
                case 3:
                    return 'IN_VOTING';
                case 4:
                    return 'SUBMITTED';
                case 5:
                    return 'COMPLETED';
                case 6:
                    return 'DISPUTE';
                default:
                    return status;
            }
        }
    }
}
