export default {
    methods: {
        getGenderString(number){
            switch (number) {
                case '1':
                case 1:
                    return "Male";
                case '2':
                case 2:
                    return "Female";
                default:
                    return "Both";
            }
        },
    }
}
