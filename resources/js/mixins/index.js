import StatusesMixins from './statuses';
import GendersMixins from './genders';
import UtilMixins from './utils';

export default [
    StatusesMixins,
    GendersMixins,
    UtilMixins,
];
