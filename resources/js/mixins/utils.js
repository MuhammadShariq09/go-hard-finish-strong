export default {
    data(){
        return {
            baseUrl: window.base_url,
        };
    },
    methods: {
        profileImage: (path, name, justReturn = false) => {
            if(justReturn)
                return path;
            return path? path:`https://ui-avatars.com/api/?rounded=true&background=20aaea&color=fff&name=${name}`
        },
        makeSerial(index, currentPage, perPage){
            return index + (perPage * currentPage - perPage);
        },
        redirectBack(){
            window.history.back();
        }
    }
}
