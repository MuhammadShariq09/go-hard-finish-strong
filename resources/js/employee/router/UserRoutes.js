import UserIndex from "@admin/components/users/IndexComponent";
import UserCreate from "@admin/components/users/CreateComponent";
import ChallengesShow from "@admin/components/challenges/ShowComponent";
import UserShow from "@admin/components/users/ShowComponent";
import ProfileShow from "@admin/components/profile/ShowComponent";
import ChallengesIndex from "@admin/components/challenges/IndexComponent";

export default [
    {
        path: '/users',
        name: 'users.index',
        component: UserIndex,
        meta: {
            title: "Users List",
            description: ""
        }
    },
    {
        path: '/users/create',
        name: 'users.create',
        component: UserCreate,
        meta: {
            title: "Add User",
            description: ""
        }
    },
    {
        path: '/users/:id/',
        name: 'users.edit',
        component: UserCreate,
        meta: {
            title: "Edit User",
            description: ""
        }
    },
    {
        path: '/users/:userId/challenges/:id',
        name: 'user.challenges.show',
        component: ChallengesShow,
        meta: {
            title: "Edit User",
            description: ""
        }
    },
    {
        path: '/users/:id/:tab',
        name: 'users.show',
        component: UserShow,
        meta: {
            title: "User Profile",
            description: ""
        }
    },
    {
        path: '/profile/:edit?',
        name: 'profile.show',
        component: ProfileShow,
        meta: {
            title: "User Edit",
            description: ""
        }
    },
    {
        path: '/challenges/ftfp',
        name: 'challenges.index',
        component: ChallengesIndex,
        meta: {
            title: "Challenges List",
            description: ""
        }
    },
];
