import NotFound from "@admin/components/NotFoundComponent";
import DashboardComponent from "@employee/components/DashboardComponent";
import UserRoutes from "@employee/router/UserRoutes";
import FitnessChallengesRoutes from "./FitnessChallengesRoutes";
import FeedbackPollingRoutes from "../../admin/routes/FeedbackPollingRoutes";

export default [
    {
        path: '*',
        name: 'notfound',
        component: NotFound,
        meta: {
            title: "Not found",
            description: ""
        }
    },
    {
        path: '/',
        redirectTo: '/dashboard'
    },
    {
        path: '/dashboard',
        name: 'home',
        component: DashboardComponent,
        meta: {
            title: "Dashboard",
            description: ""
        }
    },
    ...UserRoutes,
    ...FitnessChallengesRoutes,
    ...FeedbackPollingRoutes
];
