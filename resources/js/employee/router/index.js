import VueRouter from "vue-router";
import routeBeforeEach from "@middleware/routeBeforeEach";

import basePath from "@utils/base_url";

import Routes from '@employee/router/router';

const router = new VueRouter({
    mode: 'history',
    base: `${basePath}/employee`,
    routes: Routes
});

router.beforeEach(routeBeforeEach);

export default router;
