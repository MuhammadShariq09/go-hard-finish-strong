const Filters = {
    status: (value) => {
        switch (value) {
            case 0:
                return "Pending";
            case 1:
                return "Active";
            case 2:
                return "REJECTED";
            case 3:
                return "IN VOTING";
            case 4:
                return "SUBMITTED";
            case 5:
                return "COMPLETED";
            case 6:
                return "DISPUTE";
            case "DEFEATED":
                return "DEFEATED";
            case "WINNER":
                return "WINNER";
            default:
                return "Invalid Status"
        }
    }
};
export default Filters;
