const Filters = {
    gender: (value) => {
        if(!value) return;
        switch(value.toString())
        {
            case '1':
                return "Male";
            case '2':
                return "Female";
            default: return 'Both';
        }
    }
};
export default Filters;
