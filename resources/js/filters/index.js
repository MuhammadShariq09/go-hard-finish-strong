import dateTime from './datetime_filters';
import status from './status';
import gender from './gender_filters'
import currency from './currency_filters'

export default {
    ...dateTime,
    ...status,
    ...gender,
    ...currency,
};
