let basePath = process.env.MIX_LOCAL_FOLDER;

if(process.env.NODE_ENV === 'staging')
    basePath = process.env.MIX_STAGING_FOLDER;
else if(process.env.NODE_ENV === 'production')
    basePath = process.env.MIX_PRODUCTION_FOLDER;

export default basePath;
