import FeedbackIndex from "../components/feedback/IndexComponent";
import FeedbackShow from "../components/feedback/ShowComponent";
import PollingRequestsIndex from "../components/polling-requests/IndexComponent";
import PollingRequestsShow from "../components/polling-requests/ShowComponent";

export default [

    {
        path: '/feedback',
        name: 'feedback.index',
        component: FeedbackIndex,
        meta: {
            title: "Feedback",
            description: ""
        }
    },
    {
        path: '/feedback/:id/show',
        name: 'feedback.show',
        component: FeedbackShow,
        meta: {
            title: "Feedback details",
            description: ""
        }
    },
    {
        path: '/polling-requests',
        name: 'polling.requests.index',
        component: PollingRequestsIndex,
        meta: {
            title: "Polling Requests",
            description: ""
        }
    },
    {
        path: '/polling-requests/:id',
        name: 'polling.requests.show',
        component: PollingRequestsShow,
        meta: {
            title: "Polling Requests Details",
            description: ""
        }
    },
];
