import DashboardComponent from '../components/DashboardComponent';
import EditProfileComponent from '../components/users/EditProfileComponent';
import SettingsComponent from '../components/SettingsComponent';
import UserIndex from '../components/users/IndexComponent';
import UserCreate from '../components/users/CreateComponent';
import UserShow from '../components/users/ShowComponent';
import ProfileShow from '../components/profile/ShowComponent';
import ChallengesIndex from '../components/challenges/IndexComponent';
import ChallengesShow from '../components/challenges/ShowComponent';
import NotFound from '../components/NotFoundComponent';

import NotificationComponent from '../components/notifications/Index';
import PaymentLogs from '../components/payment-logs/Index';

import EmployeeRoutes from './EmployeeRoutes';
import FitnessChallengesRoutes from "./FitnessChallengesRoutes";
import TriviaChallengesRoutes from "./TriviaChallengesRoutes";
import FTFPChallengesRoutes from "./FTFPChallengesRoutes";
import FeedbackPollingRoutes from "./FeedbackPollingRoutes";
import LeaderBoardIndex from '../components/leader_board/IndexComponent';

export default [
    {
        path: '*',
        name: 'notfound',
        component: NotFound,
        meta: {
            title: "Not found",
            description: ""
        }
    },
    {
        path: '/dashboard',
        name: 'home',
        component: DashboardComponent,
        meta: {
            title: "Dashboard",
            description: ""
        }
    },
    {
        path: '/notifications',
        name: 'notifications.index',
        component: NotificationComponent,
        meta: {
            title: "Notifications",
            description: ""
        }
    },
    {
        path: '/profile/:id',
        name: 'profile.edit',
        component: UserCreate,
        meta: {
            title: "Profile",
            description: ""
        }
    },
    {
        path: '/settings/:module',
        name: 'settings',
        component: SettingsComponent,
        meta: {
            title: "Settings",
            description: ""
        }
    },
    {
        path: '/users',
        name: 'users.index',
        component: UserIndex,
        meta: {
            title: "Users List",
            description: ""
        }
    },
    {
        path: '/users/create',
        name: 'users.create',
        component: UserCreate,
        meta: {
            title: "Add User",
            description: ""
        }
    },
    {
        path: '/users/:id/',
        name: 'users.edit',
        component: UserCreate,
        meta: {
            title: "Edit User",
            description: ""
        }
    },
    {
        path: '/users/:userId/challenges/:id',
        name: 'user.challenges.show',
        component: ChallengesShow,
        meta: {
            title: "Edit User",
            description: ""
        }
    },
    {
        path: '/users/:id/:tab',
        name: 'users.show',
        component: UserShow,
        meta: {
            title: "User Profile",
            description: ""
        }
    },
    {
        path: '/profile/:edit?',
        name: 'profile.show',
        component: ProfileShow,
        meta: {
            title: "Admin Profile",
            description: ""
        }
    },
    {
        path: '/challenges/ftfp',
        name: 'challenges.index',
        component: ChallengesIndex,
        meta: {
            title: "Challenges List",
            description: ""
        }
    },
    ...FitnessChallengesRoutes,
    ...EmployeeRoutes,
    ...TriviaChallengesRoutes,
    ...FTFPChallengesRoutes,
    ...FeedbackPollingRoutes,
    {
        path: '/payments/:type?',
        name: 'payment.logs.index',
        component: PaymentLogs,
        meta: {
            title: "Payment Logs",
            description: ""
        }
    },
    {
        path: '/leaderboard',
        name: 'leaderboard.index',
        component: LeaderBoardIndex,
        meta: {
            title: "LeaderBoard",
            description: ""
        }
    },
    // {
    //     path: '/charts',
    //     name: 'charts.index',
    //     component: eChartsComponent,
    //     meta: {
    //         title: "eCharts - Demo",
    //         description: ""
    //     }
    // },
];
