export default [
    {
        path: '/challenges/fitness/closed',
        name: 'fitness.challenges.index.closed',
        component: require('../components/fitness-challenges/Index').default,
        meta: {
            title: "Invitation Challenges",
            description: "",
            type: 'closed'
        }
    },
    {
        path: '/challenges/fitness/invitations',
        name: 'fitness.challenges.index.invitation',
        component: require('../components/fitness-challenges/Index').default,
        meta: {
            title: "Invitation Challenges",
            description: "",
            type: 'invitation'
        }
    },
    {
        path: '/challenges/fitness/invitations/create',
        name: 'fitness.challenges.create.invitation',
        component: require('../components/fitness-challenges/Create').default,
        meta: {
            title: "Add Invitation Challenges",
            description: "",
            type: 'invitation'
        }
    },
    {
        path: '/challenges/fitness/closed/create',
        name: 'fitness.challenges.create.closed',
        component: require('../components/fitness-challenges/Create').default,
        meta: {
            title: "Add Closed Challenges",
            description: "",
            type: 'closed'
        }
    },
    {
        path: '/challenges/fitness/invitations/:id',
        name: 'fitness.challenges.show.invitation',
        component: require('../components/fitness-challenges/Show').default,
        meta: {
            title: "Invitation Challenges Details",
            description: "",
            type: 'invitation'
        }
    },
    {
        path: '/challenges/fitness/closed/:id',
        name: 'fitness.challenges.show.closed',
        component: require('../components/fitness-challenges/Show').default,
        meta: {
            title: "Closed Challenges Details",
            description: "",
            type: 'closed'
        }
    },
    /*{
        path: '/challenges/fitness/invitations/:id/edit',
        name: 'fitness.challenges.edit.invitation',
        component: require('./components/fitness-challenges/Create').default,
        meta: {
            title: "Edit Invitation Challenges",
            description: "",
            type: 'invitation'
        }
    },
    {
        path: '/challenges/fitness/closed/:id/edit',
        name: 'fitness.challenges.edit.closed',
        component: require('./components/fitness-challenges/Create').default,
        meta: {
            title: "Edit Closed Challenges",
            description: "",
            type: 'closed'
        }
    },*/
];
