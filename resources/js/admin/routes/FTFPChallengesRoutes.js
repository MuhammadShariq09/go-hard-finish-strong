import ChallengesCreate from "../components/challenges/CreateComponent";
import FitToPathCreate from "../components/fit-to-path/CreateComponent";
import ChallengesShow from "../components/challenges/ShowComponent";

export default [
    {
        path: '/challenges/create',
        name: 'challenges.create',
        component: ChallengesCreate,
        meta: {
            title: "Add Challenge",
            description: ""
        }
    },
    {
        path: '/challenges/fit-to-path',
        name: 'fit_to_path.create',
        component: FitToPathCreate,
        meta: {
            title: "Fit To Path Points",
            description: ""
        }
    },
    {
        path: '/challenges/:id',
        name: 'challenges.show',
        component: ChallengesShow,
        meta: {
            title: "Challenge Details",
            description: ""
        }
    },
    {
        path: '/challenges/:id/edit',
        name: 'challenges.edit',
        component: ChallengesCreate,
        meta: {
            title: "Edit Challenge",
            description: ""
        }
    },
    {
        path: '/challenges/:id/exercises/:exerciseId/edit',
        name: 'challenges.exercise.edit',
        component: ChallengesCreate,
        meta: {
            title: "Edit Challenge",
            description: ""
        }
    },
];
