import EmployeeIndexComponent from '../components/employees/IndexComponent';
import EmployeeCreateComponent from '../components/users/CreateComponent';
import EmployeeShowComponent from '../components/employees/ShowComponent';

export default [
    {
        path: '/employees',
        name: 'employees.index',
        component: EmployeeIndexComponent,
        meta: {
            title: "Employees Listing",
            description: ""
        }
    },
    {
        path: '/employees/create',
        name: 'employees.create',
        component: EmployeeCreateComponent,
        meta: {
            title: "Add Employees",
            description: "",
            employee: true
        }
    },
    {
        path: '/employees/:id',
        name: 'employees.show',
        component: EmployeeShowComponent,
        meta: {
            title: "Employees Details",
            description: "",
            employee: true
        }
    }
];
