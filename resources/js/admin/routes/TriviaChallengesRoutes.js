import TriviaChallengeIndex from '../components/trivia-challenges/Index';
import TriviaChallengeCreate from '../components/trivia-challenges/Create';
import TriviaChallengeShow from '../components/trivia-challenges/Show';

export default [

    {
        path: '/challenges/trivia',
        name: 'trivia.challenges.index',
        component: TriviaChallengeIndex,
        meta: {
            title: "Free Trivia Challenges",
            description: ""
        }
    },
    {
        path: '/challenges/trivia/cost',
        name: 'trivia.challenges.cost.index',
        component: TriviaChallengeIndex,
        meta: {
            title: "Cost Trivia Challenges",
            description: ""
        }
    },{
        path: '/challenges/trivia/cost/create',
        name: 'trivia.challenges.cost.create',
        component: TriviaChallengeCreate,
        meta: {
            title: "Create Cost Trivia Challenges",
            description: ""
        }
    },
    {
        path: '/challenges/trivia/free/create',
        name: 'trivia.challenges.create',
        component: TriviaChallengeCreate,
        meta: {
            title: "Create Free Trivia Challenges",
            description: ""
        }
    },
    {
        path: '/challenges/trivia/:id/show',
        name: 'trivia.challenges.show',
        component: TriviaChallengeShow,
        meta: {
            title: "Trivia Challenges Details",
            description: ""
        }
    },
    {
        path: '/challenges/trivia/:id/edit',
        name: 'trivia.challenges.edit',
        component: TriviaChallengeCreate,
        meta: {
            title: "Edit Trivia Challenges",
            description: ""
        }
    },
];
