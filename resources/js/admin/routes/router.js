import VueRouter from "vue-router";
import routeBeforeEach from "@middleware/routeBeforeEach";

/*const DashboardComponent = () => import(/!* webpackChunkName: "ftfp-settings" *!/ './components/DashboardComponent');
const EditProfileComponent = () => import(/!* webpackChunkName: "ftfp-settings" *!/ './components/users/EditProfileComponent');
const SettingsComponent = () => import(/!* webpackChunkName: "ftfp-settings" *!/ './components/SettingsComponent');
const UserIndex = () => import(/!* webpackChunkName: "index-users" *!/ './components/users/IndexComponent');
const UserCreate = () => import(/!* webpackChunkName: "user-create" *!/ './components/users/CreateComponent');
const UserShow = () => import(/!* webpackChunkName: "user-show" *!/ './components/users/ShowComponent');
const ProfileShow = () => import(/!* webpackChunkName: "single-profile" *!/ './components/profile/ShowComponent');
const ChallengesIndex = () => import(/!* webpackChunkName: "index-ftfp-challenges" *!/ './components/challenges/IndexComponent');
const ChallengesCreate = () => import(/!* webpackChunkName: "create-ftfp-challenges" *!/ './components/challenges/CreateComponent');
const ChallengesShow = () => import(/!* webpackChunkName: "single-ftfp-challenges" *!/ './components/challenges/ShowComponent');
const NotFound = () => import(/!* webpackChunkName: "create-fittopath" *!/ './components/NotFoundComponent');
const FitToPathCreate = () => import(/!* webpackChunkName: "create-fittopath" *!/ './components/fit-to-path/CreateComponent');
const FeedbackIndex = () => import(/!* webpackChunkName: "index-feedback" *!/ './components/feedback/IndexComponent');
const FeedbackShow = () => import(/!* webpackChunkName: "single-feedback" *!/ './components/feedback/ShowComponent');
const PollingRequestsIndex = () => import(/!* webpackChunkName: "index-polling-request" *!/ './components/polling-requests/IndexComponent');
const PollingRequestsShow = () => import(/!* webpackChunkName: "single-polling-request" *!/ './components/polling-requests/ShowComponent');
const TriviaChallengeIndex = () => import(/!* webpackChunkName: "index-trivia-challenge" *!/ './components/trivia-challenges/Index');
const TriviaChallengeCreate = () => import(/!* webpackChunkName: "create-trivia-challenge" *!/ './components/trivia-challenges/Create');
const TriviaChallengeShow = () => import(/!* webpackChunkName: "show-trivia-challenge" *!/ './components/trivia-challenges/Show');
const NotificationComponent = () => import(/!* webpackChunkName: "index-notification" *!/ './components/notifications/Index');*/



// import eChartsComponent from "../components/eChartsComponent";

import basePath from "@utils/base_url";

import Routes from './index';

const router = new VueRouter({
    mode: 'history',
    base: `${basePath}/admin`,
    routes: Routes
});

router.beforeEach(routeBeforeEach);

export default router;
