require('@/bootstrap');

import Vue from 'vue';
import VueRouter from 'vue-router';
import VuejsDialog from 'vuejs-dialog';

import VeeValidate from 'vee-validate';
import VueToastr2 from 'vue-toastr-2'
import 'vue-toastr-2/dist/vue-toastr-2.min.css';
import VueTimeago from 'vue-timeago'

Vue.use(VueTimeago, {
    name: 'Timeago', // Component name, `Timeago` by default
    locale: 'en', // Default locale
    // We use `date-fns` under the hood
    // So you can use all locales from it
    // locales: {
    //     'zh-CN': require('date-fns/locale/zh_cn'),
    //     ja: require('date-fns/locale/ja')
    // }
});

// import ECharts from 'vue-echarts' // refers to components/ECharts.vue in webpack

import BlockUI from 'vue-blockui';

Vue.use(BlockUI);
Vue.use(VuejsDialog, {
    html: true,
    loader: true,
    okText: 'Proceed',
    cancelText: 'Cancel',
    animation: 'bounce'
});
// include the default style
import VueGooglePlaces from 'vue-google-places'
import 'vuejs-dialog/dist/vuejs-dialog.min.css';
Vue.use(VueGooglePlaces);

window.toastr = require('toastr');

Vue.use(VueToastr2);
Vue.use(VeeValidate);
Vue.use(VueRouter);

import Filters from '../filters/index.js';
for (let filter in Filters) Vue.filter(filter, Filters[filter]);

import Vue2Pipes from 'vue2-pipes';
Vue.use(Vue2Pipes);

import CustomMixins from '../mixins/index';
for (let mixin in CustomMixins) Vue.mixin(CustomMixins[mixin]);

import router from './routes/router';

Vue.component('top-header', require('./components/HeaderComponent').default);
Vue.component('side-navbar', require('./components/SidebarComponent').default);
Vue.component('pagination', require('laravel-vue-pagination'));
Vue.component('table-length', require('../components/TableLengthComponent').default);

// Vue.component('v-chart', ECharts);

import App from './components/AppComponent.vue';

Vue.config.productionTip = false;
Vue.prototype.$isEmployee = false;
Vue.prototype.$isAdmin = true;

const app = new Vue({
    el: '#app',
    router,
    components: { App },
    mounted(){

    },
    beforeRouteUpdate(to, from, next) {
        console.log(to, from);
        // next()
    }
});
