window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
    require('../app-assets/vendors/js/tables/datatable/datatables.min.js');
} catch (e) {}


/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

import basePath from "@utils/base_url";

// do not use this variable
// const baseUrl = process.env.NODE_ENV === 'production' ? process.env.MIX_PRODUCTION_BASE_URL: process.env.MIX_BASE_URL;

window.axios.defaults.baseURL =  `${basePath? '/' + basePath: ''}/admin/api`;

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

import toastr from 'toastr'

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

/*window.axios.interceptors.response.use(res => {
    return res;
}, err => {
    if(err.response.status === 401)
        // window.location.reload();
    toastr.error(err.response.data.message, 'Error!');
    return Promise.reject(err);
});*/

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo'

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     encrypted: true
// });


$(window).on('resize', function(){
    // 768
    let ww = $(window).width();
    let body = document.querySelector('body'); // Using a class instead, see note below.
    if(ww < 768)
    {
        body.classList.remove('vertical-menu');
        body.classList.remove('menu-expanded');
    }else{
        body.classList.add('vertical-menu');
        body.classList.add('menu-expanded');
    }
});

$(document).on('keypress', 'input[type=number]', function(evt){
    if (evt.which !== 8 && evt.which !== 0 && evt.which < 48 || evt.which > 57)
        evt.preventDefault();
});
