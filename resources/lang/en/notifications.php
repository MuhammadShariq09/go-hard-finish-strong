<?php


return [
    'titles' => [
        'challenge_declared_result' => 'Challenge Result',
        'challenge_accepted' => 'Challenge Accepted',
        'challenge_rejected' => 'Challenge Rejected',
        'challenge_sent' => 'Challenge Request',
        'challenge_submitted' => 'Challenge Submitted',
        'challenge_voted' => 'Challenge Voted',
        'sent_friend_request' => 'Friend Request',
    ],
    'body' => [
        'challenge_declared_result' => 'Challenge result has been declared by the competitor',
        'challenge_declared_result_by_admin' => 'Result has been declared by admin',
        'challenge_declared_result_success' => 'Congratulations! you have won!',
        'challenge_declared_result_lost' => 'You have lost the challenge!',
        'challenge_declared_result_in_pool' => 'Your Challenge is in pool now!',
        'challenge_accepted' => ':name has accepted your challenge request',
        'challenge_rejected' => ':name rejected your challenge request',
        'challenge_sent' => ':name challenge you.',
        'challenge_submitted' => ':name submitted challenge.',
        'challenge_voted' => ':name vote in your challenge.',
        'sent_friend_request' => ':name sent you friend request',
    ],
];
