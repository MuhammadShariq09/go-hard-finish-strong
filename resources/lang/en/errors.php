<?php


return [
    'not_enough_initial_request_to_send_challenge_request' => 'You do not have enough request to send. please wait to reset your request',
    'not_enough_accept_request_to_accept_challenge_request' => 'You do not have enough request to accept. please wait to reset your privileges',
    'recipient_id_should_not_be_same' => 'Recipient User ID Must not be same as logged in user',
    'sender_id_should_not_be_same' => 'Sender User ID Must not be same as logged in user',
    'unauthorized' => 'Unauthorized request.',
    'unauthorized_to_accept_challenge_request' => 'Unauthorized to accept this request',
    'reached_to_accept_challenge_request_limit' => 'You can only accept challenge requests after 72 hours',
    'request_is_already_accepted' => 'You cannot reject the challenge request due to The challenge request Already accepted or closed',
    'request_is_already_completed' => 'You cannot redeclare as winner/defeated',
];
