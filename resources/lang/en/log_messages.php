<?php


return [
    'send_challenge_request' => 'You send challenge request to :recipient_name',
    'challenge_request_accepted' => 'You Accepted challenge request from :sender_name',
    'challenge_request_rejected' => 'You rejected challenge request from :sender_name',
    'you_declared_challenge' => 'You Declare result for :challenge_name',
    'you_voted' => 'you give a vote',
    'fitness_challenge_accepted' => 'You Accepted :type challenge.',
    'fitness_challenge_submit_video' => 'You have submitted video.',
    'fitness_challenge_submit_phase' => 'You have submitted phase.',
];
