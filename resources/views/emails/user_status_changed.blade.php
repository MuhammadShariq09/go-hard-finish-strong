@extends('emails.master')

@section('title', "Friends Request Cancelled")

@section('content')

    @if($user -> status)

        <h1>Congratulations!</h1>
        <h5>Your Account has been reactivated</h5>
    @else
        <h1>Opx...</h1>
        <h5>Your Account has been suspended!</h5>
    @endif

    <table align="center" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
        <tr>
            <td align="center">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                        <td align="center">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                <tr>
                                    <td>
                                        </a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>

@endsection
