@extends('emails.master')

@section('title', "Friends Request Cancelled")

@section('content')

    <h5>You have received fitness challenge invitation.</h5>

    <table align="center" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
        <tr>
            <td align="center">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                        <td align="center">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                <tr>
                                    <td>
                                        <p>Please accept from you app.</p>

                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>

@endsection
