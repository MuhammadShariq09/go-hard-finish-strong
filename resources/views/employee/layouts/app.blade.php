<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }}</title>
    <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}"/>
    <link href="https://fonts.googleapis.com/css?family=Khand:300,400,500,600,700&display=swap" rel="stylesheet">

    {{--<link rel="stylesheet" href="{{ asset('app-assets/css/vendors.css') }}">
    <link rel="stylesheet" href="{{ asset('app-assets/vendors/css/forms/icheck/icheck.css') }}">
    <link rel="stylesheet" href="{{ asset('app-assets/css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('app-assets/vendors/css/forms/icheck/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    <link rel="stylesheet" href="{{ asset('app-assets/css/plugins/extensions/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ asset('app-assets/css/pages/login-register.css') }}">
    <link rel="stylesheet" href="{{ asset('app-assets/css/style.css') }}">--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('admin-assets/css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.4.1/cropper.css">
    @yield('css')
    <script type="text/javascript">
        window.base_url = "{{ url('/') }}";
        window.user = @json(auth()->guard('employee')->user());
        window.employee = 1;
    </script>
</head>
<body class="vertical-layout vertical-menu 2-columns fixed-navbar  menu-expanded pace-done @yield('body-class')">
<div id="app">
    <top-header></top-header>
    <side-navbar></side-navbar>
    <app></app>
</div>
@yield('js')
<script src="{{ url(mix('/js/employee-app.js')) }}"></script>
<script src="{{ asset('app-assets/js/core/app-menu.js') }}" type="text/javascript"></script>

</body>
</html>
