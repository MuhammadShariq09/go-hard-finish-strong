<?php

return [
    'challenge_sent' => env("NOTIFICATION_CHALLENGE_SENT", false),
    'challenge_accepted' => env("NOTIFICATION_CHALLENGE_ACCEPTED", false),
    'challenge_rejected' => env("NOTIFICATION_CHALLENGE_REJECTED", false),
    'challenge_submitted' => env("NOTIFICATION_CHALLENGE_SUBMITTED", false),
    'challenge_voted' => env("NOTIFICATION_CHALLENGE_VOTED", false),
    'challenge_result' => env("NOTIFICATION_CHALLENGE_RESULT", false),
];