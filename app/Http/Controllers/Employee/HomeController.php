<?php

namespace App\Http\Controllers\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:employee');
    }

    public function index()
    {
        return view('employee.layouts.app');
    }
}
