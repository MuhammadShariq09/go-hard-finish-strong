<?php

namespace App\Http\Controllers\Employee\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/employee/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:employee')->except('logout');
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('employee.auth.login');
    }

    /**
     * Log the user out of the application.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout(Request $request)
    {
        $this->guard('employee')->logout();

        $request->session()->invalidate();

        return redirect(route('employee.login'), 302, [
            'Pragma' => 'no-cache',
            'Expires' => 'Fri, 01 Jan 1990 00:00:00 GMT',
            'Cache-Control' => 'no-cache, must-revalidate, no-store, max-age=0, private'
        ]);
    }

//    /**
//     * Get the failed login response instance.
//     *
//     * @param  \Illuminate\Http\Request  $request
//     * @return \Symfony\Component\HttpFoundation\Response
//     *
//     * @throws \Illuminate\Validation\ValidationException
//     */
//    protected function sendFailedLoginResponse(Request $request)
//    {
//        throw ValidationException::withMessages([
//            'password' => ['The password is invalid.'],
//        ]);
//    }
//
//    /**
//     * Validate the user login request.
//     *
//     * @param  \Illuminate\Http\Request  $request
//     * @return void
//     *
//     * @throws \Illuminate\Validation\ValidationException
//     */
//    protected function validateLogin(Request $request)
//    {
//        $request->validate([
//            $this->username() => 'required|string|exists:admins,email',
//            'password' => 'required|string',
//        ]);
//    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('employee');
    }
}
