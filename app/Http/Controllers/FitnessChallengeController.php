<?php

namespace App\Http\Controllers;

use App\Models\FitnessChallenge;
use Illuminate\Http\Request;

class FitnessChallengeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FitnessChallenge  $fitnessChallenge
     * @return \Illuminate\Http\Response
     */
    public function show(FitnessChallenge $fitnessChallenge)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FitnessChallenge  $fitnessChallenge
     * @return \Illuminate\Http\Response
     */
    public function edit(FitnessChallenge $fitnessChallenge)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FitnessChallenge  $fitnessChallenge
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FitnessChallenge $fitnessChallenge)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FitnessChallenge  $fitnessChallenge
     * @return \Illuminate\Http\Response
     */
    public function destroy(FitnessChallenge $fitnessChallenge)
    {
        //
    }
}
