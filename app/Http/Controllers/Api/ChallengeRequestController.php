<?php

namespace App\Http\Controllers\Api;

use App\Core\Filters\ChallengesFilters;
use App\Core\Status;
use App\Events\ChallengeDeclaredResult;
use App\Events\ChallengeLeveraged;
use App\Events\ChallengeMoveIntoVoting;
use App\Events\ChallengeRequestAccepted;
use App\Events\ChallengeRequestSent;
use App\Events\ChallengeSubmitted;
use App\Http\Resources\Invitation;
use App\Models\Challenge;
use App\Models\ChallengeRequest;
use App\Notifications\ChallengeLost;
use App\Notifications\ChallengeWon;
use App\Pipelines\Challenges\CanAcceptRequestAfterSeventyTwoHours;
use App\Pipelines\Challenges\HasLimitWithinSeventyTwoHours;
use App\Pipelines\Challenges\NoLimitToAcceptRequest;
use App\Pipelines\Challenges\ShouldNotBeSender;
use App\User;
use Hootlex\Friendships\Models\Friendship;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Pipeline\Pipeline;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

class ChallengeRequestController extends ApiBaseController
{

    public function __construct()
    {
        $this->middleware('bindings');
    }

    public function request(Request $request)
    {
        $request->validate([
            'recipient_id' => 'required|exists:users,id',
            'challenge_id' => 'required|exists:challenges,id',
        ]);

        /** @var User $user*/
        $user = auth()->user();
        /** @var User $recipient*/
        $recipient = User::find($request->recipient_id);

        if( $user->countActiveFriends() < setting('minimum_friends_for_challenge', 5) )
            return $this->responseWithError("Not Enough Friends to Send Request", ['friends' => ['You must have at least ' . setting('minimum_friends_for_challenge', 5) . ' friends to send invitations']]);

        if( $recipient->countActiveFriends() < setting('minimum_friends_for_challenge', 5) )
            return $this->responseWithError("$recipient->name doesn't have enough Friends to Send Request", ['friends' => ['You can’t initiate challenge with him']]);

        if($request->user()->id == $request->recipient_id)
            return $this->responseWithError(__('errors.unauthorized'), ['request' => [__('errors.recipient_id_should_not_be_same')]]);

        if(!$user->canSendChallengeRequest())
            return $this->responseWithError(__('errors.unauthorized'), ['request' => [__('errors.not_enough_initial_request_to_send_challenge_request')]]);

        // Check if user's are played this challenge before
        // if so you wont allow them to re initiated
        $challengeExists = ChallengeRequest::query()->where('challenge_id', $request->challenge_id)->where(function(Builder $q){
            $q->where('sender_id', auth()->id())->orWhere('recipient_id', auth()->id());
        })->where(function(Builder $q){
            $q->where('sender_id', request('recipient_id'))->orWhere('recipient_id', request('recipient_id'));
        })->count();

        if($challengeExists > 0)
            return $this->responseWithError(__('errors.unauthorized'), ['request' => ["The challenge is already initiated."]]);

        $recipient = User::find($request->recipient_id);

        $challenge = Challenge::find($request->challenge_id);

        $challengeRequest = $request->user()->sendChallengeRequest($recipient, $challenge);

        return response()->json($challengeRequest);
    }

    public function currentUserChallengeRequests(Request $request)
    {
        $challengeRequest = ChallengeRequest::with('sender')->whereRecipientId(auth()->id())->whereStatus(Status::PENDING)->paginate();
        return response()->json($challengeRequest);
    }

    public function accept(Request $request)
    {
        $request->validate([
            'request_id' => 'required|exists:challenge_requests,id',
        ]);

        /*$validated = validate_input([
            'request_id' => 'required|exists:challenge_requests,id',
        ]);

        if(!$validated)
            $this->responseWithError(['message']);*/

        $pipes = [
            CanAcceptRequestAfterSeventyTwoHours::class,
            ShouldNotBeSender::class,
            NoLimitToAcceptRequest::class
        ];

//        try {
        /** @var ChallengeRequest $challengeRequest */
        $challengeRequest = ChallengeRequest::find($request->request_id);
        /*event(new ChallengeRequestAccepted($challengeRequest));
        return $challengeRequest;*/

//
//            app(Pipeline::class)
//                ->send(['challengeRequest' => $challengeRequest])
//                ->through($pipes)
//                ->then(function ($content) {
//
//                });
//        }catch (\Exception $e){
//            return $this->responseWithError($e->getMessage(), ["fields" => [$e->getMessage()]]);
//        }

        /** @var User $user */
        $user = auth()->user();

        if( $user->getFriendsCount() < setting('minimum_friends_for_challenge', 5) )
            return $this->responseWithError("Not Enough Friends to Send Request", ['friends' => ['You must have at least ' . setting('minimum_friends_for_challenge', 5) . ' friends to accept this invitation']]);


        if($user->id == $challengeRequest->sender_id)
            return $this->responseWithError(__('errors.unauthorized'), ['request' => [__('errors.sender_id_should_not_be_same')]]);

        if($user->id != $challengeRequest->recipient_id)
            return $this->responseWithError(__('errors.unauthorized'), ['request' => [__('errors.unauthorized_to_accept_challenge_request')]]);

        if(!$user->canAcceptChallengeRequest())
            return $this->responseWithError(__('errors.unauthorized'), ['request' => [__('errors.not_enough_accept_request_to_accept_challenge_request')]]);


        /** check if he is reached to their limit to accept challenge in 72 hours */
        $totalChallengeAccepted = ChallengeRequest::where('recipient_id', $user->id)
            ->where('status', Status::ACCEPTED)
            ->where("created_at", ">", now()->modify('-72 hours')->toDateTimeString())->count();
        if($totalChallengeAccepted >= setting('max_no_challenge_accepted_in_72_hr'))
            return $this->responseWithError(__('errors.unauthorized'), ['request' => [__("errors.reached_to_accept_challenge_request_limit")]]);

        /** check if he is passes the time limit to accept next challenge request */
        $timeLimit = ChallengeRequest::where('recipient_id', $user->id)
            ->whereNotNull('challenge_accepted_at')
            ->latest()->first();

        if($timeLimit && now() < $timeLimit->challenge_accepted_at->addHours(setting('time_to_accept_a_challenge')))
            return $this->responseWithError(__('errors.unauthorized'), ['request' => [ "You have reached limit to accept challenge request in " . setting('time_to_accept_a_challenge') . " hour." ]]);

        $challengeRequest->mark_as_accepted();

        return response()->json($challengeRequest);
    }

    public function reject(Request $request)
    {
        $request->validate(['request_id' => 'required|exists:challenge_requests,id']);

        $challengeRequest = ChallengeRequest::find($request->request_id);

        if($challengeRequest->status != Status::PENDING)
            return $this->responseWithError(__('errors.unauthorized'), ['request' => [__('errors.request_is_already_accepted')]]);

        if($request->user()->id == $challengeRequest->sender_id)
            return $this->responseWithError(__('errors.unauthorized'), ['request' => [__('errors.sender_id_should_not_be_same')]]);

        if($request->user()->id != $challengeRequest->recipient_id)
            return $this->responseWithError(__('errors.unauthorized'), ['request' => [__('errors.unauthorized_to_accept_challenge_request')]]);

        $challengeRequest->mark_as_rejected();

        return response()->json($challengeRequest);
    }

    public function declareResult(Request $request)
    {
        $request->validate(['request_id' => 'required|exists:challenge_requests,id', 'result' => 'required|in:'. Status::DEFEATED .',' . Status::WINNER]);

        $challengeRequest = ChallengeRequest::find($request->request_id);

        if($challengeRequest->status == Status::COMPLETED)
            return $this->responseWithError(__('errors.unauthorized'), ['request' => [__('errors.request_is_already_completed')]]);

        if(!in_array($request->user()->id, [$challengeRequest->sender_id, $challengeRequest->recipient_id]))
            return $this->responseWithError(__('errors.unauthorized'), ['request' => [__('errors.unauthorized')]]);

        if($request->result == Status::DEFEATED)
        {
            return $this->declareDefeated($challengeRequest);
        }

        return $this->declareWinner($challengeRequest, $request);
    }

    function invitations(ChallengesFilters $filters)
    {
        /** @var User $user */
        $user = auth()->user();

        if(in_array(request('status'), [1, 4])){
            ChallengeRequest::whereRaw("$user->id IN (recipient_id)")->whereIn('status', [4, 1])->update(['recipient_seen' => true]);
            ChallengeRequest::whereRaw("$user->id IN (sender_id)")->whereIn('status', [4, 1])->update(['sender_seen' => true]);
        }
        else{
            ChallengeRequest::whereRaw("$user->id IN (recipient_id)")->where('status', request('status'))->update(['recipient_seen' => true]);
            ChallengeRequest::whereRaw("$user->id IN (sender_id)")->where('status', request('status'))->update(['sender_seen' => true]);
        }

        return $user->getChallenges()->latest()->filter($filters)->get()->map(function ($ch){ return $ch->prepare(); });
    }

    function submitVideo(Request $request)
    {
        $request->validate([
            'challenge_id' => 'required|exists:challenge_requests,id',
            'exercise_id' => 'required|exists:exercises,id',
            'video' => 'required|mimetypes:video/quicktime,video/mpeg,video/mp4'
        ]);

        $path = $request->video->store('public/videos/exercises');

        if($_FILES['video']['type'] == 'video/quicktime')
        {
            $path = storage_path('app/' . $path);
            shell_exec("ffmpeg -i $path -vcodec h264 -acodec aac -strict -2 " . storage_path('app/public/videos/exercises/' . pathinfo($path, PATHINFO_FILENAME)) . ".mp4");
        }

        //dump(asset('storage/videos/exercises/' . pathinfo($path, PATHINFO_FILENAME) . '.mp4'));

        $challenge = ChallengeRequest::findOrFail($request->challenge_id);

        $challenge_data = $challenge->challenge_data;

        foreach ($challenge_data['exercises'] as $index => $exercise)
        {
            if($exercise['id'] == $request->exercise_id){
                $challenge_data['exercises'][$index]['uploadedVideos'][auth()->id()] = asset('storage/videos/exercises/' . pathinfo($path, PATHINFO_FILENAME) . '.mp4');
            }
        }

        $challenge->challenge_data = $challenge_data;

        $challenge->save();

        return $challenge;
    }

    function submitChallenge(Request $request)
    {
        $request->validate([
            'challenge_id' => 'required|exists:challenge_requests,id'
        ]);

        $challenge = ChallengeRequest::findOrFail($request->challenge_id);

        if(auth()->id() == $challenge->sender_id)
        {
            $challenge->sender_submitted = true;
            $challenge->update(['recipient_seen' => false]);
        }

        if(auth()->id() == $challenge->recipient_id)
        {
            $challenge->recipient_submitted = true;
            $challenge->update(['sender_seen' => false]);
        }

        if($challenge->recipient_submitted == $challenge->sender_submitted) {
            $challenge->status = Status::SUBMITTED;
//            $challenge->update(['recipient_seen' => false, 'sender_seen' => false]);
        }

        $challenge->save();

        event(new ChallengeSubmitted($challenge));

        $challenge->challenge = $challenge->challenge_data;

        unset($challenge->challenge_data);

        return $challenge;
    }

    public function leveragePointsPost(Request $request)
    {
        $request->validate(['challenge_id' => 'required|exists:challenge_requests,id']);

        $challenge = ChallengeRequest::findOrFail($request->challenge_id);

        if(auth()->id() == $challenge->sender_id)
            $challenge->leverage_user_id = $challenge->recipient_id;

        if(auth()->id() == $challenge->recipient_id)
            $challenge->leverage_user_id = $challenge->sender_id;

        $challenge->earned_leverage_points = $challenge->leverage_points;

        $challenge->status = Status::COMPLETED;

        if(auth()->id() == $challenge->sender_id)
        {
            $challenge->sender_result = Status::DEFEATED;
            $challenge->recipient_result = Status::WINNER;
        }else{
            $challenge->recipient_result = Status::DEFEATED;
            $challenge->sender_result = Status::WINNER;
        }

        $challenge->save();

        event(new ChallengeLeveraged($challenge));

        return $challenge;
    }

    private function declareDefeated(ChallengeRequest $challengeRequest)
    {
        $points = (int)$challengeRequest['challenge_data']['reward_points'];

        if(auth()->id() == $challengeRequest->sender_id)
        {
            $challengeRequest->sender_result = Status::DEFEATED;
            $challengeRequest->recipient_result = Status::WINNER;

            $senderPoints = $challengeRequest->sender_points ?? [];
            $senderPoints['challenge_points'] = $points;
            $challengeRequest->sender_points = $senderPoints;

        } else {
            $challengeRequest->recipient_result = Status::DEFEATED;
            $challengeRequest->sender_result = Status::WINNER;

            $challengeRequest->recipient_points = [];

            $recipientPoints = $challengeRequest->sender_points ?? [];
            $recipientPoints['challenge_points'] = $points;
            $challengeRequest->recipient_points = $recipientPoints;
        }

        $challengeRequest->status = Status::COMPLETED;

        $challengeRequest->save();

        event(new ChallengeDeclaredResult($challengeRequest));

            try{
//                $challengeRequest->recipient->notify(new ChallengeWon($challengeRequest->challenge->title));
                $challengeRequest->recipient->notify(
                    auth()->id() == $challengeRequest->sender_id
                        ? new ChallengeWon($challengeRequest->challenge->title)
                        : new ChallengeLost($challengeRequest->challenge->title)
                );
            }catch(\Exception $exception){
                Log::critical("Challenge Declare Notification not working on recipient email: " . $exception->getMessage());
            }
            try{
                $challengeRequest->sender->notify(
                    auth()->id() == $challengeRequest->sender_id
                    ? new ChallengeLost($challengeRequest->challenge->title)
                    : new ChallengeWon($challengeRequest->challenge->title)
                );
            }catch(\Exception $exception){
                Log::critical("Challenge Declare Notification not working on sender email: " . $exception->getMessage());
            }

        return response()->json($challengeRequest);
    }

    private function declareWinner(ChallengeRequest $challengeRequest, Request $request)
    {
        if(auth()->id() == $challengeRequest->sender_id)
        {
            $challengeRequest->sender_result = Status::WINNER;
        }

        if(auth()->id() == $challengeRequest->recipient_id)
        {
            $challengeRequest->recipient_result = Status::WINNER;
        }

        if($challengeRequest->sender_result === Status::WINNER && $challengeRequest->recipient_result === Status::WINNER) {
            $challengeRequest->status = Status::IN_VOTING;
            $challengeRequest->voting_expired_at = now()->addHours(setting('voting_time_for_a_challenge'))->toDateTimeString();
            event(new ChallengeMoveIntoVoting($challengeRequest));
        }

        if($challengeRequest->sender_result !== $challengeRequest->recipient_result && $challengeRequest->recipient_result && $challengeRequest->sender_result)
            $challengeRequest->status = Status::COMPLETED;

        $challengeRequest->save();

        event(new ChallengeDeclaredResult($challengeRequest));

        return response()->json($challengeRequest);
    }

    public function votings(ChallengeRequest $challengeRequest)
    {
        $challengeRequest->prepare();
        $challengeRequest->load('votes');
        return response()->json($challengeRequest);
    }

    public function show(ChallengeRequest $challengeRequest)
    {
        $challengeRequest->load(['sender.state', 'recipient.state']);
        return $challengeRequest->prepare();
    }

}
