<?php

namespace App\Http\Controllers\Api;

use App\Core\Filters\ChallengesFilters;
use App\Http\Resources\ChallengeCollection;
use App\Http\Resources\ChallengeResource;
use App\Models\Challenge;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ChallengesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param ChallengesFilters $filters
     * @return AnonymousResourceCollection
     */
    public function index(ChallengesFilters $filters)
    {
         return ChallengeResource::collection(Challenge::filter($filters)->latest()->with('exercises')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
