<?php

namespace App\Http\Controllers\Api\Employee;

use App\Core\Filters\FitnessChallengeFilter;
use App\Core\Status;
use \App\Http\Controllers\Api\Administrator\FitnessChallengeController as BaseFitnessChallengeController;
use App\Models\FitnessChallenge;

class FitnessChallengeController extends BaseFitnessChallengeController
{
    public function index(FitnessChallengeFilter $filter)
    {
        return FitnessChallenge::filter($filter)
            ->latest()
            ->withCount('phases')
            ->withCount(['subscribers' => function($builder){ $builder->where('user_fitness_challenges.status', Status::ACCEPTED); }])
            ->with('phases:challenge_id,started_at')
            ->paginate(setting('default_pagination_length', request('per_page', 10)));
    }
}
