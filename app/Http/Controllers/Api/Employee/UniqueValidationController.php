<?php

namespace App\Http\Controllers\Api\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class UniqueValidationController extends Controller
{
    public function check(Request $request)
    {
        $field = $request->field;

        $model = $request->model;

        $unique = Rule::unique(Str::plural($model), $field);

        if($request->ignore){
            $unique = $unique->ignore($request->ignore);
        }

        $request->validate([
            $field => ['required', $unique]
        ]);

        return ['email is not registered yet.'];
    }
}
