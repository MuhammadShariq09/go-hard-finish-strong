<?php

namespace App\Http\Controllers\Api\Employee;

use App\Http\Controllers\Api\ApiBaseController;
use App\Models\Employee;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends ApiBaseController
{
    public function register (Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $request['password']= Hash::make($request['password']);

        $user = Employee::create($request->toArray());

        $user->addState();

        $token = $user->createToken('Laravel Password Grant Client')->accessToken;

        $response = ['token' => $token];

        return response()->json($response, 200);
    }

    public function login (Request $request)
    {
        $request->validate([
            'email' => 'required|email|max:255|exists:admins',
            'password' => 'required',
        ]);

        $user = Employee::where('email', $request->email)->first();

        if (Hash::check($request->password, $user->password)) {
            $user->save();
            $token = $user->createToken('Laravel Password Grant Client')->accessToken;
            $response = ['token' => $token];
            return response($response);
        }

        return $this->responseWithError('Invalid Credentials', [ 'password' => ['Password mismatch'] ]);
    }

    public function logout (Request $request)
    {
        $request->user()->update(['device_id' => null, 'device_type' => null]);
        $token = $request->user()->token();
        $token->revoke();
        $response = 'You have been successfully logged out!';
        return response($response, 200);
    }

    public function profilePut(Request $request)
    {
        /** @var User $user */
        $user = auth()->user();

        $this->bcryptPassword($request);

        $user->fill($request->only($user->getFillable()));

        $user->save();

        return response()->json(new \App\Http\Resources\User($user));
    }

    private function bcryptPassword(Request $request)
    {
        if ($request->filled('password')) {
            return $request->merge(['password' => bcrypt($request->password)]);
        }

        unset($request['password']);
        return false;
    }

    public function passwordPut(Request $request){
        $request->validate([
            'current_password' => 'required',
            'password' => 'required|confirmed'
        ]);

        if(Hash::check($request->current_password, auth()->user()->getAuthPassword()))
        {
            /** @var User $user */
            $user = auth()->user();

            $this->bcryptPassword($request);

            $user->fill($request->only($user->getFillable()));

            $user->save();

            return $user;
        }

        return $this->responseWithError('Invalid Password', ['password' => ["Invalid password."]]);

    }

}
