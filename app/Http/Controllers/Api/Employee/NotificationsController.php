<?php

namespace App\Http\Controllers\Api\Employee;

use App\Http\Controllers\Api\Administrator\NotificationsController as BaseNotificationsController;
use App\User;

class NotificationsController extends BaseNotificationsController
{
    protected function getUser(){
        /** @var User $user */
        $user = auth('api_employee')->user();
        return $user;
    }
}
