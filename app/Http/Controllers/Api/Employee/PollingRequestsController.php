<?php

namespace App\Http\Controllers\Api\Employee;

use App\Core\Filters\PollingFilters;
use App\Core\Status;
use App\Events\ChallengeDeclaredResult;
use App\Http\Resources\PollingChallenge;
use App\Http\Resources\PoolingSingleChallenge;
use App\Models\ChallengeRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\Administrator\PollingRequestsController as BasePollingRequestController;

class PollingRequestsController extends BasePollingRequestController
{
}
