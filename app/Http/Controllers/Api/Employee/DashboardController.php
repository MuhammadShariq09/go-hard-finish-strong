<?php

namespace App\Http\Controllers\Api\Employee;

use App\Core\Status;
use App\Models\Challenge;
use App\Models\ChallengeRequest;
use App\Models\FitnessChallenge;
use App\Models\State;
use App\Models\Transaction;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\Administrator\DashboardController as AdminDashboardController;

class DashboardController extends AdminDashboardController
{
    function index()
    {
        $years = \DB::select("select distinct year(created_at) as year from " . (new FitnessChallenge)->getTable());

        $fitnessChallenge = FitnessChallenge::query()->select('created_at')
            ->where('created_at','>', now()->subYear()->endOfMonth()->toDateTimeString())
            ->get()
            ->groupBy(function($item) {
                return $item->created_at->month;
            });

        $fitnessChallengeChart = collect([]);

        for ($month = 1; $month <= 12; $month++)
        {
            if($fitnessChallenge->has($month))
                $fitnessChallengeChart->push($fitnessChallenge->get($month)->count());
            else
                $fitnessChallengeChart->push(0);
        }

        return [
            'total_users' => User::count(),
            'competitions_count' => FitnessChallenge::count(),
            'chart_data' => $fitnessChallengeChart,
            'available_years' => collect($years)->map(function($q){ return $q -> year; })->toArray(),
            'recent_challenges' => FitnessChallenge::latest()
                    ->withCount('phases')
                    ->withCount(['subscribers' => function($builder){ $builder->where('user_fitness_challenges.status', Status::ACCEPTED); }])
                    ->with('phases:challenge_id,started_at')
                    ->limit(10)->get(),
            'challenge_states' => $this->getChallengeStates(),
        ];
    }

    public function getChallengeStates($year = false)
    {
        return [
            'ftfp' => $this->getFtfpChallengesStates($year),
            'trivia' => $this->getTriviaChallengesStates($year),
            'fitness' => $this->getFitnessChallengesStates($year),
        ];
    }


    protected function getFtfpChallengesStates($year = false)
    {
        if(!$year) $year = date('Y');

        $users = \DB::select('select year(created_at) as year, month(created_at) as month, count(id) as user_count from challenges where  year(created_at) = ' . $year . ' group by year(created_at), month(created_at)');

        return $this->getStates($users);
    }


    protected function getTriviaChallengesStates($year = false)
    {
        if(!$year) $year = date('Y');

        $users = \DB::select('select year(created_at) as year, month(created_at) as month, count(id) as user_count from trivia_challenges where  year(created_at) = ' . $year . ' group by year(created_at), month(created_at)');

        return $this->getStates($users);
    }


    protected function getFitnessChallengesStates($year = false)
    {
        if(!$year) $year = date('Y');

        $users = \DB::select('select year(created_at) as year, month(created_at) as month, count(id) as user_count from fitness_challenges where  year(created_at) = ' . $year . ' group by year(created_at), month(created_at)');

        return $this->getStates($users);
    }

    protected function getStates($records)
    {
        $users = collect($records);

        $data = collect([]);

        $months = ['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

        for ($i = 1; $i <= 12; $i++){
            $std = new \stdClass();
            $std->name = $months[$i];
            $std->value = $users->where('month', $i)->sum('user_count');
            $data->push($std);
        }

        return $data;
    }
}
