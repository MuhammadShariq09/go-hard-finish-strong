<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LeaderboardController extends Controller
{
    function index()
    {
        /** @var User $user */
        $user = auth()->user();

        $users = User::join('states', 'users.id', '=', 'states.user_id')
            ->orderBy('states.points', 'DESC')
            ->select('users.id','users.name','users.email','states.points','users.name','users.description','users.image')->paginate(25);

        return [
            'my_rank' => $user->myRank(),
            'leader_board' => $users
        ];
    }
}
