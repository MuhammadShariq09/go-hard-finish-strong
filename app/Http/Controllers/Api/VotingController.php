<?php

namespace App\Http\Controllers\Api;

use App\Core\Status;
use App\Http\Controllers\Controller;
use App\Http\Resources\Invitation;
use App\Models\ChallengeRequest;
use App\User;
use Hootlex\Friendships\Models\Friendship;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class VotingController extends ApiBaseController
{
    function index(Request $request)
    {
        $ids = auth()->user()->getFriendsIds();

        $challenge = ChallengeRequest::whereStatus(Status::IN_VOTING)
            ->where(function($q) use($ids){
                $q->orWhereIn('sender_id', $ids);
                $q->orWhereIn('recipient_id', $ids);
            })
            ->where('sender_id', '!=', auth()->id())
            ->where('recipient_id', '!=', auth()->id())
            ->with('sender.state', 'recipient.state')
            ->paginate(setting('pagination_length', 25));

        $challenge->each(function($c){ return $c->prepare(); });

        auth()->user()->state->update(['unseen_voting_request_count' => 0]);

        return $challenge;
    }

    function store(Request $request)
    {
        $request->validate([
            'challenge_id' => 'required|exists:challenge_requests,id',
            'user_id' => 'required|exists:users,id'
        ]);

        // Log::debug(auth()->user()->votings()->whereMonth('created_at', date('d'))->whereYear('created_at', date('Y'))->count());

        /** @var User $user */
        $user = auth()->user();

        if($user->votings()->whereMonth('created_at', date('d'))->whereYear('created_at', date('Y'))->count() >= setting('allowed_votes_monthly')){
            return $this->responseWithError("You have reached to your monthly voting limit.", ['message' => 'You have reached to your monthly voting limit.']);
        }

        //$user->state->increment('ftfp_points', setting('points_earned_per_vote',0));

        $challenge = ChallengeRequest::findOrFail($request->challenge_id);

        $user->incrementBonusPoints($challenge);

        $user->incrementLevel();

        return auth()->user()->addVote($challenge, $request->user_id);
    }
}
