<?php

namespace App\Http\Controllers\Api\Administrator;

use App\Core\Filters\PaymentLogsFilters;
use App\Http\Resources\TransactionCollection;
use App\Models\Transaction;
use App\Models\TriviaChallenge;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class PaymentLogsController extends Controller
{

    public function __construct()
    {
        $this->middleware('bindings');
    }

    /**
     * Display a listing of the resource.
     *
     * @return TransactionCollection
     */
    public function index(PaymentLogsFilters $filters)
    {
        /*DB::listen(function($q){
            dump($q->sql, $q->bindings);
        });*/


        $collect = Transaction::with('user', 'source')
            ->latest()
            ->filter($filters)
            ->paginate(request('per_page', 10));
        $collect = new TransactionCollection($collect);
        $balance = Transaction::sum('amount');

        try {
            $graph = (new Transaction())->aggregate('sum')->getState();
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()]);
        }

        return $collect->additional(['balance' => $balance, 'graph' => $graph]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Transaction $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        $transaction->delete();
        return response()->json(['message' => 'Transaction has been delete!']);
    }
}
