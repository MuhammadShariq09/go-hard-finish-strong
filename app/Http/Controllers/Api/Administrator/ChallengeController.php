<?php

namespace App\Http\Controllers\Api\Administrator;

use App\Core\Filters\ChallengesFilters;
use App\Core\Status;
use App\Http\Controllers\Api\ApiBaseController;
use App\Http\Resources\ChallengeCollection;
use App\Http\Resources\Invitation;
use App\Models\Challenge;
use App\Models\ChallengeRequest;
use App\Models\Exercise;
use Illuminate\Http\Request;
use App\Http\Resources\ChallengeResource;
use Illuminate\Http\Response;

class ChallengeController extends ApiBaseController
{
    function __construct()
    {
         return $this->middleware('bindings');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request, ChallengesFilters $filters)
    {
        return ChallengeResource::collection(Challenge::filter($filters)->latest()->with('exercises')->paginate($request->per_page));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return ChallengeResource
     */
    public function store(Request $request, Challenge $challenge)
    {
        $challenge->fill($request->only($challenge->getFillable()));
        $challenge->duration = setting('time_to_complete_a_challenge');
        auth()->user()->challenges()->save($challenge);
        $challenge->addExercises($request->exercises);
        return new ChallengeResource($challenge);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Challenge  $challenge
     * @return ChallengeResource
     */
    public function show(Request $request, $challengeId)
    {
        if($request->challengeRequest) {
            $challenge = ChallengeRequest::find($challengeId);
            return $challenge->challenge_data;
        }
        else
            $challenge = Challenge::find($challengeId);

        $challenge->load('exercises.category');
        return new ChallengeResource($challenge);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Challenge  $challenge
     * @return ChallengeResource
     */
    public function update(Request $request, Challenge $challenge)
    {
        $challenge->fill($request->all());
        $challenge->save();
        $challenge->addExercises($request->exercises);
        return new ChallengeResource($challenge);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Challenge  $challenge
     * @return \Illuminate\Http\Response
     */
    public function destroy(Challenge $challenge)
    {
        $challenge->delete();
        return $this->response(['message' => 'Challenges Deleted']);
    }

    function activeChallenges()
    {
        return response()->json(Invitation::collection(ChallengeRequest::whereStatus(Status::ACCEPTED)->with('sender', 'recipient')->latest()->take(4)->get()));
    }

    function deleteExercise(Challenge $challenge, Exercise $exercise)
    {
        $exercise->delete();
        return $this->response(['message' => 'Exercise delete successfully'], Response::HTTP_NO_CONTENT);
    }
}
