<?php

namespace App\Http\Controllers\Api\Administrator;

use App\Core\Status;
use App\Models\Challenge;
use App\Models\ChallengeRequest;
use App\Models\State;
use App\Models\Transaction;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    function index()
    {
        $years = \DB::select("select distinct year(created_at) as year from " . (new User)->getTable());

        $users = User::query()->select('created_at')->where('created_at','>', now()->subYear()->endOfMonth()->toDateTimeString())->get()
            ->groupBy(function($item) {
                return $item->created_at->month;
            });

        $userChart = collect([]);

        for ($month = 1; $month <= 12; $month++)
        {
            if($users->has($month))
                $userChart->push($users->get($month)->count());
            else
                $userChart->push(0);
        }

        return [
            'total_users' => User::count(),
            'recent_users' => \App\Http\Resources\User::collection(User::with('state')->latest()->limit(5)->get()),
            'active_challenges' => Challenge::count(),
            'income' => Transaction::sum('amount'),
            'lead_boards' => \App\Http\Resources\User::collection(User::whereIn('id', State::select('user_id')->orderBy('points', 'desc')->limit(3)->get())->get()),
            'boards' => \App\Http\Resources\User::collection(User::inRandomOrder()->limit(25)->paginate()),
            'users_chart_data' => $userChart,
            'available_years' => collect($years)->map(function($q){ return $q -> year; })->toArray(),
            'users_states' => $this->getUserStates(),
        ];
    }


    public function getUserStates($year = false)
    {
        if(!$year) $year = date('Y');

        $users = \DB::select('select year(created_at) as year, month(created_at) as month, count(id) as user_count from users where  year(created_at) = ' . $year . ' group by year(created_at), month(created_at)');

        $users = collect($users);

        $data = collect([]);

        $months = ['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

        for ($i = 1; $i <= 12; $i++){
            $std = new \stdClass();
            $std->name = $months[$i];
            $std->value = $users->where('month', $i)->sum('user_count');
            $data->push($std);
        }

        return $data;
    }
}
