<?php

namespace App\Http\Controllers\Api\Administrator;

use App\Events\FitnessPhasePublished;
use App\Http\Controllers\Api\ApiBaseController;
use App\Models\FitnessChallenge;
use App\Models\FitnessChallengePhase;
use App\Models\UserFitnessChallenge;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class EmployeeController extends ApiBaseController
{
    public function __construct()
    {
        $this->middleware('bindings');
    }

    public function post_result(Request $request, FitnessChallenge $challenge)
    {
        $request->validate([
            "user_id" => 'required',
            "reps" => 'required|numeric',
            "time" => "required|date_format:H:i:s",
            "phase_id" => "required|numeric|exists:fitness_challenge_phases,id"
        ]);

        $userChallenge = UserFitnessChallenge::query()->where('challenge_id', $challenge->id)->where('user_id', $request->user_id)->first();

        $userChallenge->update([
            'phase_' . $challenge->current_phase . '_complete_time' => $request->time,
            'phase_' . $challenge->current_phase . '_reps' => $request->reps
        ]);

        $phase_data = $userChallenge->phases_data;

        foreach($phase_data as $key => $phase)
        {
            if($phase['id'] === (int)$request->phase_id)
            {
                $phase_data[$key]['complete_time'] = $request->time;
                $phase_data[$key]['user_reps'] = $request->reps;
                $phase_data[$key]['result_submitted'] = true;
                $phase_data[$key]['total_participants'] = $userChallenge->{"ph{$challenge->current_phase}_qualifier_count"};
            }
        }

        $userChallenge->phases_data = $phase_data;

        $userChallenge->save();

        return $this->response(['message' => 'Result saved successfully.']);
    }

    public function post_phase(Request $request, FitnessChallenge $challenge)
    {
        DB::transaction(function() use($request, $challenge){
            $request->validate(["phase_id" => "required|exists:fitness_challenge_phases,id"]);

            /** @var FitnessChallengePhase $phase*/
            $phase = FitnessChallengePhase::find($request->phase_id);

            $challenge->{"phase_{$challenge->current_phase}_submitted"} = true;

            $challenge->save();

            $ufc = new UserFitnessChallenge();

            $phase->update(['status' => FitnessChallengePhase::PHASE_POSTED]);

            $query = "UPDATE {$ufc->getTable()} SET phase_{$challenge->current_phase}_rank = ( CASE id ";
            $ids = [];
            $ranks = "";
            foreach($request->data as $d)
            {
                $rank = $d["phase_{$challenge->current_phase}_rank"];
                $userChallengeId = $d['user_challenge_id'];
                $ranks .= ",{$userChallengeId},{$rank}";
                array_push($ids, $userChallengeId);
                $query .= " WHEN ? THEN ? ";
            }

            $ranks = substr($ranks, 1);

            $query .= " END) WHERE id IN(". substr(str_repeat("?,", count($ids)), 0, -1) .")";

            $params = array_merge(explode(',', $ranks), $ids);

            DB::select($query, $params);
        });

        return $this->response(['message' => 'Phase submitted successfully.']);
    }

    public function publish_phase_result(Request $request, FitnessChallenge $challenge)
    {
        DB::transaction(function() use($request, $challenge){
            // Validate the required fields
            $request->validate(["phase_id" => "required|exists:fitness_challenge_phases,id"]);

            // Fetch Phase by id
            /** @var FitnessChallengePhase $phase*/
            $phase = FitnessChallengePhase::find($request->phase_id);

            // Find index of phase
            $currentPhaseIndex = $challenge->phases->search(function($phase) {
                return $phase->id === request('phase_id');
            });

            // Find Next Phase
            $nextPhase = $challenge->phases->get($currentPhaseIndex+1);

            // check if current phase is available and not started yet then throw error
            if($nextPhase && (is_null($nextPhase->started_at) || is_null($nextPhase->ended_at)))
                return $this->responseWithError("Phase add start date in upcoming phase in order to publish the result!");

            // In other case there is no remaining phase and need to complete this phase
            $phase->update(['status' => FitnessChallengePhase::PHASE_COMPLETED]);

            // Update Current Phase to the next phase
            if($nextPhase)
                $challenge->increment('current_phase');

            event(new FitnessPhasePublished($challenge, $phase, $nextPhase));
        });

        // Finally return success response
        return $this->response(['message' => 'Result has been published successfully.']);
    }
}
