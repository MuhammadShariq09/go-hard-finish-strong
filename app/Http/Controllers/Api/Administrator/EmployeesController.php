<?php

namespace App\Http\Controllers\Api\Administrator;

use App\Core\Filters\UserFilters;
use App\Http\Resources\User as UsersCollection;
use App\Models\ChallengeRequest;
use App\Models\Employee;
use App\Notifications\UserStatusChanged;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class EmployeesController extends Controller
{
    function __construct()
    {
        return $this->middleware('bindings');
    }

    function index(Request $request, UserFilters $filters)
    {
        return Employee::filter($filters)->latest()->paginate($request->per_page ?? 10);
    }

    function store(Request $request)
    {
        $employee = new Employee();

        $data = $request->only($employee->getFillable());

        $data['password'] = bcrypt($data['password']);

        $employee->fill($data);

        $employee->save();

        return response()->json($employee);
    }

    function show(Employee $employee)
    {
        return response()->json($employee);
    }

    function update(Request $request, Employee $employee)
    {
        $this->bcryptPassword($request);

        $employee->fill($request->only($employee->getFillable()));

        $employee->save();

        return response()->json($employee);
    }

    private function bcryptPassword(Request $request)
    {
        if ($request->filled('password')) {
            return $request->merge(['password' => bcrypt($request->password)]);
        }

        unset($request['password']);
    }

    function profile(Request $request)
    {
        $user = $request->user();

        $user->load(['state', 'logs' => function($q) { $q->latest()->limit(25); }]);

        return response()->json(($user));
    }

    function statusPut(Employee $employee)
    {
        $employee->update(['status' => ! $employee->status]);
//        $employee->notify(new UserStatusChanged());
        return response()->json(['message' => 'Employee status has changed']);
    }

    function destroy(Request $request) { }
}
