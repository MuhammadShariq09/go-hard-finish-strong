<?php

namespace App\Http\Controllers\Api\Administrator;

use App\Core\Filters\FitnessChallengeFilter;
use App\Core\Status;
use App\Events\FitnessChallengeCreated;
use App\Http\Controllers\Api\ApiBaseController;
use App\Models\FitnessChallenge;
use App\Models\FitnessChallengePhase;
use App\Models\UserFitnessChallenge;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class FitnessChallengeController extends ApiBaseController
{
    public function __construct()
    {
        $this->middleware('bindings');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(FitnessChallengeFilter $filter)
    {
        return FitnessChallenge::filter($filter)
            ->latest()
            ->withCount('phases')
            ->withCount(['subscribers' => function($builder){ $builder->where('user_fitness_challenges.status', Status::ACCEPTED); }])
            ->with('phases:challenge_id,started_at')
            ->paginate(\request('per_page', 10));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'image' => 'required',
            'video' => 'required|mimes:mp4',
            'points' => 'required|numeric',
            'gender_for' => 'required|array',
            'phases' => 'required|array|max:4',
            'phases.*.title' => 'required',
            'phases.*.description' => 'required',
            'phases.*.image' => 'required',
            'phases.*.reps' => 'required|numeric',
            'phases.*.passing_male_criteria' => 'required|numeric',
            'phases.*.passing_female_criteria' => 'required|numeric',
            'phases.0.started_at' => 'required',
            'phases.0.ended_at' => 'required',
        ], [
            'phases.*.started_at.required' => 'The started at field is required',
            'phases.*.started_at.date_format' => 'The started at field must follow this format yyyy-mm-dd hh:mm:ss in 24 hours format',
            'phases.*.ended_at.required' => 'The ended at field is required',
            'phases.*.ended_at.date_format' => 'The ended at field must follow this format yyyy-mm-dd hh:mm:ss in 24 hours format',
        ]);

        if($request->fee > 0) {
            $request->validate([
                'user_limit' => 'required_if:closed,true|numeric',
                'fee' => 'numeric',
            ]);
        }

        if($request->get('isPrize') === 'true') {
            $request->validate([
                'prize' => 'numeric',
            ]);
        }

        DB::transaction(function() use($request) {
            $challenge = new FitnessChallenge();
            $challenge->fill($request->only($challenge->getFillable()));
            $challenge->type = $request->fee > 0? 'closed': 'invitation';

            $challenge->gender_for = join('-', $request->gender_for);
            $challenge->video = str_replace('public/', '', $request->video->store('public/videos/fitness'));
            $challenge->save();

            foreach($request->phases as $phase)
                $challenge->phases()->save(new FitnessChallengePhase($phase));

            event(new FitnessChallengeCreated($challenge));
        });

        return $this->response(['message' => 'Challenge created!']);
    }

    /**
     * Display the specified resource.
     *
     * @param FitnessChallenge $challenge
     * @return JsonResponse
     */
    public function show(FitnessChallenge $challenge)
    {
        $challenge->load(['winner', 'phases', 'subscribers']);

        return \response()->json($challenge);
    }

    /**
     * Update the specified resource in storage.
     * This will update only challenge details
     *
     * @param Request $request
     * @param  FitnessChallenge $challenge
     * @return JsonResponse
     */
    public function update(Request $request, FitnessChallenge $challenge)
    {
        DB::transaction(function() use($request, $challenge) {

            $data = array_filter($request->only($challenge->getFillable()), function($key){
                return !is_null($key) && $key !== 'null';
            });

            $challenge->fill($data);

            $challenge->gender_for = str_replace(',', '-', $request->gender_for);

            if($request->hasFile('video'))
                $challenge->video = str_replace('public/', '', $request->video->store('public/videos/fitness'));

            $challenge->save();
        });

        return $this->response(['message' => 'Challenge has been updated!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param FitnessChallenge $challenge
     * @return JsonResponse
     */
    public function destroy(FitnessChallenge $challenge)
    {
        $challenge->loadCount(['subscribers' => function($builder){ $builder->where('user_fitness_challenges.status', Status::ACCEPTED); }]);

        if($challenge->subscribers_count > 0){
            return $this->response(['message' => 'There are active users and cannot be delete.'], Response::HTTP_EXPECTATION_FAILED);
        }

        try {
            UserFitnessChallenge::where('challenge_id', $challenge->id)->delete();
            $challenge->delete();
        } catch (\Exception $exception) {
            return $this->response(['message' => 'Something went wrong!'], Response::HTTP_NOT_FOUND);
        }
        return $this->response(['message' => 'Challenge has been deleted!']);
    }

    /**
     * Update Phase the specified resource from storage.
     *
     * @param Request $request
     * @param FitnessChallengePhase $phase
     * @return Response
     */
    public function phase_update(Request $request, FitnessChallengePhase $phase)
    {
        if($request->phases && is_array($request->phases))
        {
            foreach($request->phases as $index => $phase)
            {
                $phase = FitnessChallengePhase::find($phase['id']);
                $phase->fill($request->phases[$index]);
                $phase->save();
            }
            return response(['message' => 'Phases has been updated successfully.']);
        }
        $phase->fill($request->only($phase->getFillable()));
        $phase->save();
        return response(['message' => 'Phase has been updated successfully.']);
    }

    /**
     * Update Phase the specified resource from storage.
     *
     * @param Request $request
     * @param FitnessChallengePhase $phase
     * @return Response
     */
    public function user_challenge_details(Request $request, $ids)
    {
        list($challengeId, $userId) = explode(',', $ids);

        return UserFitnessChallenge::with('player', 'challenge')->whereChallengeId($challengeId)->whereUserId($userId)->first();
    }

    /**
     * Update Phase the specified resource from storage.
     *
     * @param Request $request
     * @param FitnessChallengePhase $phase
     * @return Response
     */
    public function user_challenges(Request $request, $id)
    {
        return UserFitnessChallenge::with('player.state:user_id,points')
            ->orderByRaw("phase_{$request->phase}_rank IS NULL, phase_{$request->phase}_rank ASC")
            ->whereNotNull("phase_{$request->phase}_qualified")
            ->whereChallengeId($id)
            ->get();
    }

    /**
     * Update Phase the specified resource from storage.
     *
     * @param Request $request
     * @param FitnessChallenge $challenge
     * @return Response
     */
    public function invite(Request $request, FitnessChallenge $challenge)
    {
        $ignoreUserIds = $challenge->userChallenges()->whereIn('user_Id', $request->user_ids)->pluck('user_id');

        $ids = collect($request->user_ids)->filter(function($id) use($ignoreUserIds){
            return $ignoreUserIds->search($id) === false;
        });

        if(count($ids))
        {
            $request->merge(['user_ids' => $ids->implode(',')]);
            event(new FitnessChallengeCreated($challenge));
            return \response()->json(['message' => 'Invitation has been send.', 'method' => 'success']);
        }

//        \DB::listen(function($q){
//            dd($q->sql, $q->bindings);
//        });

        $challenge->userChallenges()->where('status', '0')
            ->whereIn('user_Id', $request->delete_user_ids)
            ->delete();

        return \response()->json(['message' => 'Invitation already has been send.', 'method' => 'info']);
    }
}
