<?php

namespace App\Http\Controllers\Api\Administrator;

use App\Core\Filters\PollingFilters;
use App\Core\Status;
use App\Events\ChallengeDeclaredResult;
use App\Http\Controllers\Api\ChallengeRequestController;
use App\Http\Resources\ChallengeResource;
use App\Http\Resources\PollingChallenge;
use App\Http\Resources\PoolingSingleChallenge;
use App\Models\ChallengeRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PollingRequestsController extends Controller
{
    public function __construct()
    {
        $this->middleware('bindings');
    }

    public function index(Request $request, PollingFilters $filters)
    {
        return PollingChallenge::collection(ChallengeRequest::filter($filters)->with('sender', 'recipient')->paginate());
    }

    public function show(Request $request, ChallengeRequest $challengeRequest)
    {
        $challengeRequest->load('sender', 'recipient', 'votes');
//        return $challengeRequest;
        return new PoolingSingleChallenge($challengeRequest);
    }

    public function declareResult(Request $request)
    {
        $request->validate([
            'challenge_id' => 'required',
            'winner_id' => 'required'
        ]);

        $challengeRequest = ChallengeRequest::find($request->challenge_id);

        if($challengeRequest->sender_id == $request->winner_id)
        {
            $challengeRequest->sender_result = Status::WINNER;
            $challengeRequest->recipient_result = Status::DEFEATED;
        }

        if($challengeRequest->recipient_id == $request->winner_id)
        {
            $challengeRequest->recipient_result = Status::WINNER;
            $challengeRequest->sender_result = Status::DEFEATED;
        }

        $challengeRequest->status = Status::COMPLETED;

        $challengeRequest->save();

        event(new ChallengeDeclaredResult($challengeRequest));

        return response()->json($challengeRequest);
    }
}
