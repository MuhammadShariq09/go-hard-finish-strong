<?php

namespace App\Http\Controllers\Api\Administrator;

use App\Core\Filters\FeedbackFilters;
use App\Http\Controllers\Api\ApiBaseController;
use App\Models\Feedback;
use Illuminate\Http\Request;

class FeedbackController extends ApiBaseController
{
    public function __construct()
    {
        return $this->middleware('bindings');
    }

    public function index(Request $request, FeedbackFilters $filters)
    {
        $feedback = Feedback::with('owner')->filter($filters)->latest()->paginate($request->per_page);

        return \App\Http\Resources\Feedback::collection($feedback);
    }

    public function show(Request $request, Feedback $feedback)
    {
        return new \App\Http\Resources\Feedback($feedback);
    }

    public function destroy(Request $request, Feedback $feedback)
    {
        $feedback->delete();
        return ['message' => 'Feedback has been deleted'];
    }
}
