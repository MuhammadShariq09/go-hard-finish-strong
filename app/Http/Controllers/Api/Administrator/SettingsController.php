<?php

namespace App\Http\Controllers\Api\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    function index()
    {
        return setting()->all();
    }

    function store(Request $request)
    {
        setting()->setExtraColumns(['guard' => 'admin']);
        setting($request->all())->save();
        return response()->json(['inserted success']);
    }

    function show($key)
    {
        return response()->json([$key => setting($key)]);
    }
}
