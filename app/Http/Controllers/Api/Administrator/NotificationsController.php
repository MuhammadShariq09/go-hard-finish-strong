<?php

namespace App\Http\Controllers\Api\Administrator;

use App\Http\Controllers\Api\ApiBaseController;
use App\Http\Resources\NotificationResouce;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Support\Collection;

class NotificationsController extends ApiBaseController
{
    public function index()
    {
        $user = $this->getUser();
        $notifications = $user->notifications()->where('data', 'LIKE', "%". request('search_term','') ."%")->latest()->paginate(request('per_page'));

        /** @var Collection $notifications */
        $notifications = NotificationResouce::collection($notifications);

//        $notifications->each(function ($notification){ $notification->markAsRead(); });

        return $notifications;
    }
    public function bell_notifications()
    {
        $user = $this->getUser();
        $notifcations = NotificationResouce::collection($user->unreadNotifications()->latest()->limit(10)->get());

        return $this->response([
            // 'notifications' => $user->notifications()->latest()->paginate(3),
            'notifications' => $notifcations,
            'unread_notifications_count' => $user->unreadNotifications()->count(),
        ]);
    }

    public function readNotification($notificationId)
    {
        $notification = DatabaseNotification::find($notificationId);

        $notification->markAsRead();

        return $this->response(['message' => "Notification read"]);
    }

    protected function getUser(){
        /** @var User $user */
        $user = request()->user();
        return $user;
    }
}
