<?php

namespace App\Http\Controllers\Api\Administrator;

use App\Http\Controllers\Controller;
use App\Models\Path;
use Illuminate\Http\Request;

class PathController extends Controller
{
    function index()
    {
        return Path::all();
    }

    function store(Request $request)
    {
        $paths = Path::all();
        collect($request->all())->each(function($path) use($paths){
            $dbPath = $paths->find($path['id']);
            if($dbPath->id != $path['points'])
            {
                $dbPath->points = $path['points'];
                $dbPath->save();
            }
        });
        return $request->all();
    }
}
