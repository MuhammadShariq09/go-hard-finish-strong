<?php

namespace App\Http\Controllers\Api;

use App\Notifications\ChallengeWon;
use App\User;
use Hootlex\Friendships\Models\Friendship;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class FriendshipController extends ApiBaseController
{
    public function request(Request $request)
    {
        $submitted = auth()->user()->befriend(User::find($request->recipient_id));
        $user = User::find($request->recipient_id);
        $user->state->increment('unseen_friend_request_count');
        return $submitted? response()->json($submitted): $this->responseWithError('Already sent', ['friend' => ['You already sent a request!']]);
    }

    public function accept(Request $request)
    {
        $sender = User::find($request->sender_id);

        /** @var User $user */
        $user = auth()->user();

        if(!$user->state->remaining_friends_request_accept)
            return $this->responseWithError(__('errors.unauthenticated'), ['user' => ['You have reached the limit to accept friend requests for this month.']]);

        if($user->acceptFriendRequest($sender)) {
            $user->state->decrement('remaining_friends_request_accept');
            return response()->json(new \App\Http\Resources\User($sender));
        }

        return $this->responseWithError(__('errors.unauthenticated'), ['user' => 'Unauthenticated']);
    }

    public function unfriend(Request $request)
    {
        $unfriend = auth()->user()->unfriend(User::find($request->friend_id));

        return $unfriend ? response()->json(['message' => 'Successfully removed from friends', 'status' => true])
            : $this->responseWithError(__('errors.unauthenticated'), ['user' => ['Unauthenticated']]);
    }

    public function reject(Request $request)
    {
        $sender = User::find($request->sender_id);

        if(!$sender)
            return $this->responseWithError(__('errors.unauthenticated'), ['user' => ['Unauthenticated']]);

        /** @var User $user */
        $user = auth()->user();

        $friendship = Friendship::whereRecipientId($user->id)
            ->whereSenderId($sender->id)->whereStatus(0)->first();

        if(!$friendship)
            return $this->responseWithError(__('errors.unauthenticated'), ['user' => ['Not found']], Response::HTTP_NO_CONTENT);

        if($friendship->delete())
            return response()->json(new \App\Http\Resources\User($sender));

        return $this->responseWithError(__('errors.unauthenticated'), ['user' => ['Unauthenticated']]);
    }

    public function cancel(Request $request)
    {
        $recipient = User::find($request->recipient_id);

        if(!$recipient)
            return $this->responseWithError(__('errors.unauthenticated'), ['user' => ['Unauthenticated']]);

        /** @var User $user */
        $user = auth()->user();

        $friendship = Friendship::whereSenderId($user->id)
            ->whereRecipientId($recipient->id)->whereStatus(0)->first();

        if(!$friendship)
            return $this->responseWithError(__('errors.unauthenticated'), ['user' => ['Not found']], Response::HTTP_NO_CONTENT);

        if($friendship->delete())
            return response()->json(new \App\Http\Resources\User($recipient));

        return $this->responseWithError(__('errors.unauthenticated'), ['user' => ['Unauthenticated']]);
    }

    public function myFriendRequests()
    {
        auth()->user()->state->update(['unseen_friend_request_count' => 0]);
        return auth()->user()->getFriendRequests();
    }
}
