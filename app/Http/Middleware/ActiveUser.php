<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class ActiveUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth('api')->user()->status)
            return $next($request);

        return response(['message' => "Your account has been suspended.", "errors" => ["message" => ["Your account has been suspended. please contact to admin."]]], Response::HTTP_UNAUTHORIZED);
    }
}
