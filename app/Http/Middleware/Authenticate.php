<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            if($request->route()->getPrefix() === 'admin'){
                return redirect(route('admin.login'));
            }else if($request->route()->getPrefix() === 'employee'){
                return redirect('/employee/login');
            }
            return route('login');
        }
    }
    /*protected function redirectTo($request)
    {
        // http://localhost:1000/employee
        if (! $request->expectsJson()) {
            switch (true)
            {
                case strpos($request->path(), 'admin') !== false:
                    dd($request->path(), strpos($request->path(), 'admin'));
                    return route('admin.login');
                case strpos($request->path(), 'employee') >= 0:
                    dd($request->path(), strpos($request->path(), 'employee'));
                    return route('employee.login');
                default:
                    return route('login');
            }
//            if($request->path() == 'admin')
        }
    }*/
}
