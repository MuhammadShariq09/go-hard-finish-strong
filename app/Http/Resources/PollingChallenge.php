<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PollingChallenge extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'sender' => $this->sender,
            'sender_result' => $this->sender_result,
            'recipient_result' => $this->recipient_result,
            'status' => $this->status,
            'recipient' => $this->recipient,
            'challenge' => $this->challenge_data,
            'votes' => array_key_exists('votes', $this->getRelations())? $this->votes: [],
            'created_date' => $this->created_at->format(config('app.date_format')),
            'created_at' => $this->created_at->format(config('app.datetime_format')),
            'updated_at' => $this->updated_at->format(config('app.datetime_format'))
        ];
    }
}
