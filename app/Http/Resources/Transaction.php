<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Transaction extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            "amount" => $this->amount,
            "created_at" => $this->created_at->format(config('app.date_format')),
            "description" => $this->description,
            "transaction_id" => $this->transaction_id,
            "transactor" => $this->user,
            "source" => $this->transitionable,
            "transitionable_id" => $this->transitionable_id,
            "transitionable_type" => $this->transitionable_type,
        ];
    }
}
