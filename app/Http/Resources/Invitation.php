<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Invitation extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "sender_id" => $this->sender_id,
            "recipient_id" => $this->recipient_id,
            "challenge_id" => $this->challenge_id,
            "status" => $this->status,
            "created_at" => $this->created_at->format('d/m/Y h:m:s'),
            "updated_at" => $this->updated_at->format('d/m/Y h:m:s'),
            "sender_submitted" => $this->sender_submitted,
            "recipient_submitted" => $this->recipient_submitted,
            "recipient_result" => $this->recipient_result,
            "sender_result" => $this->recipient_result,
            "voting_expired_at" => $this->voting_expired_at,
            "challenge_accepted_at" => $this->challenge_accepted_at,
            "time_to_complete" => $this->time_to_complete,
            "duration" => $this->duration,
            "leverage_points" => $this->leverage_points,
            "earned_leverage_points" => $this->earned_leverage_points,
            "leverage_user_id" => $this->leverage_user_id,
            "left_time" => $this->time_to_complete? str_replace('from now', 'left', $this->time_to_complete->diffForHumans(null, null, false, 2)): null,
            "sender" => $this->sender,
            "recipient" => new User($this->recipient),
            // "challenge" => $this->challenge_data? $this->challenge_data : [],
            "challenge" => $this->challenge_data? $this->challenge_data : [],
            "created_date" => $this->created_at->format('Y-m-d'),
        ];

//        $time = now();
//        $time2 = now()->addMinutes(10)->addHours(2);
//
//        return $time2;
    }
}
