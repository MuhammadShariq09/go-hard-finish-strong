<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'image' => $this->image,
            'email' => $this->email,
            'contact' => $this->contact,
            'description' => $this->description,
            'status' => $this->status,
            'city' => $this->city,
            'country' => $this->country,
            'address_state' => $this->address_state,
            'zipcode' => $this->postal_code,
            'postal_code' => $this->postal_code,
            'state' => $this->state,
            'gender' => $this->gender,
            'friend_status' => $this->friend_status ? $this->friend_status : new \stdClass(),
            'challenges' => $this->showWithChallenge ? $this->challenges : [],
            'has_pending_request' => $this->has_pending_request,
            'created_at' => $this->created_at->format(config('app.date_format')),
            'updated_at' => $this->updated_at->format(config('app.datetime_format')),
        ];
    }
}
