<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ExerciseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'image' => $this->image,
            'title' => $this->title,
            'description' => $this->description,
            'sets' => $this->sets,
            'repeat' => $this->repeat,
            'category' => new CategoryResource($this->category),
            'category_id' => $this->category_id,
            'challenge_id' => $this->challenge_id
        ];
    }
}
