<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class Feedback extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'subject' => Str::limit($this->subject, 50),
            'message' => $this->message,
            'short_message' => Str::limit($this->message, 50),
            'owner' => $this->owner,
            'email' => $this->email,
            'created_date' => $this->created_at->format(config('app.date_format')),
            'created_at' => $this->created_at->format(config('app.datetime_format'))
        ];
    }
}
