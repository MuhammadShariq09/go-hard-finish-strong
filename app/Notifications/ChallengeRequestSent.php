<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ChallengeRequestSent extends Notification implements ShouldQueue
{
    use Queueable;

    public $challenge;
    public $sender;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($challenge, $sender)
    {
        $this->challenge = $challenge;
        $this->sender = $sender;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->view('emails.challenge_requests_sent', ['recipient' => $notifiable, 'sender' => $this->sender, 'challenge' => $this->challenge])
                    ->subject("New Challenge");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
