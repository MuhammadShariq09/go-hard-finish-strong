<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PointLog extends Model
{
    protected $fillable = ['description', 'player_id', 'points', 'module'];
}
