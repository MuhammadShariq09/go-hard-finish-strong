<?php

namespace App\Models;

use App\Core\Traits\ImageableFill;
use App\Core\Traits\Payloggable;
use App\Core\Traits\Pointable;
use App\TriviaQuestion;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class TriviaChallenge extends Model
{
    use ImageableFill, SoftDeletes, Payloggable, Pointable;

    protected $fillable = ["title", "description", "time", "type", "image", "points", "prize", "prize_value", "image", "amount"];

//    protected $appends = ['image'];

    public $imageFolder = 'trivia/challenges';

    public function questions()
    {
        return $this->hasMany(TriviaQuestion::class, 'challenge_id');
    }

    public function saveQuestions($questions)
    {
        DB::transaction(function() use($questions){
            $ids = collect($questions)->pluck('id');

            $this->questions()->whereNotIn('id', $ids)->delete();

            foreach($questions as $question)
            {
                if(array_key_exists('id', $question))
                {
                    /** @var TriviaQuestion $questionModel */
                    $questionModel = TriviaQuestion::find($question['id']);
                    $questionModel->fill($question);
                    $questionModel->save();
                    $questionModel->saveAnswers($question['answers']);
                    continue;
                }

                $questionModel = new TriviaQuestion();
                $questionModel->fill($question);
                $this->questions()->save($questionModel);
                $questionModel->saveAnswers($question['answers']);
            }
        });
    }

    public function scopeFilter($query, $filters)
    {
        return $filters->apply($query);
    }
}
