<?php

namespace App\Models;

use App\Core\Traits\ImageableFill;
use Illuminate\Database\Eloquent\Model;

class Exercise extends Model
{
    use ImageableFill;

    protected $fillable = [ 'title', 'sets', 'repeat', 'description', 'image', 'challenge_id', 'category_id'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
