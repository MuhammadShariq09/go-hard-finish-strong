<?php

namespace App\Models;

use App\Core\Traits\ImageableFill;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Challenge extends Model
{
    use ImageableFill, SoftDeletes;

    protected $fillable = ['title', 'reward_points', 'path', 'level', 'description', 'image'];

    // public function myfill($data)
    // {
    //     try {
    //         $data['image'] = upload_base_64_image(request('image'), 'challenges');
    //     }catch (\Exception $e) {
    //         unset($data['image']);
    //     }

    //     $this->fill($data);
    // }

    public function addExercises($exercises)
    {
        $this->exercises()->whereNotIn('id', collect($exercises)->pluck('id'))->delete();

        foreach($exercises as $exercise)
        {
            if(array_key_exists('id', $exercise))
            {
                $exerciseModel = Exercise::find($exercise['id']);
                $exerciseModel->fill($exercise);
                $exerciseModel->save();
            }else{
                $exerciseModel = new Exercise();
                $exerciseModel->fill($exercise);
                $this->exercises()->save($exerciseModel);
            }
        }
    }

    public function owner()
    {
        return $this->belongsTo(\App\User::class, 'created_by');
    }

    public function exercises()
    {
        return $this->hasMany(Exercise::class);
    }

    public function scopeFilter($query, $filters)
    {
        return $filters->apply($query);
    }
}
