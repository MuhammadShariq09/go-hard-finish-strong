<?php

namespace App\Models;

use App\Core\Traits\Graphable;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Transaction extends Model
{
    use Graphable;

    protected $fillable = ['description', 'transactor', 'amount', 'transaction_id'];


    public function source() : MorphTo {
        return $this->morphTo('transitionable', 'transitionable_type', 'transitionable_id');
    }

    public function scopeFilter($query, $filters){
        return $filters->apply($query);
    }

    public function user() : BelongsTo{
        return $this->belongsTo(User::class, 'transactor');
    }

}
