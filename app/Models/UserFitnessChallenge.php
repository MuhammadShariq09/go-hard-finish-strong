<?php

namespace App\Models;

use App\Models\FitnessChallengePhase;
use App\User;
use Illuminate\Database\Eloquent\Model;

class UserFitnessChallenge extends Model
{
    protected $guarded = [];

    protected $casts = ['phases_data' => 'array'];

    public function player()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function challenge()
    {
        return $this->belongsTo(FitnessChallenge::class, 'user_id');
    }

}
