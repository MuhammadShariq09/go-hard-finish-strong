<?php

namespace App\Models;

use App\Core\Traits\Filterable;
use App\Core\Traits\ImageableFill;
use App\Notifications\SendAdminPasswordResetNotification;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class Employee extends Authenticatable
{
    use Filterable, ImageableFill, Notifiable, HasApiTokens;

    protected $fillable = [ 'name',  'email',  'email_verified_at', 'gender',  'password',  'type',  'status',  'address',  'country',  'city',  'address_state',  'postal_code',  'contact',  'image',  'device_id',  'device_type'];


    public function sendPasswordResetNotification($token)
    {
        $this->notify(new SendAdminPasswordResetNotification($token, Employee::class));
    }
}
