<?php

namespace App\Models;

use App\Core\Traits\Filterable;
use App\Core\Traits\ImageableFill;
use App\Core\Traits\Payloggable;
use App\Core\Traits\Pointable;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class FitnessChallenge extends Model
{
    use ImageableFill, Payloggable, Filterable, SoftDeletes;

    const INVITATION = 'invitation';
    const CLOSED = 'closed';

    protected $casts = ['phases_data' => 'array', 'fee' => 'integer'];

    protected $fillable = [
        'title',
        'description',
        'type',
        'current_phase',
        'status',
        'points',
        'user_limit',
        'prize',
        'fee',
        'gender_for',
        'image',
        'video',
        'phase_1_submitted',
        'phase_2_submitted',
        'phase_3_submitted',
        'phase_4_submitted',
        'phase_1_published',
        'phase_2_published',
        'phase_3_published',
        'phase_4_published',
    ];

    public function phases(): HasMany
    {
        return $this->hasMany(FitnessChallengePhase::class, 'challenge_id');
    }


    public function phasesData(): HasMany
    {
        return $this->hasMany(FitnessChallengePhase::class, 'challenge_id');
    }

    public function subscribers(): BelongsToMany
    {
        return $this->belongsToMany(User::class, UserFitnessChallenge::class, 'challenge_id')
            ->withPivot('status');
    }

    public function userChallenges(): HasMany
    {
        return $this->hasMany(UserFitnessChallenge::class, 'challenge_id');
    }

    public function winner(): BelongsTo
    {
        return $this->belongsTo(User::class, 'winner_id');
    }
}
