<?php

namespace App\Models;

use App\Core\Traits\ImageableFill;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class FitnessChallengePhase extends Model
{
    use ImageableFill;

    const PHASE_PENDING = 0;
    const PHASE_STARTED = 1;
    const PHASE_POSTED = 2;
    const PHASE_COMPLETED = 3;

    protected $fillable = ['title', 'challenge_id', 'description', 'image', 'reps', 'started_at', 'ended_at', 'passing_male_criteria', 'passing_female_criteria', 'status'];

    public function challenge() : BelongsTo
    {
        return $this->belongsTo(FitnessChallenge::class);
    }
}
