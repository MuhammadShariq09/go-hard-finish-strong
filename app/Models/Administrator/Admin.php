<?php

namespace App\Models\Administrator;

use App\Core\Traits\ImageableFill;
use App\Notifications\SendAdminPasswordResetNotification;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class Admin extends Authenticatable
{
    use Notifiable, HasApiTokens, ImageableFill;

    protected $fillable = ['name', 'email', 'password', 'image', 'address', 'country', 'city', 'address_state', 'postal_code', 'contact'];

    protected $hidden = ['password', 'remember_token'];

    public function challenges()
    {
        return $this->hasMany(\App\Models\Challenge::class, 'created_by');
    }

    public function trivia_challenges()
    {
        return $this->hasMany(\App\Models\TriviaChallenge::class, 'created_by');
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new SendAdminPasswordResetNotification($token));
    }
}
