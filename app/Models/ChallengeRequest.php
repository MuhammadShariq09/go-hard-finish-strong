<?php

namespace App\Models;

use App\Core\Status;
use App\Core\Traits\Friendable;
use App\Core\Traits\Loggable;
use App\Core\Traits\Pointable;
use App\Events\ChallengeRequestAccepted;
use App\Events\ChallengeRequestRejected;
use App\Events\ChallengeRequestSent;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Event;

class ChallengeRequest extends Model
{
    use Pointable;

    protected $table = 'challenge_requests';

    protected $guarded = ['id'];

    protected $casts = [
        'challenge_data' => 'json',
        'sender_points' => 'json',
        'recipient_points' => 'json',
        'recipient_submitted' => 'boolean',
        'sender_submitted' => 'boolean',
        'time_to_complete' => 'datetime',
        'challenge_accepted_at' => 'datetime',
    ];
    
    // protected function getChallengeDataAttribute($value){
    //     return $value;
    // }

    protected static function boot()
    {
        parent::boot();

        // Order by name ASC
        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('id', 'desc');
        });
    }

    public function sender()
    {
        return $this->belongsTo(User::class);
    }

    public function recipient()
    {
        return $this->belongsTo(User::class);
    }

    public function challenge()
    {
        return $this->belongsTo(Challenge::class);
    }

    public function votes()
    {
        return $this->hasMany(Voting::class, 'challenge_id');
    }

    public function mark_as_accepted()
    {
        $this->status = Status::ACCEPTED;

        $this->challenge_accepted_at = now()->toDateTimeString();

        $this->time_to_complete = now()->addHours(setting('time_to_complete_a_challenge'))->toDateTimeString();

        ChallengeRequest::find($this->id)->update(['sender_seen' => 0, 'recipient_seen' => 0]);

//        ALTER TABLE `challenge_requests` ADD `challenge_accepted_at` DATETIME NULL DEFAULT NULL AFTER `challenge_data`, ADD `time_to_be_completed` DATETIME NULL DEFAULT NULL AFTER `challenge_accepted_at`;

        $this->save();

        event(new ChallengeRequestAccepted($this));
    }

    public function mark_as_rejected()
    {
//        $this->status = Status::REJECTED;

        $this->delete();

        event(new ChallengeRequestRejected($this));
    }

    public function prepare(){
        $this->challenge = $this->challenge_data;
        $this->left_time = $this->time_to_complete? str_replace('from now', 'left', $this->time_to_complete->diffForHumans(null, null, true, 2)): null;
        unset($this->challenge_data);
        $this->total_points = !! $this->earned_leverage_points ? $this->earned_leverage_points: $this->challenge['reward_points'];
        return $this;
    }

    /**
     * @param Model $recipient
     * @return $this
     */
    public function fillRecipient($recipient)
    {
        return $this->fill([
            'recipient_id' => $recipient->getKey(),
            'recipient_type' => $recipient->getMorphClass()
        ]);
    }

    /**
     * @param $query
     * @param Model $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWhereRecipient($query, $model)
    {
        return $query->where('recipient_id', $model->getKey())
            ->where('recipient_type', $model->getMorphClass());
    }

    /**
     * @param $query
     * @param Model $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWhereSender($query, $model)
    {
        return $query->where('sender_id', $model->getKey())
            ->where('sender_type', $model->getMorphClass());
    }

    /**
     * @param $query
     * @param Model $model
     * @param string $groupSlug
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWhereGroup($query, $model, $groupSlug)
    {

        $groupsPivotTable   = config('friendships.tables.fr_groups_pivot');
        $friendsPivotTable  = config('friendships.tables.fr_pivot');
        $groupsAvailable = config('friendships.groups', []);

        if ('' !== $groupSlug && isset($groupsAvailable[$groupSlug])) {

            $groupId = $groupsAvailable[$groupSlug];

            $query->join($groupsPivotTable, function ($join) use ($groupsPivotTable, $friendsPivotTable, $groupId, $model) {
                $join->on($groupsPivotTable . '.friendship_id', '=', $friendsPivotTable . '.id')
                    ->where($groupsPivotTable . '.group_id', '=', $groupId)
                    ->where(function ($query) use ($groupsPivotTable, $friendsPivotTable, $model) {
                        $query->where($groupsPivotTable . '.friend_id', '!=', $model->getKey())
                            ->where($groupsPivotTable . '.friend_type', '=', $model->getMorphClass());
                    })
                    ->orWhere($groupsPivotTable . '.friend_type', '!=', $model->getMorphClass());
            });

        }

        return $query;
    }

    /**
     * @param $query
     * @param Model $sender
     * @param Model $recipient
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeBetweenModels($query, $sender, $recipient)
    {
        $query->where(function ($queryIn) use ($sender, $recipient){
            $queryIn->where(function ($q) use ($sender, $recipient) {
                $q->whereSender($sender)->whereRecipient($recipient);
            })->orWhere(function ($q) use ($sender, $recipient) {
                $q->whereSender($recipient)->whereRecipient($sender);
            });
        });
    }

    public function scopeFilter($query, $filters)
    {
        return $filters? $filters->apply($query): $query;
    }
}
