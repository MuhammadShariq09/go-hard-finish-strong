<?php

namespace App\Models;

use App\Core\Traits\Filterable;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

// this class will store user's store
class State extends Model
{
    use Filterable;

    protected $guarded = [];

    protected $casts = [
        'subscribed_fitness_invitation' => 'boolean'
    ];

    public function toggleFitnessChallengeInvitation()
    {
        $this->subscribed_fitness_invitation = !$this->subscribed_fitness_invitation;
        $this->save();
        return $this;
    }

    public function user() : BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
