<?php

namespace App\Models;

use App\Core\Traits\Pointable;
use Illuminate\Database\Eloquent\Model;

class UserTriviaChallenge extends Model
{
    use Pointable;

    protected $fillable = ['challenge_id', 'user_id', 'challenge_data', 'total_questions', 'attempted_questions', 'correct_questions', 'wrong_questions', 'earned_points', 'duration', 'paid', 'ended_at', 'completed'];

    protected $casts = ['challenge_data' => 'json'];

    protected $dates = ['ended_at'];
}
