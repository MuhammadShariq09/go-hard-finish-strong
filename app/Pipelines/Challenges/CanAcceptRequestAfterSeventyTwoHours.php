<?php

namespace App\Pipelines\Challenges;

use App\Core\Status;
use App\Models\ChallengeRequest;
use App\Pipelines\Pipe;
use Closure;

class CanAcceptRequestAfterSeventyTwoHours implements Pipe {

    /**
     * @param $content
     * @param Closure $next
     * @return mixed
     * @throws \Exception
     */
    public function handle($content, Closure $next)
    {
        $totalChallengeAccepted = ChallengeRequest::where('recipient_id', auth()->id())
            ->where('status', Status::ACCEPTED)
            ->where("created_at", ">", now()->modify('-72 hours')->toDateTimeString())->count();

        if($totalChallengeAccepted >= setting('max_no_challenge_accepted_in_72_hr'))
            throw new \Exception(__("errors.reached_to_accept_challenge_request_limit"), 451);

        return  $next($content);
    }
}