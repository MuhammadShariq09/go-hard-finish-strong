<?php

namespace App\Events;

use App\Models\FitnessChallenge;
use App\Models\FitnessChallengePhase;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class FitnessPhasePublished
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var FitnessChallenge
     */
    public $challenge;
    /**
     * @var FitnessChallengePhase
     */
    public $phase;
    /**
     * @var FitnessChallengePhase
     */
    public $upComingPhase;

    /**
     * Create a new event instance.
     *
     * @param FitnessChallenge $challenge
     * @param FitnessChallengePhase $phase
     * @param FitnessChallengePhase $upComingPhase
     */
    public function __construct(FitnessChallenge $challenge, FitnessChallengePhase $phase, FitnessChallengePhase $upComingPhase = null)
    {
        $this->challenge = $challenge;
        $this->phase = $phase;
        $this->upComingPhase = $upComingPhase;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
