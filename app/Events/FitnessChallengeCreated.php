<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class FitnessChallengeCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var \App\Models\FitnessChallenge
     */
    public $challenge;

    /**
     * Create a new event instance.
     *
     * @param \App\Models\FitnessChallenge $challenge
     */
    public function __construct(\App\Models\FitnessChallenge $challenge)
    {
        $this->challenge = $challenge;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
