<?php

namespace App\Listeners;

use App\Core\Notifications\PushNotifications;
use App\Core\NotificationType;
use App\Events\ChallengeVoted;
use App\Models\ChallengeRequest;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ChallengeVotedListener
{
    /**
     * @var PushNotifications
     */
    private $pushNotifications;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(PushNotifications $pushNotifications)
    {
        $this->pushNotifications = $pushNotifications;
    }

    /**
     * Handle the event.
     *
     * @param  ChallengeVoted  $event
     * @return void
     */
    public function handle(ChallengeVoted $event)
    {
        $challenge = ChallengeRequest::find($event->vote->challenge_id);

        /** @var \App\User $currentUser */
        $currentUser = auth()->user();

        $currentUser->state->increment('points', setting('points_earned_per_vote', 1));

        $challenge->savePointLog(setting('points_earned_per_vote', 1), "FTFP Challenge voted");

        $currentUser->saveLog(__('log_messages.you_voted'));

        $this->sendPushNotifications($challenge);
    }

    private function sendPushNotifications(ChallengeRequest $challenge)
    {
        $sender = $challenge->sender;

        $recipient = $challenge->recipient;

        $this->pushNotifications->addDevice($sender->device_id, $sender->device_type);

        $this->pushNotifications->addDevice($recipient->device_id, $recipient->device_type);

        $this->pushNotifications->send(
            __('notifications.titles.challenge_voted'),
            __('notifications.body.challenge_voted', ['name' => auth()->user()->name]),
            NotificationType::CHALLENGE,
            ['challenge_id' => $challenge->id, 'view' => 4]
        );
    }
}
