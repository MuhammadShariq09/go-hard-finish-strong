<?php

namespace App\Listeners;

use App\Events\ChallengeRequestDelete;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ChallengeRequestDeleteListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ChallengeRequestDelete  $event
     * @return void
     */
    public function handle(ChallengeRequestDelete $event)
    {
        $challenge = $event->challengeRequest;

        $challenge
            ->sender
            ->state
            ->decrement('points', setting('challenge_points',1));

        $challenge->savePointLog(-$challenge->leverage_points, "FTFP Challenge Accepted", $challenge->sender->id);
    }
}
