<?php

namespace App\Listeners;

use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ChallengeMoveIntoVotingListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $challenge = $event->challenge;
        /** @var User $sender*/
        $sender = $challenge->sender;
        /** @var User $recipient*/
        $recipient = $challenge->recipient;

        $friends = $sender->getFriends();
        $friends = $friends->merge($recipient->getFriends());

        $friends->each(function ($friend) use($sender, $recipient) {

            if (!in_array($friend->id, [$sender->id, $recipient->id])) {
                $friend->state->increment('unseen_voting_request_count');
            }

        });

    }
}
