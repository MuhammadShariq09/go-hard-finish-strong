<?php

namespace App\Listeners;

use App\Core\Notifications\PushNotifications;
use App\Core\NotificationType;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ChallengeRequestRejectedListener
{
    /**
     * @var PushNotifications
     */
    private $pushNotifications;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(PushNotifications $pushNotifications)
    {
        //
        $this->pushNotifications = $pushNotifications;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $sender = $event->challengeRequest->sender;
        $recipient = $event->challengeRequest->recipient;

        $sender->state
            ->increment('count_initiated_request');

        $sender->state
            ->increment('count_initiated_request');

        // Generate Log to user
        auth()->user()->saveLog(__('log_messages.challenge_request_rejected', ['sender_name' => $sender->name]));

        $this->pushNotifications->addDevice($sender->device_id, $sender->device_type);

        $this->pushNotifications->send(
            __('notifications.titles.challenge_rejected'),
            __('notifications.body.challenge_rejected', ['name' => $recipient->name]),
            NotificationType::CHALLENGE,
            ['challenge_id' => $event->challengeRequest->id, 'view' => 0]
        );
    }
}
