<?php

namespace App\Listeners;

use App\Models\Administrator\Admin;
use App\Models\Employee;
use App\Notifications\FeedbackSubmitted;
use App\Notifications\NewContactFormSubmitted;
use App\Notifications\UserRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;

class FeedbackSubmittedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        Notification::send(Admin::all(), new FeedbackSubmitted($event->feedback));
        Notification::send(Employee::all(), new FeedbackSubmitted($event->feedback));
        auth()->user()->notify(new NewContactFormSubmitted());
    }
}
