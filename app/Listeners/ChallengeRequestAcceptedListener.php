<?php

namespace App\Listeners;

use App\Core\Notifications\PushNotifications;
use App\Core\NotificationType;
use App\Events\ChallengeRequestAccepted;
use App\Models\ChallengeRequest;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ChallengeRequestAcceptedListener
{
    /**
     * @var PushNotifications
     */
    private $pushNotifications;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(PushNotifications $pushNotifications)
    {
        //
        $this->pushNotifications = $pushNotifications;
    }

    /**
     * Handle the event.
     *
     * @param  ChallengeRequestAccepted  $event
     * @return void
     */
    public function handle(ChallengeRequestAccepted $event)
    {
        /** @var \App\Models\ChallengeRequest $challengeRequest*/
        $challengeRequest = $event->challengeRequest;

        /** @var \App\User $recipient*/
        $recipient = $event->challengeRequest->recipient;

        /** @var \App\User $sender*/
        $sender = $event->challengeRequest->sender;

        // Decrement initial request.
        $recipient->state->decrement('accept_requests');
        $sender->state->decrement('initial_requests');

        // Increment Running Challenges.
        $recipient->state->increment('count_running_challenges');
        $sender->state->increment('count_running_challenges');
        $sender->state->increment('count_initiated_request');


        // Increment Points.
        $sender->state
            ->increment('ftfp_points', setting('challenge_points', 0));
        $recipient->state
            ->increment('ftfp_points', setting('challenge_accept_points', 0));

        $sender->incrementBonusPoints();
        $recipient->incrementBonusPoints();

        $sender->incrementLevel();
        $recipient->incrementLevel();

        $challengeRequest->savePointLog(setting('challenge_accept_points', 0), "FTFP Challenge Accepted");

        $challengeRequest->savePointLog(setting('challenge_points', 0), "FTFP Challenge Initiated", $sender->id);

        $event->challengeRequest->update(['sender_seen' => false]);

        // Generate Log to user
        $recipient->saveLog(__('log_messages.challenge_request_accepted', ['sender_name' => $event->challengeRequest->sender->name]));

        $sender->notify(new \App\Notifications\ChallengeRequestAccepted($recipient));

        $this->pushNotifications->addDevice($sender->device_id, $sender->device_type);

        $this->pushNotifications->send(
            __('notifications.titles.challenge_accepted'),
            __('notifications.body.challenge_accepted', ['name' => $recipient->name]),
            NotificationType::CHALLENGE,
            ['challenge_id' => $event->challengeRequest->id, 'view' => 3, 'type' => 'challenge']
        );

        // dd($sender, $this->pushNotifications->getResults());

    }
}
