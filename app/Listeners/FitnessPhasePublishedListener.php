<?php

namespace App\Listeners;

use App\Core\Gender;
use App\Core\Status;
use App\Models\UserFitnessChallenge;
use App\Notifications\FitnessChallengeWon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class FitnessPhasePublishedListener
{
    protected $event;

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return bool|void
     */
    public function handle($event)
    {
        $this->event = $event;

        if(!$event->upComingPhase)
            return $this->getWinner();

        $this->sendInvite();
    }

    protected function sendInvite()
    {
        $malePercentage = $this->event->upComingPhase->passing_male_criteria;
        $femalePercentage = $this->event->upComingPhase->passing_female_criteria;
        $challenge = $this->event->challenge;
        $phaseNo = $this->event->challenge->current_phase-1;

        $maleCounts = $this->getChallengesByStatus(Gender::MALE, $malePercentage);
        $femaleCounts = $this->getChallengesByStatus(Gender::FEMALE, $femalePercentage);

        $userIds = $this->getUserIds($maleCounts, $femaleCounts);

        $challenge->{"phase_{$challenge->current_phase}_qualifier_count"} = $userIds->count();
        $challenge->{"phase_{$phaseNo}_published"} = true;
        $challenge->save();

        UserFitnessChallenge::query()
            ->whereIn('user_id', $userIds)
            ->where('challenge_id', $challenge->id)
        ->update(["phase_{$challenge->current_phase}_qualified" => true]);
    }

    protected function getChallengesByStatus($status, $percentage)
    {
        $phaseNo = $this->event->challenge->current_phase-1;
        $count = UserFitnessChallenge::query()->where('challenge_id', $this->event->challenge->id)
            ->whereHas('player', function($q) use($status){
                $q->where('gender', $status);
            })->whereNotNull("phase_{$phaseNo}_rank")
            ->orderBy("phase_{$phaseNo}_rank")->count();

        if(!$count) return 0;
        return round($count/100*$percentage, 0);
    }

    protected function getUserIds($maleCounts, $femaleCounts)
    {
        $maleIds = $this->getUserIdsQuery(Gender::MALE, $maleCounts);
        $femaleIds = $this->getUserIdsQuery(Gender::FEMALE, $femaleCounts);
        return $maleIds->merge($femaleIds);
    }

    protected function getUserIdsQuery($gender ,$limit)
    {
        if(!$limit) return collect([]);

        $phaseNo = $this->event->challenge->current_phase-1;

        return UserFitnessChallenge::query()
            ->select('user_id')
            ->where('challenge_id', $this->event->challenge->id)
            ->whereHas('player', function($q) use($gender){
                $q->where('gender', $gender);
            })->whereNotNull("phase_{$phaseNo}_rank")
            ->orderBy("phase_{$phaseNo}_rank")
            ->limit($limit)->pluck('user_id');
    }

    private function getWinner()
    {
        $challenge = $this->event->challenge;
        $winner = UserFitnessChallenge::query()
            ->select('user_id')
            ->where('challenge_id', $challenge->id)
            ->whereNotNull("phase_{$challenge->current_phase}_rank")
            ->orderBy("phase_{$challenge->current_phase}_rank")
            ->first();

        if($winner) {
            $winner->player->state->increment('points', $challenge->points);
            $winner->player->notify(new FitnessChallengeWon());

            $challenge->winner_id = $winner->player->id;
        }

        $challenge->status = Status::COMPLETED;
        $challenge->{"phase_{$challenge->current_phase}_published"} = true;

        $challenge->save();

        return false;
    }
}
