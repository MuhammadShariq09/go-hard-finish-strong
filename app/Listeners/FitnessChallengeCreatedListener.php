<?php

namespace App\Listeners;

use App\Core\Notifications\PushNotifications;
use App\Models\FitnessChallenge;
use App\Models\State;
use App\Models\UserFitnessChallenge;
use App\Notifications\FitnessChallengeInvitation;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;

class FitnessChallengeCreatedListener
{
    protected $pushNotifications;

    /**
     * Create the event listener.
     *
     * @param PushNotifications $pushNotifications
     */
    public function __construct(PushNotifications $pushNotifications)
    {
        $this->pushNotifications = $pushNotifications;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $this->sendInvitation($event->challenge);
    }

    private function sendInvitation($challenge)
    {
        if($challenge->type === FitnessChallenge::CLOSED)
            return;

        $users = $this->getUsers($challenge);

        $userChallenges = collect([]);

        $challenge->phases->each(function($phase){
            $phase->video_submitted = false;
            $phase->video = null;
            $phase->phase_submitted = false;
            $phase->my_rank = null;
            $phase->user_reps = null;
            $phase->phase_id = $phase->id;
            $phase->message = "";
            $phase->qualified_to_next_phase = false;
            $phase->complete_time = null;
            $phase->total_participants = null;
            $phase->result_submitted = false;
        });

        $ph1_qualifier_count = count($users);

        // Add Total participants in phases
        $challenge->phase_1_qualifier_count = $ph1_qualifier_count;
        $challenge->phase_2_qualifier_count = 0;
        $challenge->phase_3_qualifier_count = 0;
        $challenge->phase_4_qualifier_count = 0;
        $challenge->save();

        $users->each(function($user) use($userChallenges, $challenge, $ph1_qualifier_count){
            $userChallenges->push([
                'challenge_id'          => $challenge->id,
                'user_id'               => $user->id,
                'status'                => 0,
                'phases_data'           => json_encode($challenge->phases->toArray())
            ]);
        });

        UserFitnessChallenge::insert($userChallenges->toArray());

        State::query()->whereIn('user_id', $users->pluck('id'))
            ->increment("unseen_fitness_{$challenge->type}_challenge");

        // Notification::send($users, new FitnessChallengeInvitation($challenge));

        $this->pushNotifications->addDevices($users->pluck('device_type', 'device_id')->toArray());

        $this->pushNotifications->send( ucfirst($challenge->type) . ' Fitness Challenge', $challenge->title, '', ['challenge_id' => $challenge->id]);
    }

    protected function getUsers($challenge)
    {
        $query = User::query()->select('id', 'email', 'device_id', 'device_type');

        if(request('gender_for') && count(is_string(request('gender_for'))? explode(',', request('gender_for')): request('gender_for')) === 1)
            $query->where('gender', request('gender_for')[0]);

        $ids = request('user_ids', '');

        if($ids && $challenge->type == FitnessChallenge::INVITATION)
            $query->whereIn('id', explode(',',$ids));

        return $query->get();
    }

}
