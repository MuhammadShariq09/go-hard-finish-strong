<?php

namespace App;

use App\Core\Status;
use App\Core\Traits\Challengable;
use App\Core\Traits\Friendable;
use App\Core\Traits\ImageableFill;
use App\Core\Traits\Loggable;
use App\Events\ChallengeRequestSent;
use App\Models\ChallengeRequest;
use App\Models\CreditCard;
use App\Models\FitnessChallenge;
use App\Models\Path;
use App\Models\State;
use App\Models\Transaction;
use App\Models\TriviaChallenge;
use App\Models\UserFitnessChallenge;
use App\Models\UserTriviaChallenge;
use App\Models\Voting;
use Hootlex\Friendships\Models\Friendship;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Laravel\Cashier\Billable;
use Laravel\Cashier\Cashier;
use Laravel\Cashier\Exceptions\InvalidStripeCustomer;
use Laravel\Cashier\PaymentMethod;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens, Friendable, Loggable, Challengable, ImageableFill, Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'address', 'status', 'country', 'city', 'address_state', 'postal_code', 'contact', 'image', 'device_id', 'description', 'gender'
    ];

//    protected $appends = ['gender'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'status' => 'boolean',
    ];

    protected $imageFolder = 'profile';

    public function state()
    {
        return $this->hasOne(State::class);
    }

    public function addState()
    {
        $this->state()->save(new State([
            'level' => "1",
            'path' => "EAST",
            'global_path' => "1",
            'global_level' => 0,
            'points' => 0,
            'initial_requests' => setting('allowed_requests_monthly', 0),
            'challenge_votes' => setting('allowed_requests_monthly', 0),
            'accept_requests' => setting('allowed_requests_monthly', 0),
            'remaining_friends_request_accept' => setting('maximum_friends_request_accept_in_a_month', 0),
            'remaining_days' => 30,
        ]));
        $this->load('state');
    }

    public function canSendChallengeRequest() {
        return $this->state->initial_requests > 0;
    }

    public function canAcceptChallengeRequest() { return $this->state->accept_requests > 0; }

    public function feedbacks()
    {
        return $this->hasMany(\App\Models\Feedback::class);
    }

    public function challenges()
    {
        return $this->hasMany(\App\Models\ChallengeRequest::class, 'sender_id');
    }

    public function votings()
    {
        return $this->hasMany(\App\Models\Voting::class, 'voter_id');
    }

    public function addVote(ChallengeRequest $challenge, $userID)
    {
        $vote = new Voting(['challenge_id' => $challenge->id, 'vote_for' => $userID]);
        $this->votings()->save($vote);
        $this->state->increment('ftfp_points', setting('points_earned_per_vote'));
        $this->state->decrement('challenge_votes');
        $this->incrementBonusPoints();
        $this->incrementLevel();

        $challenge->savePointLog(setting('points_earned_per_vote', 0), "FTFP Challenge Vote");

        return $this;
    }

    public function getChallenges($filter=null)
    {
        return $this->challenges()->filter($filter)->orWhere('recipient_id', $this->id)
            ->with(['sender.state', 'recipient.state']);
    }

    public function getChallengeVotingRequestCount()
    {
        $ids = auth()->user()->getFriendsIds();

        $voting_request_count = ChallengeRequest::whereStatus(Status::IN_VOTING)
            ->where(function($q) use($ids){
                $q->orWhereIn('sender_id', $ids);
                $q->orWhereIn('recipient_id', $ids);
            })
            ->where('sender_id', '!=', auth()->id())
            ->where('recipient_id', '!=', auth()->id())
            ->count();

        $this->voting_request_count = $voting_request_count;

        return $voting_request_count;
    }

    public function getCurrentPath()
    {
        return Path::query()->where('level', $this->state->level)->where('path', $this->state->path)->first();
    }

    public function getNextPath()
    {
        return Path::find(optional($this->getCurrentPath())->id + 1);
    }

    public function setBadgeCount()
    {
        $this->useen_pending_challenges_count = ChallengeRequest::query()->where('status', Status::PENDING)->where('recipient_id', $this->id)
            ->where('recipient_seen', false)->count();
        $this->unseen_accepted_challenges_count = $this->getUnseenCountByStatus(Status::ACCEPTED);
        $this->unseen_voting_challenges_count = $this->getUnseenCountByStatus(Status::IN_VOTING);
        $this->unseen_completed_challenges_count = $this->getUnseenCountByStatus(Status::COMPLETED);
        $this->unseen_submitted_challenges_count = $this->getUnseenCountByStatus(Status::SUBMITTED);
        $this->unseen_friend_request_count = $this->state->unseen_friend_request_count;
        $this->unseen_voting_request_count = $this->state->unseen_voting_request_count;
        $this->unseen_fitness_invitation_challenge_count = $this->state->unseen_fitness_invitation_challenge;
        $this->unseen_fitness_closed_challenge_count = $this->state->unseen_fitness_closed_challenge;
        $this->unseen_fitness_my_challenge_count = $this->state->unseen_fitness_my_challenge;

        $this->next_level_points = $this->getNextPath()->points;
        $this->minimum_users_to_show_trivia_challenge = (int)setting('minimum_users_to_show_trivia_challenge', 500);
        $this->minimum_users_to_show_fitness_challenge = (int)setting('minimum_users_to_show_fitness_challenge', 1000);
        $this->total_users_count = User::query()->count('id');
    }

    protected function getUnseenCountByStatus($status)
    {
        // \DB::listen(function($d){ dd($d); });
        return ChallengeRequest::query()->where('status', $status)->where(function($builder){
            $builder->orWhere(function($builder){
                $builder->where('sender_id', $this->id)
                    ->where('sender_seen', false);
            })->orWhere(function($builder){
                $builder->where('recipient_id', $this->id)
                    ->where('recipient_seen', false);
            });
        })->count();
    }

    public function getChallengesCount()
    {
        $this->challenge_request_count      = ChallengeRequest::query()->where('status', Status::PENDING)->where('recipient_id', $this->id)->count();
        $this->challenge_initiated_count    = ChallengeRequest::query()->where('sender_id', $this->id)->count();
        $this->challenge_accepted_count     = ChallengeRequest::whereIn('status', [Status::ACCEPTED, Status::SUBMITTED])->where(function($builder){
            $builder->orWhere(function($builder){
                $builder->orWhere('sender_id', $this->id)
                    ->orWhere('recipient_id', $this->id);
            })->where(function($builder){
                $builder->orWhere('recipient_id', $this->id)
                    ->orWhere('sender_id', $this->id);
            });
        })->count();

        $this->challenge_completed_count    = ChallengeRequest::query()->where('status', Status::COMPLETED)->where(function(Builder $builder){
            $builder->orWhere('sender_id', $this->id)->orWhere('recipient_id', $this->id);
        })->count();

        $this->challenge_verification_count = ChallengeRequest::query()->whereIn('status', [Status::IN_VOTING, Status::DISPUTE])->where(function(Builder $builder){
            $builder->orWhere('sender_id', $this->id)->orWhere('recipient_id', $this->id);
        })->count();

        $this->total_challenges = ChallengeRequest::query()->where(function(Builder $builder){
            $builder->orWhere('sender_id', $this->id)->orWhere('recipient_id', $this->id);
        })->count();

        $this->next_level = $this->getNextPath();
    }

    protected function getCountByStatus($status, $dump = false)
    {
        // \DB::listen(function($d){ dd($d); });
        $query = ChallengeRequest::query();

        if(is_array($status))
            $query->whereIn('status', $status);
        else{
            $query->where('status', $status);
        }

        return $query->where(function($builder){
            $builder->orWhere(function($builder){
                $builder->where('sender_id', $this->id)
                    ->where('sender_seen', false);
            })->orWhere(function($builder){
                $builder->where('recipient_id', $this->id)
                    ->where('recipient_seen', false);
            });
        })->{$dump? 'dump': 'count'}();
    }

    public function sendChallengeRequest($friend, $challenge)
    {
        $challenge->load('exercises');
        $this->challenges()->save(new ChallengeRequest([
            'recipient_id'      => $friend->id,
            'challenge_id'      => $challenge->id,
            'challenge_data'    => $challenge,
            'duration'          => setting('time_to_complete_a_challenge'),
            'leverage_points'   => setting('leverage_points'),
        ]));

        $challengeRequest = $this->challenges()->latest()->first();

        event(new ChallengeRequestSent($challengeRequest));

        return $challengeRequest;
    }

    public function incrementBonusPoints($challenge = null)
    {
        $bonus_applicable_points = $this->state->ftfp_points - $this->state->ftfp_bonus_exclusive_points;

        $multiplier = floor($bonus_applicable_points / setting('bonus_points_interval', 1));

        if($multiplier < 1)
            return false;

        $bonusPoints = $multiplier * setting('ftfp_bonus_points', 0);

        $this->state->increment( 'points', $bonusPoints );

        $this->state->increment( 'ftfp_bonus_exclusive_points', $bonusPoints );

        if($challenge)
            $challenge->savePointLog($bonusPoints, "FTFP Challenge Bonus Point");

        return $this;
    }

    public function incrementLevel()
    {
        $path = Path::whereLevel($this->state->level+1)->wherePath($this->state->path)->first();

        $myState = $this->state;

        if(!$path) return;

        if($myState->points >= $path->points && $path->level == $myState->level && $path->path === $myState->path)
        {
            $upLevel = Path::where('id', $path->id+1)->first();

            $this->state->update(['path' => $upLevel->path, 'level' => $upLevel->level]);
        }

        return $this;
    }

    public function countActiveFriends()
    {
        try{
            /** @var Collection $friendsId */
            $friendsId = Friendship::whereSenderId($this->id)->orWhere('recipient_id', $this->id)->pluck('sender_id', 'recipient_id');

            $friendsId = $friendsId->merge($friendsId->keys())->unique();

            $activeFriendsCount = User::query()->whereIn('id', $friendsId)->whereStatus(true)->count();

            return $activeFriendsCount;
        } catch (\Exception $exception) {
            return 0;
        }
    }

    public function triviaChallenges()
    {
        return $this->belongsToMany(TriviaChallenge::class, UserTriviaChallenge::class, 'user_id', 'challenge_id')
            ->withPivot('challenge_data', 'total_questions', 'attempted_questions', 'correct_questions', 'wrong_questions', 'earned_points', 'duration', 'paid', 'ended_at', 'completed')
            ->withTimestamps();
    }

    public function alreadyAttempted($challenge){
        return $this->triviaChallenges()->whereRaw('`user_trivia_challenges`.`challenge_id` = ' . $challenge->id)->count() > 0;
    }

    /**
     * Add a payment method to the customer.
     *
     * @param \Stripe\PaymentMethod|string $paymentMethod
     * @return \Laravel\Cashier\PaymentMethod
     * @throws InvalidStripeCustomer
     */
    public function addPaymentMethod($card_details)
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
        $paymentMethod = \Stripe\PaymentMethod::create(['type' => 'card', 'card' => $card_details]);

        $this->assertCustomerExists();

        $this->addCards($card_details, $paymentMethod->id);

        $stripePaymentMethod = $this->resolveStripePaymentMethod($paymentMethod);

        if ($stripePaymentMethod->customer !== $this->stripe_id) {
            $stripePaymentMethod = $stripePaymentMethod->attach(
                ['customer' => $this->stripe_id], Cashier::stripeOptions()
            );
        }

        return new PaymentMethod($this, $stripePaymentMethod);
    }

    // Payment Log will be linked to user by it's transactor_id column in transactions table
    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'transactor');
    }

    // Points Log will be linked to user by it's player_id column in point_logs table
    public function points()
    {
        return $this->hasMany(PointLog::class, 'player_id');
    }

    public function addCards($card_details, $stripe_pm_id = null)
    {

        foreach ($card_details as $key => $value)
            $card_details[$key] = encrypt($value);

        $card_details = array_merge($card_details, ['stripe_pm_id' => $stripe_pm_id, 'default' => 1]);

        $card = new CreditCard($card_details);

        $this->cards()->whereDefault(true)->update(['default' => false]);

        $this->cards()->save($card);
    }

    public function cards()
    {
        return $this->hasMany(CreditCard::class, 'user_id');
    }

    public function scopeFilter($query, $filters)
    {
        return $filters->apply($query);
    }

    /*public function getContactAttribute($value)
    {
        return sprintf("+1 (%d) %d-%d", substr($value,0, 3), substr($value,3, 3), substr($value,6, 4));
    }*/

    public function fitnessChallenges()
    {
        return $this->belongsToMany(FitnessChallenge::class, UserFitnessChallenge::class, 'user_id', 'challenge_id')
            ->withPivot('status', 'accepted_at');
    }

    public function myRank()
    {
        $sql = "SELECT    points,
                          user_id,
                          @curRank := @curRank + 1 AS rank
                FROM      states, (SELECT @curRank := 0) r
                ORDER BY  points DESC";

        $ranks = DB::select($sql);
        $rank = array_filter($ranks, function($rank){
            return $rank->user_id == $this->id;
        });
        return array_shift($rank)->rank;
    }

    /*public function getGenderAttribute($value){
        if($value == '1')
        {
            return "Male";
        }
        else
            return "Female";
    }*/

}
