<?php


namespace App\Core;


class NotificationType
{
    const FRIENDSHIP = "friendship";
    const CHALLENGE = "challenge";
}