<?php

namespace App\Core\Traits;

use App\Models\ActivityLog;

trait Filterable{

    public function scopeFilter($query, $filters)
    {
        return $filters->apply($query);
    }

}
