<?php


namespace App\Core\Traits;


use Illuminate\Support\Facades\DB;

trait Graphable
{
    protected $stateYear;
    protected $stateAggregate;
    protected $stateTable;
    protected $aggregateColumn;
    protected $groupByColumn;

    public function aggregate($aggregate)
    {
        if(!in_array($aggregate, ['sum', 'avg', 'count']))
            throw new \Exception("Invalid Aggregate Function call.");
        $this->stateAggregate = $aggregate;
        return $this;
    }

    public function aggregateColumn($col)
    {
        $this->aggregateColumn = $col;
        return $this;
    }

    public function groupBy($col)
    {
        $this->groupByColumn = $col;
        return $this;
    }

    protected function intialize()
    {
        if(!$this->stateYear) $this->stateYear = date('Y');
        if(!$this->stateAggregate) $this->stateAggregate = config('graphable.aggregate_function', 'count');
        if(!$this->aggregateColumn) $this->aggregateColumn = config('graphable.aggregate_column', 'created_at');
        if(!$this->groupByColumn) $this->groupByColumn = config('graphable.aggregate_column', 'created_at');

        $this->stateTable = $this->getTable();
    }

    public function getState()
    {
        $this->intialize();

        $records = DB::table($this->stateTable)
            ->selectRaw("year($this->aggregateColumn) as year,
                            month($this->aggregateColumn) as month,
                            {$this->stateAggregate}(id) as {$this->stateTable}_count"
                )
            ->whereYear($this->aggregateColumn, $this->stateYear)
            ->groupBy([DB::raw("year({$this->aggregateColumn})"), DB::raw("month({$this->aggregateColumn})")])
            ->get();

        $records = collect($records);

        $data = collect([]);

        $months = ['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

        for ($i = 1; $i <= 12; $i++){
            $std = new \stdClass();
            $std->name = $months[$i];
            $std->value = $records->where('month', $i)->sum("{$this->stateTable}_count");
            $data->push($std);
        }

        return $data;
    }
}
