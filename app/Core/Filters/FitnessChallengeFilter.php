<?php

namespace App\Core\Filters;

use App\Core\Filters\Filters;
use App\Core\Status;
use App\Models\FitnessChallenge;

class FitnessChallengeFilter extends Filters
{
    /**
     * Defines columns that end-user may filter by.
     *
     * @var array
     */
    protected $filters = ['type', 'search_term'];


    /**
     * Define allowed generics, and for which fields.
     *
     * @return void
     * @throws \Throwable
     */
    protected function type($value)
    {
        throw_if(!in_array($value, [FitnessChallenge::INVITATION, FitnessChallenge::CLOSED, 'pending', 'history']),
            "Invalid type argument passed.");

        if($value === 'pending'){
            $this->builder->where('status', '!=', Status::COMPLETED);
            return;
        }

        if($value === 'history'){
            $this->builder->where('status', Status::COMPLETED);
            return;
        }

        $this->builder->where('type', $value);
	}

	protected function search_term($value)
    {
        $this->builder->where('title', "LIKE", "%{$value}%");
    }
}
