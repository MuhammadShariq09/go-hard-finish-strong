<?php

namespace App\Core\Filters;

use App\Models\ChallengeRequest;
use App\Models\UserTriviaChallenge;
use Illuminate\Http\Request;

class TriviaChallengesFilters extends Filters {

    protected $filters = ['title', 'paid', 'search_term'];

    public function apply($builder)
    {
        $this->builder = $builder;

        if(auth()->check())
        {
            $challengeIds = UserTriviaChallenge::query()->where('user_id', auth()->id())->pluck('challenge_id');
            $this->builder->whereNotIn('id', $challengeIds);
        }

        foreach($this->getFilters() as $filter => $value)
        {
            if(method_exists($this, $filter))
            {
                $this->$filter($value);
            }
        }

        return $this->builder;
    }

    public function title($title)
    {
        $this->builder->where('title', "LIKE", "%{$title}%");
    }

    public function paid($isPaid)
    {
        if($isPaid)
            $this->builder->where('type', 'PAID');
        else
            $this->builder->where('type', 'FREE');
    }

    public function search_term($term)
    {
        $this->builder->where(function($q)use($term){
            $q->where('title', "LIKE", '%' . $term . '%');
        });
    }
}
