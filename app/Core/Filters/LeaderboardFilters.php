<?php

namespace App\Core\Filters;

use App\Models\UserFitnessChallenge;
use Illuminate\Http\Request;

class LeaderboardFilters extends Filters {

    protected $filters = ['name', 'search_term', 'status', 'points', 'not_id_in', 'id_in', 'not_member'];

    function __construct(Request $request)
    {
        $request->merge(['status' => $request->status ?? 1]);
        parent::__construct($request);
    }

    public function name($name)
    {
        $this->builder->where('id', '!=', auth()->id());
        $this->builder->where("name", 'LIKE', "%{$name}%");
    }

    public function status($status)
    {
        $this->builder->whereStatus($status);
    }

    public function points($points)
    {
        try{
            list($int, $values) = explode('<=>', $points);
            list($from, $to) = explode(',', $values);
            if(!$from || !$to) return;

            $this->builder->whereHas('state', function($q) use($from, $to){
                $q->whereBetween('points', [$from, $to]);
            });
        } catch(\Exception $e){
        }
    }

    public function not_member($challengeId)
    {
        if(!$challengeId) return;
        $this->builder->whereNotIn('id', UserFitnessChallenge::whereChallengeId($challengeId)->pluck('user_id'));
    }

    public function not_id_in($ids)
    {
        if(!$ids) return;
        $this->builder->whereNotIn('id', explode(',', $ids));
    }

    public function id_in($ids)
    {
        if($ids === '') return;
        $this->builder->whereIn('id', explode(',', $ids));
    }

    public function search_term($term)
    {
        $this->builder->where(function($q)use($term){
            $q->where('name', "LIKE", '%' . $term . '%')
//            ->orWhere('description', "LIKE", '%' . $term . '%')
                ->orWhere('email', "LIKE", '%' . $term . '%')
                ->orWhere('contact', "LIKE", '%' . $term . '%');
        });
    }

}
