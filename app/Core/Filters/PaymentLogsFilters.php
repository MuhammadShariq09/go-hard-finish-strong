<?php

namespace App\Core\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class PaymentLogsFilters extends Filters {

    protected $filters = ['search_term'];

    function __construct(Request $request)
    {
        $request->merge(['status' => $request->status ?? 1]);
        parent::__construct($request);
    }

    public function type($type)
    {
        /*$this->builder->where("name", 'LIKE', "%{$type}%");*/
    }

    public function search_term($term)
    {
        $this->builder->where(function(Builder $builder) use($term){
            $builder
                ->orWhere("description", 'LIKE', "%{$term}%")
                ->orWhere("transitionable_type", 'LIKE', "App\Models\{$term}%")
                ->orWhere("amount", 'LIKE', "%{$term}%")
                ->orWhereRaw("DATE_FORMAT(created_at, '%m/%d/%Y') LIKE \"%{$term}%\"")
            ->orWhereHas('user', function(Builder $builder) use($term){
                $builder->where('name', 'LIKE', "%$term%");
                $builder->orWhere('id', 'LIKE', "%$term%");
            });
        });
    }

}
