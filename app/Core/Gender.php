<?php

namespace App\Core;

/**
 * Class Status.
 */
class Gender
{
    const MALE = 1;
    const FEMALE = 2;
    const BOTH = '3';
}
