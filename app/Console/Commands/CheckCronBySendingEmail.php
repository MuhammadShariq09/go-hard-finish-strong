<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class CheckCronBySendingEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Just Send an email to arif.iqbal@salsoft.net to check if cron is actually working or not.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Mail::raw("Cron is working", function($mail){
            $mail->to('arif.iqbal@salsoft.net');
            $mail->subject("Cron Test.");
        });
    }
}
