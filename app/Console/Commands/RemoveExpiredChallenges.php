<?php

namespace App\Console\Commands;

use App\Notifications\ChallengeExpired;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Models\ChallengeRequest;

class RemoveExpiredChallenges extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'challenge:remove_expired_challenge';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'It will removed expired challenges based on admin settings';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        /*Mail::raw("challenge expired called", function($message){
            $message->to('arif.iqbal@salsoft.net')
                ->subject('Command Ran');
        });*/

        $query = ChallengeRequest::whereIn('status', [1 ,4])->where('time_to_complete', '<', now()->toDateTimeString());
        $query->get()->each(function($challenge){
            $challenge->sender->notify(new ChallengeExpired());
            $challenge->recipient->notify(new ChallengeExpired());
        });
        $query->delete();
    }
}
