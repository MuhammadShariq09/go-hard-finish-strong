<?php

namespace App;

use App\Models\ChallengeAnswer;
use App\Models\TriviaChallenge;
use Illuminate\Database\Eloquent\Model;

class TriviaQuestion extends Model
{

    protected $table = 'trivia_questions';

    protected $fillable = ['title', 'question_id'];

    public function question()
    {
        return $this->belongsTo(TriviaChallenge::class, 'challenge_id');
    }

    public function answers()
    {
        return $this->hasMany(ChallengeAnswer::class, 'challenge_id');
    }


    public function saveAnswers($answers)
    {
        $this->answers()->whereNotIn('id', collect($answers)->pluck('id'))->delete();

        foreach($answers as $ans)
        {
            if(array_key_exists('id', $ans))
            {
                $exerciseModel = ChallengeAnswer::find($ans['id']);
                $exerciseModel->fill($ans);
                $exerciseModel->save();
                continue;
            }

            $exerciseModel = new ChallengeAnswer();
            $exerciseModel->fill($ans);
            $this->answers()->save($exerciseModel);
        }
    }
}
