<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

Route::redirect('/', 'admin/dashboard');

Route::get('logout', 'Auth\LoginController@logout');

Auth::routes();

Route::middleware('auth:admin')->get('/{admin?}', 'HomeController@index')->where('admin', '.*')->name('home');

//Route::get('/{any?}', function(){
//    dd('we here in admin route');
//})->where('any', '.*')->name('home');
