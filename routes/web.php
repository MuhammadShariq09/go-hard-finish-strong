<?php

use App\Events\UserRegistered;
use App\User;
use \Illuminate\Support\Facades\Route;
use \Illuminate\Support\Facades\Auth;
use \Illuminate\Support\Facades\Redis;
use \GraphQL\Type\Schema;
use \GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

Route::get('/', function(){
    return redirect()->route('admin.home');
});

Route::view('privacy-policy', 'privacy-policy')->name('privacy-policy');

/*Route::get('/', function (Illuminate\Http\Request $request) {


//    1) Publish event with redis
    $data = [
            'user_id' => '1',
            'user_name' => 'arifiqbal'
        ];

    $user = User::first();

    auth()->login($user);

    event(new UserRegistered($user));

//    Redis::publish('user-channel', json_encode($data));

    return 'done';
//    2) Node + Redis subscribe to the event
//    3) Use socket to emit to all client

});*/

Auth::routes();

Route::get('/{home?}', 'HomeController@index')->where('home', '(!admin|employee).*');
