<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use \Illuminate\Http\Request;

/**
 * API Routes for Employee
 */
Route::middleware('auth:api_employee')->group(function(){
    Route::get('/user', function (Request $request) {
        return $request->user();
    });
    Route::put('/user', 'AuthController@profilePut');
    Route::put('/user/password', 'AuthController@passwordPut');
});

Route::prefix('')->group(function () {
    Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::get('/chart/{year}/states', 'DashboardController@getChallengeStates')->name('chart.challenges.states');

    Route::prefix('users')->name('users')->group(function(){
        Route::get('/', 'UsersController@index')->name('index');
        Route::get('/profile', 'UsersController@profile')->name('profile');
        Route::get('/{user}', 'UsersController@show')->name('show');
        Route::put('/{user}', 'UsersController@update')->name('update');
        Route::put('/{user}/status', 'UsersController@statusPut')->name('status');
        Route::post('/', 'UsersController@store')->name('store');
        Route::get('/{user}/friends', 'UsersController@friends')->name('friends');
    });

    Route::get('polling-requests', 'PollingRequestsController@index');
    Route::get('polling-requests/{challengeRequest}', 'PollingRequestsController@show');
    Route::post('polling-requests/declare-result', 'PollingRequestsController@declareResult');

    Route::post('/check', 'UniqueValidationController@check');


    Route::get('/notifications', 'NotificationsController@index');
    Route::get('/notifications/bell', 'NotificationsController@bell_notifications');
    Route::patch('/notifications/{notification}/read', 'NotificationsController@readNotification');

    Route::prefix('fitness')->group(function() {
        Route::apiResource('challenges', 'FitnessChallengeController');
        Route::post('challenges/{challenge}', 'FitnessChallengeController@update');
        Route::put('challenges/{challenge}/invite', 'FitnessChallengeController@invite');
        Route::put('challenge/{challenge}/submit-result', 'EmployeeController@post_result');
        Route::put('challenge/{challenge}/submit-phase', 'EmployeeController@post_phase');
        Route::put('challenge/{challenge}/publish-phase-result', 'EmployeeController@publish_phase_result');
        Route::put('phase/{phase}', 'FitnessChallengeController@phase_update');
        Route::get('challenges/user_challenge_details/{ids}', 'FitnessChallengeController@user_challenge_details');
        Route::get('challenges/user_challenges/{id}', 'FitnessChallengeController@user_challenges');
    });
});
