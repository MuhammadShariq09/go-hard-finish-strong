<?php

use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Route;

Route::middleware('auth:api_admin')->group(function(){
    Route::get('/user', function (Request $request) {
        return $request->user();
    });
    Route::put('/user', 'AuthController@profilePut');
    Route::put('/user/password', 'AuthController@passwordPut');
});

// public routes
Route::post('/login', 'AuthController@login')->name('admin.login.api');
Route::post('/register', 'AuthController@register')->name('admin.register.api');

Route::group(['prefix' => 'password'], function(){
    Route::post('create', 'PasswordResetController@create');
    Route::get('toke/validate/{token}', 'PasswordResetController@validateToken');
    Route::post('reset', 'PasswordResetController@reset');
});

// private routes only for normal users
Route::middleware('auth:api_admin')->group(function () {

    Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::get('/chart/{year}/users', 'DashboardController@getUserStates')->name('chart.users.states');

    Route::get('/settings', 'SettingsController@index')->name('settings');
    Route::get('/settings/{key}', 'SettingsController@show')->name('settings.show');
    Route::post('/settings', 'SettingsController@store')->name('settings.store');

    Route::prefix('users')->name('users')->group(function(){
        Route::get('/', 'UsersController@index')->name('index');
        Route::get('/leaderboard', 'UsersController@leaderboardWeb')->name('leaderboard');
        Route::get('/profile', 'UsersController@profile')->name('profile');
        Route::get('/{user}', 'UsersController@show')->name('show');
        Route::put('/{user}', 'UsersController@update')->name('update');
        Route::put('/{user}/status', 'UsersController@statusPut')->name('status');
        Route::post('/', 'UsersController@store')->name('store');
        Route::get('/{user}/friends', 'UsersController@friends')->name('friends');
    });

    Route::prefix('employees')->name('employees')->group(function(){
        Route::get('/', 'EmployeesController@index')->name('index');
        Route::post('/', 'EmployeesController@store')->name('store');
        Route::get('/profile', 'EmployeesController@profile')->name('profile');
        Route::get('/{employee}', 'EmployeesController@show')->name('show');
        Route::put('/{employee}', 'EmployeesController@update')->name('update');
        Route::put('/{employee}/status', 'EmployeesController@statusPut')->name('status');
    });

    Route::apiResource('challenges', 'ChallengeController');
    Route::delete('challenges/{challenge}/exercises/{exercise}', 'ChallengeController@deleteExercise');
    Route::apiResource('categories', 'CategoriesController');
    Route::apiResource('fit-to-paths', 'PathController');
    Route::get('active-challenges', 'ChallengeController@activeChallenges');

    Route::get('polling-requests', 'PollingRequestsController@index');
    Route::get('polling-requests/{challengeRequest}', 'PollingRequestsController@show');
    Route::post('polling-requests/declare-result', 'PollingRequestsController@declareResult');

    Route::post('/check', 'UniqueValidationController@check');

    Route::get('/feedback', 'FeedbackController@index');
    Route::get('/feedback/{feedback}', 'FeedbackController@show');
    Route::delete('/feedback/{feedback}', 'FeedbackController@destroy');

    Route::get('/notifications', 'NotificationsController@index');
    Route::get('/notifications/bell', 'NotificationsController@bell_notifications');
    Route::patch('/notifications/{notification}/read', 'NotificationsController@readNotification');

    Route::get('/payment-logs', 'PaymentLogsController@index');
    Route::delete('/payment-logs/{transaction}', 'PaymentLogsController@destroy');

    Route::prefix('trivia')->group(function(){
        Route::apiResource('challenges', 'TriviaChallengeController');
    });

    Route::prefix('fitness')->group(function() {
        Route::apiResource('challenges', 'FitnessChallengeController');
        Route::post('challenges/{challenge}', 'FitnessChallengeController@update');
        Route::put('challenges/{challenge}/invite', 'FitnessChallengeController@invite');
        Route::put('challenge/{challenge}/submit-result', 'EmployeeController@post_result');
        Route::put('challenge/{challenge}/submit-phase', 'EmployeeController@post_phase');
        Route::put('challenge/{challenge}/publish-phase-result', 'EmployeeController@publish_phase_result');
        Route::put('phase/{phase}', 'FitnessChallengeController@phase_update');
        Route::get('challenges/user_challenge_details/{ids}', 'FitnessChallengeController@user_challenge_details');
        Route::get('challenges/user_challenges/{id}', 'FitnessChallengeController@user_challenges');
    });

});
