<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

Route::redirect('/', 'employee/dashboard');

Route::get('logout', 'Auth\LoginController@logout');

Auth::routes();

Route::middleware('auth:employee')
    ->get('/dashboard', 'HomeController@index')->name('dashboard');

Route::middleware('auth:employee')
    ->get('/{employee?}', 'HomeController@index')
    ->where('employee', '.*')->name('home');
