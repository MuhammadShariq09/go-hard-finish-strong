<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),

        'status' => 1,
        'address' => $faker->address,
        'country' => $faker->country,
        'city' => $faker->city,
        'address_state' => $faker->streetAddress,
        'postal_code' => $faker->postcode,
        'contact' => $faker->phoneNumber,
        'image' => 'images/profile/' . $faker->image( storage_path('app/public/images/profile/'),250, 250, 'fashion', false),
    ];
});

$factory->define(\App\Models\State::class, function (Faker $faker) {

    $path = ['EAST', 'WEST', 'NORTH', 'SOUTH'];
    return [
        'level' => $faker->numberBetween(1, 40),
        'path' => $path[$faker->numberBetween(0, 2)],
        'points' => $faker->numberBetween(0, 100000),
        'initial_requests' => $faker->numberBetween(0, 40),
        'challenge_votes' => $faker->numberBetween(0, 100),
        'accept_requests' => $faker->numberBetween(0, 40),
        'remaining_days' => $faker->numberBetween(0, 30),
        'user_id' => 1,
    ];
});

$factory->define(\App\Models\Administrator\Admin::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});

$factory->define(\App\Models\Staff\Staff::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});
