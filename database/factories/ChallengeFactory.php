<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Challenge;
use Faker\Generator as Faker;

$factory->define(Challenge::class, function (Faker $faker) {
    $path = ['East', 'West', 'North', 'South'];
    return [
        'title' => $faker->sentence,
        'reward_points' => $faker->numberBetween(10, 25),
        'description' => $faker->paragraph,
        'path' => $path[rand(0, 3)],
        'level' => "Level " . rand(1, 10),
        'image' => 'images/challenges/' . $faker->image( storage_path('app/public/images/challenges/'),400, 400, 'sports', false),
        'created_by' => function(){
            return factory(\App\Models\Administrator\Admin::class)->create()->id;
        }
    ];
});

$possibleCategories = array(
    'abstract', 'animals', 'business', 'cats', 'city', 'food', 'nightlife',
    'fashion', 'people', 'nature', 'sports', 'technics', 'transport'
);