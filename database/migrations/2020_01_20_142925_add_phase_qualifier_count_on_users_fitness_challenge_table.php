<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPhaseQualifierCountOnUsersFitnessChallengeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_fitness_challenges', function (Blueprint $table) {
            $table->integer('phase_1_rank')->nullable();
            $table->integer('phase_2_rank')->nullable();
            $table->integer('phase_3_rank')->nullable();
            $table->integer('phase_4_rank')->nullable();

            $table->boolean('phase_1_qualified')->nullable()->default(1);
            $table->boolean('phase_2_qualified')->nullable();
            $table->boolean('phase_3_qualified')->nullable();
            $table->boolean('phase_4_qualified')->nullable();
        });

        Schema::table('fitness_challenges', function (Blueprint $table) {
            $table->boolean('phase_1_submitted')->nullable();
            $table->boolean('phase_2_submitted')->nullable();
            $table->boolean('phase_3_submitted')->nullable();
            $table->boolean('phase_4_submitted')->nullable();

            $table->boolean('phase_1_published')->nullable();
            $table->boolean('phase_2_published')->nullable();
            $table->boolean('phase_3_published')->nullable();
            $table->boolean('phase_4_published')->nullable();

            $table->integer('phase_1_qualifier_count')->nullable();
            $table->integer('phase_2_qualifier_count')->nullable();
            $table->integer('phase_3_qualifier_count')->nullable();
            $table->integer('phase_4_qualifier_count')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_fitness_challenges', function (Blueprint $table) {
            $table->dropColumn([
                'phase_1_rank',
                'phase_2_rank',
                'phase_3_rank',
                'phase_4_rank',

                'phase_1_qualified',
                'phase_2_qualified',
                'phase_3_qualified',
                'phase_4_qualified',

            ]);
        });
        Schema::table('fitness_challenges', function (Blueprint $table) {
            $table->dropColumn([
                'phase_1_submitted',
                'phase_2_submitted',
                'phase_3_submitted',
                'phase_4_submitted',

                'phase_1_published',
                'phase_2_published',
                'phase_3_published',
                'phase_4_published',

                'phase_1_qualifier_count',
                'phase_2_qualifier_count',
                'phase_3_qualifier_count',
                'phase_4_qualifier_count',
            ]);
        });
    }
}
