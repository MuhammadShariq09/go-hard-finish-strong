<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('states', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('level')->default(1);
            $table->string('path')->default('EAST');
            $table->integer('points')->default(0);
            $table->integer('initial_requests')->default(0);
            $table->integer('challenge_votes')->default(0);
            $table->integer('accept_requests')->default(0);
            $table->integer('remaining_days')->default(0);
            $table->integer('count_initiated_request')->default(0);
            $table->integer('count_completed_challenges')->default(0);
            $table->integer('count_unverified_challenges')->default(0);
            $table->integer('count_running_challenges')->default(0);
            $table->integer('next_level_points')->default(0);
            $table->unsignedBigInteger('user_id');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('states');
    }
}
