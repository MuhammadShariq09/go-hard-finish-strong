<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFitnessChallengesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fitness_challenges', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->text('description')->nullable();
            $table->string('type');
            $table->tinyInteger('current_phase', false, true)->default(1);
            $table->unsignedTinyInteger('status')->default(1);
            $table->unsignedInteger('points')->default(0);
            $table->unsignedInteger('user_limit')->default(0);
            $table->unsignedInteger('prize')->default(0);
            $table->unsignedInteger('fee')->nullable();
            $table->string('gender_for')->default('1');
            $table->string('image')->nullable();
            $table->string('video')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        if (!Schema::hasColumn('users', 'gender'))
        {
            Schema::table('users', function (Blueprint $table) {
                $table->unsignedTinyInteger('gender')->default(1);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fitness_challenges');

        if(Schema::hasColumn('users', 'gender'))
        {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('gender');
            });
        }

    }
}
