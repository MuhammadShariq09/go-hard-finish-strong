<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTimeRepsFieldsOnUserFitnessChallenges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_fitness_challenges', function (Blueprint $table) {
            $table->unsignedInteger('phase_1_reps')->nullable();
            $table->time('phase_1_complete_time')->nullable();
            $table->unsignedInteger('phase_2_reps')->nullable();
            $table->time('phase_2_complete_time')->nullable();
            $table->unsignedInteger('phase_3_reps')->nullable();
            $table->time('phase_3_complete_time')->nullable();
            $table->unsignedInteger('phase_4_reps')->nullable();
            $table->time('phase_4_complete_time')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_fitness_challenges', function (Blueprint $table) {
            $table->dropColumn([
                'phase_1_reps',
                'phase_1_complete_time',
                'phase_2_reps',
                'phase_2_complete_time',
                'phase_3_reps',
                'phase_3_complete_time',
                'phase_4_reps',
                'phase_4_complete_time',
            ]);
        });
    }
}
