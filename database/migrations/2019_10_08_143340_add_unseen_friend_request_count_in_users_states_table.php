<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUnseenFriendRequestCountInUsersStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('states', function (Blueprint $table) {
            $table->integer('unseen_friend_request_count')->default(0);
            $table->integer('unseen_voting_request_count')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('states', function (Blueprint $table) {
            $table->dropColumn('unseen_friend_request_count');
            if (Schema::hasColumn('states', 'unseen_voting_request_count'))
                $table->dropColumn('unseen_voting_request_count');
        });
    }
}
