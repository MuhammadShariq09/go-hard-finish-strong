<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFitnessChallengesUnseenCountColumnsInUsersStateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('states', function (Blueprint $table) {
            $table->integer('unseen_fitness_invitation_challenge')->default(0);
            $table->integer('unseen_fitness_closed_challenge')->default(0);
            $table->integer('unseen_fitness_my_challenge')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('states', function (Blueprint $table) {
            $table->dropColumn(['unseen_fitness_invitation_challenge', 'unseen_fitness_closed_challenge', 'unseen_fitness_my_challenge']);
        });
    }
}
