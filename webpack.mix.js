const mix = require('laravel-mix');
require('laravel-mix-alias');

mix.alias({
    '@middleware': '/resources/js/middleware',
    '@validations': '/resources/js/validation_roles',
    '@utils': '/resources/js/utils',
    '@admin': '/resources/js/admin',
    '@': '/resources/js',
    '@employee': '/resources/js/employee',
    '~': '/resources/sass',
});

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/employee/employee-app.js', 'public/js/employee-app.js')
    .sass('resources/sass/app.scss', 'public/css');

// mix.js('resources/js/app.js', 'public/js')
//     .sass([
//         "resources/app-assets/css/vendors.css",
//         "resources/app-assets/vendors/css/forms/icheck/icheck.css",
//         "resources/app-assets/css/app.css",
//         "resources/app-assets/vendors/css/forms/icheck/custom.css",
//         "resources/app-assets/css/core/menu/menu-types/vertical-menu.css",
//         "resources/app-assets/css/plugins/extensions/toastr.min.css",
//         "resources/app-assets/css/pages/login-register.css",
//         "resources/assets/css/style.css",
//         "resources/app-assets/vendors/css/tables/datatable/datatables.min.css",
//         "resources/app-assets/vendors/css/tables/datatable/datatables.min.css"
//     ], 'public/css/app.css');




mix.js("resources/js/admin/app.js",  'public/admin-assets/js/app.js')
    .sass('resources/sass/admin/app.scss', 'public/admin-assets/css').options({
        processCssUrls: false
    });

if (mix.inProduction()) {
    mix.version();
}
